#!/usr/bin/perl -w
use strict;

#    [0m         reset; clears all colors and styles (to white on black)
#    [30m        set foreground color to black
#    [31m        set foreground color to red
#    [32m        set foreground color to green
#    [33m        set foreground color to yellow
#    [34m        set foreground color to blue
#    [35m        set foreground color to magenta (purple)
#    [36m        set foreground color to cyan
#    [37m        set foreground color to white
#    [39m        set foreground color to default (white)
#    [40m        set background color to black
#    [41m        set background color to red
#    [42m        set background color to green
#    [43m        set background color to yellow
#    [44m        set background color to blue
#    [45m        set background color to magenta (purple)
#    [46m        set background color to cyan
#    [47m        set background color to white
#    [49m        set background color to default (black)


while (<>) {
    s/([aàâäAÀÂÄ])/\e[30m$1\e[0m/g;
    s/([eéèêEÉÈÊ])/\e[37m$1\e[0m/g;
    s/([iîïIÎÏ])/\e[31m$1\e[0m/g;
    s/([oôöOÔÖ])/\e[35m$1\e[0m/g;
    s/([uûüUÛÜ])/\e[34m$1\e[0m/g;

    print $_;
}

my $text = "éà";

print "\e[31mé$text\e[0m\n";

