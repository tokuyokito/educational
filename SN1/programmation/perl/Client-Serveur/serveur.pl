#!/usr/bin/perl

# imports
use Socket;
use FileHandle;

# num�ro de port utilis� par le serveur
$port = 7799;

# protocole mis en jeu : TCP
$protocole = getprotobyname('tcp');

# cr�ation socket utilis� par le proxy
socket(ACCES_SERVEUR,AF_INET,SOCK_STREAM,$protocole) || die("Ouverture de socket: $!");

# on r�utilise le port si d�ja utilis�
setsockopt(ACCES_SERVEUR, SOL_SOCKET, SO_REUSEADDR, 1) || die("Configuration socket: $!");
$TSAP_serveur = pack('SnA4x8', AF_INET, $port, INADDR_ANY);

# affectation du port au socket
bind(ACCES_SERVEUR,$TSAP_serveur) || die("Accrochage socket: $!");

# �coute...
listen(ACCES_SERVEUR, SOMAXCONN) || die("Ecoute socket: $!");

# on attend les connexions des clients...
while ($maSocket = accept(CLIENT, ACCES_SERVEUR)) {
        while ($ligne = <CLIENT>) {
                chomp $ligne;
                print "$ligne\n";
                if ($ligne =~ /^callerid=(\d+)/) {
                        $callerid = $1;
                }
        }
        close(CLIENT);

        # Ex�cution de l'application
        #system("/usr/bin/mozilla-firefox http://linuxfr.org/pub/");
        system("/usr/bin/mozilla-firefox http://127.0.0.1/applicationCTI/appel.php?callerid=$callerid");
}
close (ACCES_SERVEUR);
