#!/usr/bin/perl

# imports dont on a besoin
use Socket;
use FileHandle;

# numero de port du serveur � contacter
$port_serveur = 7799;
$nom_serveur = "127.0.0.1";

# protocole mis en jeu : TCP
$protocole = getprotobyname('tcp');
#$iaddr = gethostbyname('portable-erwann');
 
# creation du socket utilis�e
socket(VERS_SERVEUR,AF_INET,SOCK_STREAM,$protocole) || die("Ouverture de socket: $!");

# suivant que l'adresse du serveur est sous forme d'adresse IP ou de nom symbolique
if ($nom_serveur =~ /^(\d+)\.(\d+)\.(\d+)\.(\d+)$/) {
        print "$1, $2, $3, $4\n";
        $adresse_ip_serveur = pack("C4", $1, $2, $3, $4);
} else { #conversion en adresse IP
        $adresse_ip_serveur = gethostbyname($nom_serveur);
}

# On a toutes les infos, on peut donc se connecter au serveur
$destination = pack('sna4x8', AF_INET, $port_serveur, $adresse_ip_serveur);

# connexion au serveur
connect(VERS_SERVEUR, $destination) || die("Connect: $!");

autoflush VERS_SERVEUR, 1;


while ($ligne = <STDIN>) {
        print VERS_SERVEUR $ligne;
}

close(VERS_SERVEUR);
