#!/usr/bin/perl
use strict;
use warnings;
use POSIX 'strftime';

if (scalar @ARGV != 1) {
    print "Mise en oeuvre de stat: informations relatives aux fichiers\n";
    print "Usage: $0 filename\n";
    exit(0);
}

my $filename = $ARGV[0];

#      0 dev      device number of filesystem
#      1 ino      inode number
#      2 mode     file mode  (type and permissions)
#      3 nlink    number of (hard) links to the file
#      4 uid      numeric user ID of file's owner
#      5 gid      numeric group ID of file's owner
#      6 rdev     the device identifier (special files only)
#      7 size     total size of file, in bytes
#      8 atime    last access time in seconds since the epoch
#      9 mtime    last modify time in seconds since the epoch
#     10 ctime    inode change time in seconds since the epoch (*)
#     11 blksize  preferred I/O size in bytes for interacting with the
#                 file (may vary from file to file)
#     12 blocks   actual number of system-specific blocks allocated
#                 on disk (often, but not always, 512 bytes each)

my ($dev, $ino, $mode, $nlink, $uid, $gid, $rdev,
    $size, $atime, $mtime, $ctime, $blksize, $blocks)
    = stat($filename);

my $adate = date($atime);
my $mdate = date($mtime);
my $cdate = date($ctime);

my $hsize = nice_size($size);

print <<"EOT"
\e[34m
  device number of filesystem..............................: \e[33m$dev\e[34m,
  inode number.............................................: \e[33m$ino\e[34m,
  file mode (type and permissions).........................: \e[33m$mode\e[34m,
  number of (hard) links to the file.......................: \e[33m$nlink\e[34m,
  numeric user ID of file's owner..........................: \e[33m$uid\e[34m,
  numeric group ID of file's owner.........................: \e[33m$gid\e[34m,
  the device identifier (special files only)...............: \e[33m$rdev\e[34m,
  total size of file, in bytes.............................: \e[33m$hsize\e[34m,
  last access time in seconds since the epoch..............: \e[33m$atime -> $adate\e[34m,
  last modify time in seconds since the epoch..............: \e[33m$mtime -> $mdate\e[34m,
  inode change time in seconds since the epoch.............: \e[33m$ctime -> $cdate\e[34m,
  preferred I/O size in bytes for interacting with the file: \e[33m$blksize\e[34m,
  actual number of system-specific blocks allocated........: \e[33m$blocks\e[34m.
\e[0m

EOT
;


exit 0;

sub nice_size
{
    my @sizes = qw( octet Ko Mo Go To Po);
    my $size = shift;

    my $i = 0;

    while ($size > 1000) {
        $size /= 1000;
        $i++;
    }

    return sprintf("%.2f $sizes[$i]", $size);
}

sub date {
    my ($time) = @_;
    my ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = localtime($time);

    my %month = (
         0 => 'Jan',  1 => 'Feb',  2 => 'Mar',  3 => 'Apr',
         4 => 'May',  5 => 'Jun',  6 => 'Jul',  7 => 'Aug',
         8 => 'Sep',  9 => 'Oct', 10 => 'Nov', 11 => 'Dec',
    );

    return sprintf($month{$mon} . " %2d " . (1900 + $year) . " %02d:%02d:%02d", $mday, $hour, $min, $sec);
}
