#!/usr/bin/perl

use strict;
use warnings;

#
# Programme comptant le pourcentage de lettres d'après un dictionnaire
#
# Auteur:  Le Gall Cédric 1 2017-2018
# version 0.1
# date 21 decembre 2017
#
# Installation du dictionnaire: apt-get install wfrench
# vérification: cat /usr/share/dict/french | wc -l
#

open (DIC, "< /usr/share/dict/french") || die"Erreur $! \n";

#creation de la table de hachage
my %alpha;

while (my $mot = <DIC>) {
	chomp $mot;
	
	# gestion les accents
    $mot =~ s/[àäâ]/a/g;
    $mot =~ s/[ç]/c/g;
    $mot =~ s/[éèêë]/e/g;
    $mot =~ s/[îï]/i/g;
    $mot =~ s/[ôöœ]/o/g;
    $mot =~ s/[ûüù]/u/g;
    $mot =~ s/[ÿŷ]/y/g;

    #séparé les lettres d'un mot
	my @lettres = split //, $mot;
	
	foreach my $c (@lettres){

		if(exists $alpha{$c}){
			$alpha{$c}++;
		}
		else{
			$alpha{$c} = 1;
		}
	}
}
close DIC;

#calcul de la somme total
my $somme;

foreach my $c (keys %alpha){
	
		if($c =~ /[a-z]/){
			
			$somme += $alpha{$c};
		}
		
}

print "$somme\n"

foreach my $c (keys %alpha){
	
		if($c =~ /[a-z]/){
			
			$somme += $alpha{$c};
		}

print "$alpha{$c}"

}
