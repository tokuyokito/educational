#!/usr/bin/perl

use strict;
use warnings;

#
# Programme comptant le pourcentage de lettres d'après un dictionnaire
#
# Auteur: ............. BTS SNIR 1 2017-2018
# version 0.1
# date 21 decembre 2017
#
# Installation du dictionnaire: apt-get install wfrench
# vérification: cat /usr/share/dict/french | wc -l
#

# table de hashage clés: lettres, valeurs: occurences 
my %alpha;

# ouverture du dictionnaire
open(DICT, '< /usr/share/dict/french') || die "Erreur: $!";

# lire tous les mots
while (<DICT>) {
    
    # retirer '\n'
    chomp;
    
    # gérer les accents
    s/[àäâ]/a/g;
    s/[ç]/c/g;
    s/[éèêë]/e/g;
    s/[îï]/i/g;
    s/[ôöœ]/o/g;
    s/[ûüù]/u/g;
    s/[ÿŷ]/y/g;

    
    # décomposer le mot en un tableau de lettres
    my @lettres = split //;
    
    # compter chaques caratères
    foreach my $c (@lettres) {

        if (exists $alpha{$c}) {
            $alpha{$c}++;
        }
        else { # 1ère fois
            $alpha{$c} = 1;
        } 
    }
}

close DICT;

# calcul de la somme 
my $somme = 0;

foreach my $c (sort keys %alpha) {
    if ( $c =~ /[a-z]/) {
        $somme += $alpha{$c};
    }
}

# afficher le résultat
print "\nNombre total de cacatères: $somme\n\n";

foreach my $c (sort keys %alpha) {
    if ( $c =~ /[a-z]/) {
        my $nb = $alpha{$c};
        $nb =~ s/^(\d+)(\d{3})$/$1.$2/;
        my $len1 = length($nb);
        my $percent = sprintf("%.2f", $alpha{$c} / $somme * 100 );
        my $len2 = length($percent);
        
        print " $c:" . ' ' x (8 - $len1) . $nb . ' ' x (8 - $len2)  . $percent . " %\n";
    }
}



