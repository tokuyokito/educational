#!/usr/bin/perl -w
use strict;

while (my $ligne = <>) {
	
	chomp $ligne;
	
	$ligne =~ s/#.*$//;
	next if($ligne =~ /\^s$/.);

	$ligne =~ s/[éèêë]/e/g;
	$ligne =~ s/[ÉÈÊË]/E/g;
	
	$ligne =~ s/[ôö]/o/g;
	$ligne =~ s/[ÔÖ]/O/g;
	
	$ligne =~ s/[îï]/i/g;
	$ligne =~ s/[ÎÏ]/I/g;

	my($nom,$prenom) = split ',', $ligne;
	print "prenom : $prenom, nom : $nom\n";
}


