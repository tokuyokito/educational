"use strict";

/* auteur      : Le Gall Cédric
 * version     : 2
 * description : Manipulation de canvas
 *
 */

//chargement de la page
 function initialisationCanvasVideos() {
	
	var video=document.getElementById("algore");
	
	var canvas1=document.getElementById("canvas1:1");
	var contextCanvas1=canvas1.getContext('2d');
	
	var canvas2=document.getElementById("canvas1:2");
	var contextCanvas2=canvas2.getContext('2d');

	var interval1;
	var interval2;

	video.addEventListener('play',function() {interval1=window.setInterval(function() {contextCanvas1.drawImage(video,0,0,576,384)},20);},false);
	video.addEventListener('pause',function() {window.clearInterval(interval1);},false);
	video.addEventListener('ended',function() {clearInterval(interval1);},false);

	video.addEventListener('play',function() {interval2=window.setInterval(function() {contextCanvas2.drawImage(video,0,0,576,384)},20);},false);
	video.addEventListener('pause',function() {window.clearInterval(interval2);},false);
	video.addEventListener('ended',function() {clearInterval(interval2);},false);
}
