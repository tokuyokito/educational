/**
 * \file main.c
 *
 * \brief Programme de chiffrage avec une cké xor
 *
 * \author BTS SNIR 1 2017
 * \version 0.1
 * \date 17 novembre 2017
 *
 * Du point de vue de la cryptanalyse, le terme crypter est contesté
 * car ce n'est qu'un faux anglicisme de chiffrer alors que le terme décrypter,
 * quant à lui, signifie « déchiffrer sans posséder la clé secrète ».
 * De plus, le synonyme classique chiffrer prévaut, pour certains,
 * sur le faux anglicisme crypter.
 *
 * Compilaion: gcc -o chiffrage-xor chiffrage-xor.c -Wall
 */

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <locale.h>
#include <string.h>

/*
 * ext4 est le successeur du système de fichiers ext3, principalement
 * destiné aux systèmes basés sur GNU/Linux. Wikipédia
 * Taille maximale du nom de fichiers : 255 octets
 * Taille maximale de fichier : 16 Tio
 * Nombre maximal de fichiers : 4 milliards
 * Allocation de fichiers : extent, bitmap
 * Taille maximale de volume : 1 Eio (limité à 16Tio par e2fsprogs)
 * Nom anglais : Fourth extended file system
 */
#define T_FILE 256
#define SUFFIXE ".secret"

typedef unsigned int index_t;

/**
 * \fn char* get_name(char* long_name)
 * \brief Fonction renvoyant le nom court d'un fichier.
 *
 * \param[in] int tab[]: adresse de la chaine.
 *
 * \return[out] char*: adresse du nam court.
 */
char* get_name(char* long_name)
{
    char* cur;
    char* pos;

    cur = pos = long_name;

    while (*cur != '\0') {
        if (*cur == '/' || *cur == '\\') {
            pos = cur + 1;
        }

        ++cur;
    }

    return pos;
}

int main(int argc, char* argv[])
{
    FILE* file_in  = NULL;
    FILE* file_out = NULL;

    char fichier_chiffre[T_FILE] = {'\0'};

    int octet;
    char* p = NULL;

    /* localisation */
    setlocale(LC_ALL, "");

    /*
     * Usage !
     */
    if (argc != 3) {
        printf("\nUsage: %s <file> <key>\n", get_name(argv[0]));
        exit(1);
    }

    /* vérifier le débordement */
    if (strlen(get_name(argv[1]) + strlen(SUFFIXE) + 1) > 255) {
        printf("Nom du fichier trop long!\n");
        exit(2);
    }

    strcpy(fichier_chiffre, get_name(argv[1]));
    strcat(fichier_chiffre, SUFFIXE);

    /*
    printf("\n");
    printf("fichier d'origine: '%s'\n", argv[1]);
    printf("fichier chiffré..: '%s'\n", fichier_chiffre);
    printf("clé de chiffrage.: '%s'\n", argv[2]);
    */

    if ((file_in = fopen(argv[1], "r")) == NULL) {
        printf("Erreur: fopen(%s), %s\n", argv[1], strerror(errno));
        exit(3);

    }

    if ((file_out = fopen(fichier_chiffre, "w")) == NULL) {
        printf("Erreur: fopen(%s), %s\n", fichier_chiffre, strerror(errno));
        fclose(file_in);
        exit(4);

    }

    /* positionner p sur la clé */
    p = argv[2];

    while ((octet = fgetc(file_in)) != EOF) {
        fputc(octet ^ *p, file_out);
        ++p;

        /* bouclage sur la clé */
        if (*p == '\0') { 
            p = argv[2];
        }
    }

    fclose(file_in);
    fclose(file_out);

    return EXIT_SUCCESS;
}


