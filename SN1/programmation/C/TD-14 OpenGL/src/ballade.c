/********************************************************/
/*                     ballade.c                        */
/********************************************************/
/* Petite ballade dans un monde (reduit) en fil de fer  */
/********************************************************/

/* inclusion des fichiers d'entete Glut */

#include <stdio.h>
#include <stdlib.h>
#include <GL/glut.h>
#include <math.h>

void display();
void keyboard(unsigned char key,int x, int y);
void mousePress(int bouton,int state,int x,int y);
void MouseMotion(int x,int y);
void calcCosSinTable();
void testPosition();
void changePerspective();

float pz=0.0,px=0.0,Sin[360],Cos[360],theta=50;
int xold,r=0;


int main(int argc,char **argv)
{

  /* initialisation de glut et creation
     de la fenetre */
  glutInit(&argc,argv);
  glutInitDisplayMode(GLUT_RGB|GLUT_DOUBLE);
  glutInitWindowPosition(200,200);
  glutInitWindowSize(500,500);
  glutCreateWindow("29");

  /* Initialisation d'OpenGL */
  glClearColor(0.0,0.0,0.0,0.0);
  changePerspective();

  /* Precalcul des sinus et cosinus */
  calcCosSinTable();

  /* enregistrement des fonctions de rappel */
  glutDisplayFunc(display);
  glutKeyboardFunc(keyboard);
  glutMouseFunc(mousePress);
  glutMotionFunc(MouseMotion);

  /* Entre dans la boucle principale glut */
  glutMainLoop();
  return 0;
}

void display()
{
  /* effacement de l'image avec la couleur de fond */
  glClear(GL_COLOR_BUFFER_BIT);
  glLoadIdentity();
  
  
  /*Application des transfos de visualisation */
  glRotated(r,0.0,1.0,0.0);
  glTranslatef(-px,0.0,-pz);
  
  
  glPushMatrix();
  /* Dessin des objets */
  
  glTranslatef(0,1.5,0);
  glScalef(1.0,0.25,1.0);
  glColor3d(1,0,0);
  glutWireCube(20);
  
  glPopMatrix();
  glPushMatrix();
  glTranslated(5,-1,5);
  glRotated(-90,1,0,0);
  glColor3f(1.0,1.0,1.0);
  glutWireCone(1,2,20,1);
  
  glPopMatrix();
  glPushMatrix();
  glTranslated(-5,0,5);
  glutWireTorus(0.2,0.8,20,30);
  
  glPopMatrix();
  glPushMatrix();
  glTranslated(5,0,-5);
  glutWireCube(2);
  
  glPopMatrix();
  glTranslated(-5,0,-5);
  glutWireSphere(1,20,20);
  
  
  /* on force l'affichage du resultat */
  glFlush();
  glutSwapBuffers();
}


void keyboard(unsigned char key,int x, int y)
{
  switch (key)
    {
    case 'q':
      exit(0);
    case 'z':
      pz-=0.5*Cos[r];
      px+=0.5*Sin[r];
      testPosition();
      glutPostRedisplay();
      break;
    case 's':
      pz+=0.5*Cos[r];
      px-=0.5*Sin[r];
      testPosition();
      glutPostRedisplay();
      break;
    case 'b':
      theta+=1;
      if (theta>180)
	theta=180;
      changePerspective();
      glutPostRedisplay();
      break;
    case 'n':
      theta-=1;
      if (theta<0)
	theta=0;
      changePerspective();
      glutPostRedisplay();
      break;
    }
}



void mousePress(int bouton,int state,int x,int y)
{
  if (bouton==GLUT_LEFT_BUTTON&&state==GLUT_DOWN){
     xold=x;
  }
}



void MouseMotion(int x,int y)
{
  r+=x-xold;
  if (r>=360)
    r-=360;
  if (r<0)
    r=360+r;
  xold=x;
  glutPostRedisplay();
}



void calcCosSinTable()
{
  int i;
  for (i=0;i<360;i++){
    Sin[i]=sin(i/360.0*6.283185);
    Cos[i]=cos(i/360.0*6.283185);
  }
}



void testPosition()
{
  if (px>9.8)
    px=9.8;
  if (px<-9.8)
    px=-9.8;
  if (pz>9.8)
    pz=9.8;
  if (pz<-9.8)
    pz=-9.8;
}



void changePerspective()
{
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(theta,1.0,0.1,40.0);
  glMatrixMode(GL_MODELVIEW);
}

