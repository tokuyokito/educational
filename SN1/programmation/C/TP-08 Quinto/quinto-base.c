/* Nom du fichier: quinto.c
 * Auteur:
 * Révision: 1
 * Programme: jeu du quinto
 * Compilation: gcc -o quinto quinto.c -Wall
 */

#include <stdlib.h>
#include <stdio.h>

#define FAUX        0
#define VRAI        1

#define HAUTEUR     4
#define LARGEUR     4

// paramêtres: hauteur, largeur
// action....: vérifier si les coordonnées sont dans la grille
// retour....: vrai si est dans la grille, sinon faux
int est_dans_grille(int h, int l)
{
    // TODO...
    if (h < 4 && l < 4)
    {
    	//on est bien positionné dans le tableau
    	return VRAI;
    }
    else
    {
    	// Le teste à échoué on n'est plus dans le tableau
    	return FAUX;
    }
}

// paramètres: adresse d'un entier
// action....: modifier la valeur de l'entier: 1 si 0, sinon 0
// retour....: aucun
void inverser(int *p_case)
{
    // TODO...

} 


int main()
{
    // variables
    int     hauteur, largeur;   // dimension de la grille
    int     i, j;   // indexe
    int     essai;              // nombre d'essais
    int     nb_case_1;          // nombre de case à 1

    int     grille[HAUTEUR][LARGEUR];

    // initialiser la grille 0 
    // TODO...
 
    // nombre d'essais
    essai = 0;

    while (VRAI)
    {
        // nouvel essai
        essai++;

        // afficher les positions sur la largeur
        printf("\e[r\e[J\n");
        // afficher information
        printf("\e[33m ~=|[ Jeux du Quinto ]|=~\e[0m\n\n");

        printf("\e[34m         ");
        for (j = 0; j < LARGEUR; j++)
        {
            // affichage à partir de 1 pour le joueur
            printf("%d ", j + 1);
        }
        printf("\e[0m\n\n");

        // compter le nombres de case à 1 
        nb_case_1 = 0;

        // afficher la grille et les positions sur la hauteur
        // si 0 afficher O
        // si 1 afficher X
        for (i = 0; i < HAUTEUR; i++)
        {
            // comptage joueur à partir de 1
            printf("\e[34m     %2d\e[0m  ", i + 1);

            for (j = 0; j < LARGEUR; j++)
            {
                if (grille[i][j] == 0)
                    printf("\e[32mO\e[0m ");
                else
                {
                    printf("\e[33mX\e[0m ");
                    // compter le nombre de case à 1
                    nb_case_1++;
                }
            }
            printf("\n");
        }
        printf("\n");

        // si la grille est entièrement à 1 sortir de la boucle (break)
        // TODO...

        // demander la position à inverser
        printf("\e[34m   Essai %d\e[0m\n", essai);
        printf("\e[34m   Donner position hauteur : \e[33m");
        scanf("%d", &hauteur);
        printf("\e[34m   Donner position largeur : \e[33m");
        scanf("%d", &largeur);
        // correction entre index et comptage joueur
        hauteur--;
        largeur--;

        // vérifier la position et inverser nord, sud, est et ouest
        // la modification n'est prise en compte que si les coordonnées
        // données sont dans la grille
        if (est_dans_grille(hauteur, largeur) == FAUX)
        {
            printf("\e[31mCoordonnees hors grille!\e[0m\n");
        }
        else
        {
            // inverser case centrale
            // TODO...
            
            // et si elles existent...
            // nord
            // TODO...
            // sud
            // TODO...
            // est
            // TODO...
            // ouest
            // TODO...
        }
        printf("\n");
    }

    return EXIT_SUCCESS;
}
