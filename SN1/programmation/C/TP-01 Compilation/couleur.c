/* Nom du fichier: couleur.c
 * Auteur:
 * Révision: 1
 * Programme: Afficher les couleurs sur terminal
 * Compilation: gcc -o couleur couleur.c -Wall
 */

#include <stdio.h>
#include <stdlib.h>

int main()
{
    printf("\e[30mBonjour\e[0m\n");
    printf("\e[31mBonjour\e[0m\n");
    printf("\e[32mBonjour\e[0m\n");
    printf("\e[33mBonjour\e[0m\n");
    printf("\e[34mBonjour\e[0m\n");
    printf("\e[35mBonjour\e[0m\n");
    printf("\e[36mBonjour\e[0m\n");
    printf("\e[37mBonjour\e[0m\n");

    return EXIT_SUCCESS;
}
