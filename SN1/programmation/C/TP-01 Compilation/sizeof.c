#include <stdio.h>
#include <stdlib.h>

int main()
{
    printf("Entiers\n");
    printf("Taille d'un char....... %ld\n", sizeof(char));
    printf("Taille d'un short...... %ld\n", sizeof(short));
    printf("Taille d'un int........ %ld\n", sizeof(int));
    printf("Taille d'un long....... %ld\n", sizeof(long));
    printf("Taille d'un long long.. %ld\n", sizeof(long long));
    printf("\nFlottants\n");
    printf("Taille d'un float...... %ld\n", sizeof(float));
    printf("Taille d'un double..... %ld\n", sizeof(double));
    printf("Taille d'un long double %ld\n", sizeof(long double));
    printf("\nPointeurs\n");
    printf("Taille d'un char*....... %ld\n", sizeof(char*));
    printf("Taille d'un short*...... %ld\n", sizeof(short*));
    printf("Taille d'un int*........ %ld\n", sizeof(int*));
    printf("Taille d'un long*....... %ld\n", sizeof(long*));
    printf("Taille d'un long long*.. %ld\n", sizeof(long long*));
    printf("Taille d'un float*...... %ld\n", sizeof(float*));
    printf("Taille d'un double*..... %ld\n", sizeof(double*));
    printf("Taille d'un long double* %ld\n", sizeof(long double*));

    return EXIT_SUCCESS;
}

