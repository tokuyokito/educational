#include <stdio.h>
#include <stdlib.h>

int main()
{
    int nb = 0x00AABBCC;
    
    printf("A........ d'un nombre sous différentes bases\n"); 

    printf("Valeur de nb en base 10....: %d\n", nb);
    printf("Valeur de nb en base  8....: %o\n", nb);
    printf("Valeur de nb en base 16 min: %x\n", nb);
    printf("Valeur de nb en base 16 maj: %X\n", nb);

    return EXIT_SUCCESS;
}

