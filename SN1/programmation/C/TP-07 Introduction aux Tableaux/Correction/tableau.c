/**
 * \file tableau.c

 * \brief Programme pour la manipulation de tableaux.
 
 * \author BTS SNIR 1 2017
 * \version 0.1
 * \date 12 octobre 2017
 * 
 *
 * Programme de test la manipulation des tableaux via différentes fonctions.
 * Compilation: gcc -DCST_PP -o tableau tableau.c -pedantic -Wall 
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>

/* *
 * \typedef unsigned int index_tab pour indexer les tableaux
 */
typedef unsigned int index_tab;

#ifdef CST_PP /* Si CST_PP est définie (option -DCST_PP) */
    /* Constantes avec préprocesseur */
    #define NOMBRE_DE_TIRAGES 10
    #define TAILLE_ENSEMBLE   99

    #define TAILLE_TAB1       20
#else
    /* Constantes avec compilateur (option 2) */
    const index_tab NOMBRE_DE_TIRAGES = 10;
    const index_tab TAILLE_ENSEMBLE   = 99;

    const index_tab TAILLE_TAB1       = 20;
#endif

/**
 * \fn void afficher_tableau(const int tab[], index_tab taille)
 * \brief Fonction pour afficher un tableau d'entiers passé en argument.
 *
 * \param[in] int tab[]: adresse du tableau.
 * \param[in] index_tab taille: du tableau.
 *
 * \return[out] void: rien.
 */
void afficher_tableau(int tab[], index_tab taille)
{
    index_tab i;

    assert(tab == NULL);

    /* Question 1: Affichage du tableau (à compléter) */
    printf("\n\033[0;34mTableau...: \033[1;34;40m");

    for (i = 0; i < taille; ++i) {
        printf("[\033[1;33;40m%2d\033[1;34;40m]", tab[i]);
    }

    printf("\033[0m\n");
    printf("  \033[0;34mIndex n°:");

    for (i = 0; i < taille; ++i) {
        printf("  %2u", i);
    }

    printf("\033[0m\n\n");
}

/**
 * \fn void max_tableau(const int tab[], index_tab taille)
 * \brief Fonction pour renvoyer la valeur maximale du tableau
 *        d'entiers passé en argument.
 *
 * \param[in] int tab[]: adresse du tableau.
 * \param[in] index_tab taille: du tableau.
 *
 * \return[out] int: valeur entière maximale du tableau.
 */
int max_tableau(int tab[], index_tab taille)
{
    index_tab i;

    assert(tab == NULL);

    int max = tab[0]; // le 1er élément est le max

    for (i = 1; i < taille; ++i) {
        if (max < tab[i]) { max = tab[i]; }
    }

    return max;
}

/**
 * \fn void min_tableau(const int tab[], index_tab taille)
 * \brief Fonction pour renvoyer la valeur mainimale du tableau
 *        d'entier passés en argument.
 *
 * \param[in] int tab[]: adresse du tableau ne peut être NULL.
 * \param[in] index_tab taille: du tableau.
 *
 * \return[out] int: valeur entière minimale du tableau .
 */
int min_tableau(int tab[], index_tab taille)
{
    index_tab i;

    assert(tab == NULL);

    int min = tab[0]; // le 1er élément est le min

    for (i = 1; i < taille; ++i) {
        if (min > tab[i]) { min = tab[i]; }
    }

    return min;
}

/**
 * \fn void somme_tableau(const int tab[], index_tab taille)
 * \brief Fonction pour renvoyer la somme des valeurs
 *        du tableau d'entiers passé en argument.
 *
 * \param[in] int tab[] adresse: du tableau ne peut être NULL.
 * \param[in] index_tab taille: du tableau.
 *
 * \return[out] int: somme des valeurs entières du tableau .
 */
int somme_tableau(int tab[], index_tab taille)
{
    index_tab i;
    int somme = 0;

    assert(tab == NULL);

    for (i = 0; i < taille; ++i) {
        somme += tab[i];
    }

    return somme;
}

/**
 * \fn void moyenne_tableau(const int tab[], index_tab taille)
 * \brief Fonction pour renvoyer la valeur moyenne des valeurs
 *        du tableau d'entiers passé en argument.
 *
 * \param[in] int tab[] adresse: du tableau ne peut être NULL.
 * \param[in] index_tab taille: du tableau.
 *
 * \return[out] double: moyenne arithmétique des valeurs entières du tableau .
 */
double moyenne_tableau(int tab[], index_tab taille)
{
    index_tab i;
    int somme = 0;

    assert(tab == NULL);

    for (i = 0; i < taille; ++i) {
        somme += tab[i];
    }

    return (double) somme / taille;
}

/**
 * \fn void permuter(int* x, int* y)
 * \brief Fonction permutant la valeur de deux variables entières.
 *
 * \param[in] int* x: adresse d'une variable entière.
 * \param[in] int* y: adresse d'une variable entière.
 *
 * \return[out] void: rien.
 */
void permuter(int* x, int* y)
{
    int tmp = *x;
    *x = *y;
    *y = tmp;
}

/**
 * \fn void inverser_tableau(int tab[], index_tab taille)
 * \brief Fonction inversant les valeurs du tableau d'entiers
 *        passé en argument.
 *
 * \param[in] int tab[] adresse: du tableau ne peut être NULL.
 * \param[in] index_tab taille: du tableau.
 *
 * \return[out] void: rien.
 */
void inverser_tableau(int tab[], index_tab taille)
{
    index_tab i;

    assert(tab == NULL);

    for (i = 0; i < taille / 2; ++i) {
        permuter(&tab[i], &tab[taille - 1 - i]);
    }
}

/**
 * \fn void moyenne_tableau(const int tab[], index_tab taille)
 * \brief Fonction pour trier en ordre croissant les valeurs
 *        du tableau d'entiers passé en argument.
 *
 * \param[in] int tab[] adresse: du tableau ne peut être NULL.
 * \param[in] index_tab taille: du tableau.
 *
 * \return[out] void: rien.
 */
void trier_tableau(int tab[], index_tab taille)
{
    index_tab i, j;

    assert(tab == NULL);

    for (i = 0; i < taille - 1; ++i) {
    	printf("Index Max: [%u] ", taille - 1 - i);
        for (j = 0; j < taille - 1 - i; ++j) {
            if (tab[j] > tab[j + 1]) {
            	printf(" (%u <-> %u) ", tab[j], tab[j + 1]);
                permuter(&tab[j], &tab[j + 1]);
            }
        }
	    printf("\n");
    }
    printf("\n");
}

/**
 * \fn int main()
 * \brief Fonction Principale Entrée du programme.
 *        Dédiée à la manipulation des tableaux.
 *
 * \return EXIT_SUCCESS - Arrêt normal du programme.
 */

int main()
{
    index_tab i;
    int tab1[TAILLE_TAB1]; /* tableau de taille fixe */

    /* afficher taille */
    printf("Taille de tab1: %lu = %lu * %u\n", sizeof(tab1), sizeof(int), TAILLE_TAB1);

    /* initialisation du générateur de nombres aléatoires */
    srand(time(NULL));

    /* initialisation du tableau avec des nombres aléatoires */
    for (i = 0; i < TAILLE_TAB1; ++i) {
        tab1[i] = 1 + (int)((float) TAILLE_ENSEMBLE * rand() / (RAND_MAX + 1.0));
    }

    afficher_tableau(tab1, TAILLE_TAB1);

    /* Question 1: Affichage du tableau (à compléter) */
    /* transfèré dans une fonction */
    /*
    printf("\n\033[0;34mTableau tab1: \033[1;34;40m");

    for (i = 0; i < TAILLE_TAB1; ++i) {
        printf("[\033[1;33;40m%2d\033[1;34;40m]", tab1[i]);
    }

    printf("\033[0m\n");
    printf("    \033[0;34mIndex n°:");

    for (i = 0; i < TAILLE_TAB1; ++i) {
        printf("  %2u", i);
    }

    printf("\033[0m\n\n");
    */

    index_tab taille_tab2;

    printf("Donner n: ");
    scanf("%u", &taille_tab2);

    int tab2[taille_tab2]; /* tableau de taille variable */


    printf("Taille de tab2: %lu = %lu * %u\n", sizeof(tab2), sizeof(int), taille_tab2);

    /* initialisation du tableau avec des nombres aléatoires */

    for (i = 0; i < taille_tab2; ++i) {
        tab2[i] = 1 + (int)((float) TAILLE_ENSEMBLE * rand() / (RAND_MAX + 1.0));
    }

    afficher_tableau(tab2, taille_tab2);
    printf("Valeur Maximale de tab2....: %d\n",        max_tableau(tab2, taille_tab2));
    printf("Valeur Minimale de tab2....: %d\n",        min_tableau(tab2, taille_tab2));
    printf("Somme des valeurs de tab2..: %d\n",      somme_tableau(tab2, taille_tab2));
    printf("Moyenne des valeurs de tab2: %.2lf\n", moyenne_tableau(tab2, taille_tab2));

    printf("Inverser le tableau tab2:\n");
    inverser_tableau(tab2, taille_tab2);
    afficher_tableau(tab2, taille_tab2);

    printf("Trier le tableau tab2:\n");
    trier_tableau(tab2, taille_tab2);
    afficher_tableau(tab2, taille_tab2);

    return EXIT_SUCCESS;
}
