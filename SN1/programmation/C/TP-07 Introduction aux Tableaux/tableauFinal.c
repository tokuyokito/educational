/*
 * Auteur: Le Gall Cédric
 *
 * programme de manipulation de tableaux
 * site utile : https://www.admin-linux.fr/bash-de-la-couleur-dans-le-shell/
 * Compilation: gcc -o tableauFinal tableauFinal.c -Wall -lm
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MAXI_VALEUR 100


int sommeTableau(int tableaux[], int tailleTableau)
{
    int i;
    int somme = 0;

    for (i = 0; i < tailleTableau; ++i) {
        somme = somme + tableaux[i];
    }

    return somme;
}

int moyenneTableau(int tableaux[], int tailleTableau)
{
    int moyenne = sommeTableau(tableaux, tailleTableau);

    return moyenne / tailleTableau;
}

void afficheTableau(int tableaux[], int tailleTableau)
{
    int i;

    for (i = 0; i < tailleTableau; ++i) {
        printf(" la case n° %d contient: [%d]\n\n ", i + 1, tableaux[i]);
    }
}

void commandes()
{
    //affiche les commandes
    printf("Touches pour navigé dans le tableau : \n");
    printf("saisir [c] : \e[4mcopier\e[0m le tableau\n");
    printf("saisir [s] : \e[4meffectué la somme\e[0m des nombres du tableaux\n");
    printf("saisir [m] : \e[4meffectué la moyenne\e[0m du tableau\n");
    printf("saisir [t] : \e[4mtrouvé le nombre le plus grand \e[0m du tableau\n");
    printf("saisir [i] : \e[4minversé les chiffres\e[0m du tableau\n");
    printf("saisir [q] : \e[4mquitté\e[0m le programme\n\n");
}


int main()
{


    printf("Programme qui permet de definir la taille d'un tableau, dont le contenue est initialisé de maniére aléatoire.\n\n" );

    printf("Définir la taille du tableau : ");
    int tailleTableau;
    scanf("%d", &tailleTableau);
    printf("\nVous avez définie un tableau de %d cases !\n\n", tailleTableau);

    // Valeurs utile a l'initialisation du tableaux

    int tableau[tailleTableau];
    int navigationTableau = 0;
    int attendre;

    //initialisation tableau

    for (navigationTableau = 0; navigationTableau < tailleTableau; ++navigationTableau) {

        tableau[navigationTableau] = 1 + (int) ((float) MAXI_VALEUR * rand() / (RAND_MAX + 1.0));


        for (attendre = 0; attendre < 5; ++ attendre) {
            // boucle crée pour ne pas obtenir un nombre aléatoire identique
        }
    }

    //fin initialisation tableau

    afficheTableau(tableau, tailleTableau);

    char saisie;

    //permet d'appelé les fonctions
    do {

        commandes();
        // NE DECTECTE PAS LA PREMIERE SELECTION, ON EST OBLIGER DE SAISIR DEUX FOIS LA VALEUR ATTENDUE
        printf("appuyer sur une touche : ");
        saisie = getchar();

        while (getchar() != '\n');

        printf("\n");

        switch (saisie) {
        case 'c':
        case 'C':

            printf("\e[32mActivation\e[0m de la fonction \e[4mcopier\e[0m\n\n");
            break;

        case 's':
        case 'S':

            printf("\e[32mActivation\e[0m de la fonction \e[4msomme\e[0m\n\n");

            int somme = sommeTableau(tableau, tailleTableau);
            printf("La somme des nombres du tableaux est de : %d\n\n", somme);
            break;

        case 'm':
        case 'M':

            printf("\e[32mActivation\e[0m de la fonction \e[4mmoyenne\e[0m\n\n");

            int moyenne = moyenneTableau(tableau, tailleTableau);
            printf("La moyenne des valeurs du tableau est de : %d\n\n", moyenne);
            break;

        case 't':
        case 'T':

            printf("\e[32mActivation\e[0m de la fonction \e[4mmax\e[0m\n\n");
            break;

        case 'i':
        case 'I':

            printf("\e[32mActivation\e[0m de la fonction \e[4minversé\e[0m\n\n");
            break;

        case 'q':
        case 'Q':

            printf("Fin du programme.\n");
            break;

        default:

            printf("\e[31m\e[4mCommande inconnu: %02X!\e[0m \n\n", saisie);
            break;

        }

    }
    while (saisie != 'q' && saisie != 'Q');


    return EXIT_SUCCESS;
}
