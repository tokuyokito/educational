#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <locale.h>
#include <string.h>

/**
 * \fn char* get_name(char* long_name)
 * \brief Fonction renvoyant le nom court d'un fichier.
 *
 * \param[in] int tab[]: adresse de la chaine.
 *
 * \return[out] char*: adresse du nam court.
 */
char* get_name(char* long_name)
{
    char* cur;
    char* pos;

    cur = pos = long_name;

    while (*cur != '\0') {

        if (*cur == '/' || *cur == '\\') {
            pos = cur;
        }

        ++cur;
    }

    /* Si aucun '/' ou '\' n'existe dans long_name */
    if (pos == long_name) {
        return pos;
    }

    /* position après '/' ou '\' */
    return pos + 1;
}

int main(int argc, char* argv[])
{
    int octet, comparaison;
    char compteur = 0;
    FILE* file_in;
    FILE* file_out;
	
	char nom[100] = "tempo"; //nom temporaire

    if (argc != 2) {

        printf("Usage <repertoire fichier>\n");

        exit(1);
    }

    file_in = fopen(argv[1], "r");
    file_out = fopen(nom, "w");
    
    if (file_in == NULL) {

        printf("Erreur ouverture fichier\n");
        exit(1);
    }
 
    while ((octet = fgetc(file_in)) != EOF) {
    	if (compteur == 1)
    	{
    		comparaison = octet;
    	}
    	
    	++compteur;

    	if (comparaison != octet)
    	{
    		fputc(compteur, file_out);
    		fputc(comparaison, file_out);

    		compteur = 1;
    		
    	}

    }
    printf("c : %d\n",compteur );
    
    fclose(file_in);
    fclose(file_out);

    return EXIT_SUCCESS;
}


