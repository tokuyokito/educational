char *char2bin(char c)
{
    static char buffer[9] = {'0', '0', '0', '0', '0', '0', '0', '0', '\0'};

    char *p = buffer + 8;
	int i;
    
	for (i = 0; i < 8; i++)
	{
        *--p = '0' + (c & 1);
        c >>= 1;
    }

    return buffer;
}

