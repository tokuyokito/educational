/* Nom du fichier: somme.c
 * Auteur:
 * Révision: 1
 * Programme: Demander un nombre N et calcule la somme 1+2+3..+N
 * Compilation: gcc -o somme somme.c -Wall
 */

#include <stdio.h>
#include <stdlib.h>

int main()
{
    int n;

    printf("Donner un nombre: ");
    scanf("%d", &n);
    
    if (n < 1)
    {
        printf("N doit être supérieur à 1\n");
        return -1;
    }

    printf("Somme de %d = %d\n", n, n * (n + 1) / 2);

    return EXIT_SUCCESS;
}
