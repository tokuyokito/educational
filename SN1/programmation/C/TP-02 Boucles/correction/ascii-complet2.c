/* Nom du fichier: ascii.c
 * Auteur:
 * Révision: 1
 * Programme: Demander un nombre N et calcule la somme 1+2+3..+N
 * Compilation: gcc -o ascii acsii.c -Wall
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *char2str(char c)
{
    static char buffer[9];

    if (c < 33 || c == 127)
	{
		switch (c) {
		case  0:
			strcpy(buffer, "NUL: Null                      '\\0'");
			break;
		case  1:
			strcpy(buffer, "SOH: Start Of Header               ");
			break;   
		case  2:
			strcpy(buffer, "STX: Start of Text                 ");
			break;
		case  3:
			strcpy(buffer, "EXT:                               ");
			break;
		case  4:
			strcpy(buffer, "EOT: End Of Transmission           ");
			break;
		case  5:
			strcpy(buffer, "ENQ: Enquiry                       ");
			break; 
		case  6:
			strcpy(buffer, "ACK: Acknowledge                   ");
			break;
		case  7:
			strcpy(buffer, "BEL: Bell                      '\\a'");
			break;
		case  8:
			strcpy(buffer, "BS : Backspace                 '\\b'");
			break; 
		case  9:
			strcpy(buffer, "TAB: Horizontal Tabulation     '\\t'");
			break;
		case 10:
			strcpy(buffer, "LF : Line Feed                 '\\n'");
			break;
		case 11:
			strcpy(buffer, "VT : Vertical Tabulation       '\\v'");
			break; 	
		case 12:
			strcpy(buffer, "FF : Form Feed                 '\\f'");
			break;
		case 13:
			strcpy(buffer, "CR : Carriage Return           '\\r'");
			break;
		case 14:
			strcpy(buffer, "SO : Shift Out                     ");
			break; 	
		case 15:
			strcpy(buffer, "SI : Shift In                      ");
			break;
		case 16:
			strcpy(buffer, "DLE: Data Link Escape              ");
			break; 	
		case 17:
			strcpy(buffer, "DC1: Device Control 1, XON         ");
			break;
		case 18:
			strcpy(buffer, "DC2: Device Control 2              ");
			break;
		case 19:
			strcpy(buffer, "DC3: Device Control 3, XOFF        ");
			break;
		case 20:
			strcpy(buffer, "DC4: Device Control 4              ");
			break;
		case 21:
			strcpy(buffer, "NAK: Negative Acknowledge          ");
			break;
		case 22:
			strcpy(buffer, "SYN: Synchronous idle              ");
			break;
		case 23:
			strcpy(buffer, "ETB: End of Transmission Block     ");
			break;
		case 24:
			strcpy(buffer, "CAN: Cancel                        ");
			break;
		case 25:
			strcpy(buffer, "EM : End of médium                 ");
			break;
		case 26:
			strcpy(buffer, "SUB: Substitute                    ");
			break;
		case 27:
			strcpy(buffer, "ESC: Escape                        ");
			break;
		case 28:
			strcpy(buffer, "FS : File Separator                ");
			break;
		case 29:
			strcpy(buffer, "GS : Group Separator               ");
			break;
		case 30:
			strcpy(buffer, "RS : Record Separator              ");
			break;
		case 31:
			strcpy(buffer, "US : Unit Separator                ");
			break;
		case 32:
			strcpy(buffer, "SP : Space                      ' '");
			break;
		case 127:
			strcpy(buffer, "DEL:                               ");
			break;

		default:
			strcpy(buffer, "                               '\\?'");
		}
	}
	else
	{
		sprintf(buffer, "                                '%c'", c);
	}

    return buffer;
}

char *char2bin(char c)
{
    static char buffer[9] = {'0', '0', '0', '0', '0', '0', '0', '0', '\0'};

    char *p = buffer + 8;
	int i;
    
	for (i = 0; i < 8; i++)
	{
        *--p = '0' + (c & 1);
        c >>= 1;
    }

    return buffer;
}

int main()
{
	unsigned char i;

	// Titre du tableau
	printf("caractère                           décimal  octal   hexa   binaire\n");
 
    i = 0;
    while (i < 128)
    {
        printf("\e[32m%s     \e[33m%3u   \e[34m0%03o   \e[35m0x%02x   \e[36m%s\e[0m\n", char2str(i), i, i, i, char2bin(i));
        ++i;
    }

    return EXIT_SUCCESS;
}
