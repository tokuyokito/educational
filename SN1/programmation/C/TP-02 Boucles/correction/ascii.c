/* Nom du fichier: ascii.c
 * Auteur:
 * Révision: 1
 * Programme: Demander un nombre N et calcule la somme 1+2+3..+N
 * Compilation: gcc -o ascii acsii.c -Wall
 */

#include <stdio.h>
#include <stdlib.h>

int main()
{
    unsigned char i;

    i = 0;
    while (i < 127)
    {
        if (i > 31)
            printf("caractère %c %3u 0%3o 0x%2x\n", i, i, i, i);
        else
            printf("caractère   %3u 0%3o 0x%2x\n", i, i, i);
        ++i;
    }


    return EXIT_SUCCESS;
}
