/* Nom du fichier: racine.c
 * Auteur:
 * Révision: 1
 * Programme: Demander un nombre réel >= 0 et calcule la racine
 * Compilation: gcc -o racine racine.c -Wall -lm
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{
    double nb;

    printf("Donner un nombre: ");
    scanf("%lf", &nb);
    
    if (nb < 0)
    {
        printf("N doit être supérieur à 0\n");
        return -1;
    }

    printf("Racine de %.4lf = %.6lf\n", nb, sqrt(nb));

    return EXIT_SUCCESS;
}
