/* Nom du fichier: bonjour.c
 * Auteur:
 * Révision: 1
 * Programme: Afficher "Bonjour"
 * Compilation: gcc -o bonjour bonjour.c -Wall
 */

#include <stdio.h>
#include <stdlib.h>

int main()
{
    printf("Bonjour\n");

    return EXIT_SUCCESS;
}
