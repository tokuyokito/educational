/* Nom du fichier: somme.c
 * Auteur:
 * Révision: 1
 * Programme: Demander un nombre N et calcule la somme 1+2+3..+N
 * Compilation: gcc -o somme somme.c -Wall
 */

#include <stdio.h>
#include <stdlib.h>

int main()
{
    int n;
    int i;
    int somme;

    printf("Donner un nombre: ");
    scanf("%d", &n);
    
    if (n < 1)
    {
        printf("N doit être supérieur à 1\n");
        return -1;
    }

    somme = 0;
    i = 1;
    while (i <= n)
    {
        somme += i;
        ++i;
    }

    printf("Somme de %d = %d\n", n, somme);

    return EXIT_SUCCESS;
}
