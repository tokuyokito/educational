/************************************************************
* nom : Le gall                                             *
* prenom : cédric                                           *
* objectif : resolution d'une équation du second degrée	    *
* version : 3	                                            *
* compilation : gcc -o 2ndDegre 2ndDegre.c -Wall -lm 		*
* defauts resolu : scanf("%lf", %b); espace entre lf et )   *
************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int main()
{

	printf("Algorithme de résolution d'équation du second degré\n");

	// saisie de a, b et c
	double a, b, c;

	printf("saisir a : ");
	scanf("%lf", &a);

	if (a == 0.0)
	{
		printf("a doit être différent de 0\n");
		printf("Relancer le programme");
		exit(1);
	}

	printf("saisir b : ");
	scanf("%lf", &b);

	printf("saisir c : ");
	scanf("%lf", &c);

	//calcul de delta
	double delta, racineDelta;
	delta = pow(b, 2) - 4 * (a) * (c);
	racineDelta = sqrt(delta);
	printf("a = %.2lf, b = %.2lf, c = %.2lf, delta = %.2lf\n", a, b, c, delta );

	// affichage delta
	double solution, solution1, solution2;
	if (delta > 0.0)
	{

		solution1 = -1 * (b + racineDelta) / 2 * a;
		solution2 = -1 * (b - racineDelta) / 2 * a;
		printf("solution1 = %.2lf\n", solution1 );
		printf("solution2 = %.2lf\n", solution2 );
	}
	else if (delta == 0.0)
	{

		solution = -b / 2 * a;
		printf("La solution est de : %lf", solution);
	}
	else
	{
		printf("Pas de solutions réels.");
	}


	return EXIT_SUCCESS;
}


