/*
compilation : gcc -o mastermind mastermind.c -Wall
*/



#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define VRAI 	      !FAUX
#define FAUX 	      0

#define DEBUG 	      VRAI

#define TAILLE_GRILLE 4

#define MAXI 		  8

#define PION_NOIR 	  1
#define PION_BLANC	  2

#define ESSAIS        12

//ne reçois rien
//affiche une notice du jeu mastermind
//ne renvoie rien
void notice()
{
	printf("Voulez voir la notice du jeu ?\n\n");
	printf("Si oui taper : o si non taper n : ");

	char reponse = getchar();
	while (getchar() != '\n');

	char attendre[20];

	switch (reponse)
	{
	case 'N':
	case 'n':
		printf("\nlancement de la partie\n");
		break;

	case 'O':
	case 'o':

		printf("\nLe Master Mind est un jeu de société, de réflexion, et de déduction, inventé par Mordecai Meirovitz dans les années 1960.\n\n");
		printf("Avant de démarrer la partie, l'ordinateur va placer ses pions de couleurs (parmi celles proposées).\n");
		printf("Le but est de retrouver quels sont les 4 pions choisis par l'ordinateur et d'en connaître les positions.\n");
		printf("Pour cela, à chaque tour, le joueur doit se servir des pions de couleurs pour former une rangée et se rapprocher le plus possible de la solution (ou de la trouver) Il a pour cela 12 tours.\n");
		printf("Le nombre de rangées pour trouver la solution est limité. Une fois les pions placés, l'ordinateur indiquera :\n\n");
		printf("1. le nombre de pions de bonne couleur bien placés en utilisant les petits pions noirs\n");
		printf("2. le nombre de pions de bonne couleur mais mal placés avec les petits pions blancs.\n\n");
		printf("Chaque couleurs a ça correspondence en chiffre. Il suffit de saisir un chiffre pour saisir une couleur : \n\n");

		printf("appuyer sur q pour sortir : ");
		scanf("%s", attendre);
		break;

	default:
		printf("Erreur saisi incorrect");
		break;

	}

}

//ne reçois rien
//affiche du texte
//ne renvoie rien
void affichage()
{

	printf("Couleurs : [rouge : 1| jaune : 2| vert : 3| bleu : 4| orange : 5|  blanc : 6| violet : 7| fuchsia : 8 ]\n\n");
}

//reçois un tableau et ça taille en argument
//initialise le tableau avec des valeurs comprise entre [1 ; 8]
//ne renvoie rien
void initialisation_grille(char tab[], int taille)
{

	srand(time(NULL));

	int i = 0;

	for ( i = 0; i < TAILLE_GRILLE; ++i) {
		tab[i] = 1 + (int) ((float) MAXI * rand() / (RAND_MAX + 1.0));
	}

 #if DEBUG == VRAI

	printf("\nCOULEUR A TROUVER DEBUG MODE\n\n");
	for ( i = 0; i < TAILLE_GRILLE; ++i) {

		printf("couleur[%d] = %d\n", i + 1, tab[i] );
	}

	printf("\n\n");

 #endif
}

//reçois un tableau et ça taille en argument
//initialise le tableau suivant la saisie du joueur
//ne renvoie rien
void completion_grille(char tab[], char taille)
{
	int i, saisi = 0;

	//saisie des couleurs dans le tableau
	for ( i = 0; i < taille; ++i) {

		printf("saisi de la %d couleur : ", i + 1);

		scanf("%d", &saisi);

		tab[i] = saisi;
	}

}

//reçois en argument : des tableau , l'adresse des pions noir et blanc, la taille des tableaux
//compare  la saisie du joueur avec la grille du jeu
//renvois le nombre de pion blanc et noir
void comparaison_grille_joueur_grille_cache(char saisie_joueur[], char grille_cache[], char grille_comparaison[], int *noir, int *blanc, int taille)
{
	int i, j;
	*noir = 0, *blanc = 0; // remise à zéro des pions

	//initialisation de la grille de comparaison
	for (i = 0; i < taille; ++i)
	{
		grille_comparaison[i] = 0;
	}

	//pion noir
	for ( i = 0; i < taille; ++i)
	{
		if ( saisie_joueur[i] == grille_cache[i]) {

			++*noir;
			grille_comparaison[i] = PION_NOIR;
		}
	}

	//pion blanc
	for ( i = 0; i < taille; ++i) {

		for (j = 0; j < taille; ++j) {

			if ((saisie_joueur[i] == grille_cache[j] && grille_comparaison[j] == 0)) {

				grille_comparaison[j] = 2;
				++*blanc;
				break;
			}
		}
	}


 #if DEBUG == VRAI

	printf("\n\nAFFICHAGE GRILLE_COMPARAISON DEBUG MODE\n\n");

	for ( i = 0; i < taille; ++i)
	{
		printf("grille_comparaison[%d] : %d\n", i + 1, grille_comparaison[i]);
	}
	printf("\nnombre de pion noir : %d et de pion blanc : %d\n", *noir, *blanc);
	printf("\n\n");
 #endif
}

int main()
{
	char saisie_joueur[TAILLE_GRILLE], grille_cache[TAILLE_GRILLE], grille_comparaison[TAILLE_GRILLE]; // diférentes grille

	int essai = 0, noir = 0, blanc = 0, i;

	notice();

	initialisation_grille(grille_cache, TAILLE_GRILLE); //initialisation de la grille de jeu

 #if DEBUG == FAUX

	printf("\033[H\033[J"); // renitialise le terminal
 #endif

	do {

		affichage(); //affiche les consignes pour le joueur

		completion_grille(saisie_joueur, TAILLE_GRILLE);//saisie des couleurs

		comparaison_grille_joueur_grille_cache(saisie_joueur, grille_cache, grille_comparaison, &noir, &blanc, TAILLE_GRILLE);

		printf("\n****************************************************\n");
		printf("\t\tessais n°%d\n\n", essai + 1 );

		for ( i = 0; i < TAILLE_GRILLE; ++i)
		{
			printf("%2d", saisie_joueur[i]);
		}

		printf("   nombre de pion noir : %d et blanc : %d\n", noir, blanc);
		printf("****************************************************\n\n");

		if (noir == 4)
		{
			printf("Vous avez gagné en : %d coup !!!\n", essai + 1);
			break;
		}

	essai++;
	
	} while (essai < ESSAIS);

	if (noir != 4)
	{
		printf("Vous avez perdue !\n la solution etais ");
		
		for ( i = 0; i < TAILLE_GRILLE; ++i)
		{
			printf("%d ", grille_cache[i]);
		}
	}

	return EXIT_SUCCESS;
}
