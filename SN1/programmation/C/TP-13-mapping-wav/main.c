/**
 * \file main.c
 *
 * \brief
 *
 * \author
 * \version
 * \date
 *
 * Compilaion:
 */

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <locale.h>
#include <string.h>

#define VRAI 0
#define FAUX !VRAI

#define DEBUG


typedef struct
{
	int   file_type_block_id;
	int   fileSize;
	int   file_format_id;
	int   format_bloc_id;
	int   bloc_size;
	short audio_format;
	short nbr_canaux;
	int   frequence;
	int   byte_per_sec;
	short byte_per_bloc;
	short bits_per_sample;
	int   data_bloc_id;
	int   data_size;

} Format_wav;


/**
 * \fn int main(int argc, char* argv[])
 * \brief Fonction principal
 *
 * \param[in]
 *
 * \return[out]
 */
int main(int argc, char* argv[])
{
	FILE* fichier_a_analyse;
	Format_wav wav;
	
	char buffer[43];
	int i;

	setlocale(LC_ALL, "");

	if (argc != 2) {

		printf("Usage %s <fichier a analyse>\n", argv[0]);
		exit(1);
	}

    /*
	 *Test ouverture fichier à annalysé
     *Si reussi copie des 44 premiers octets du fichier
     *puis fermeture du flux
	 */
	if ((fichier_a_analyse = fopen(argv[1], "r")) == NULL)
	{
		printf("%s\n", strerror(errno));
		exit(2);
	}

	for ( i = 0; i < 44; ++i)
	{
		buffer[i] = fgetc(fichier_a_analyse);
	}
	
	fclose(fichier_a_analyse);

	memcpy(&wav, buffer, sizeof(wav));
	
	printf("***********************************************************\n");
	printf("  nom          valeur\n\n");
	printf("FileTypeBlocID |%d|\n", wav.file_type_block_id);
	printf("FileSize       |%d|\n", wav.fileSize);
	printf("FileFormatID   |%d|\n", wav.file_format_id);
	printf("FormatBlocID   |%d|\n", wav.format_bloc_id);
	printf("BlocSize       |%d|\n", wav.bloc_size);
	printf("AudioFormat    |%d|\n", wav.audio_format);
	printf("NbrCanaux      |%d|\n", wav.nbr_canaux);
	printf("Frequence      |%d|\n", wav.frequence);
	printf("BytePerSec     |%d|\n", wav.byte_per_sec);
	printf("BytePerBloc    |%d|\n", wav.byte_per_bloc);
	printf("BitsPerSample  |%d|\n", wav.bits_per_sample);
	printf("DataBlocID     |%d|\n", wav.data_bloc_id);
	printf("DataSize       |%d|\n", wav.data_size);
	printf("*************************************************************\n");
	return EXIT_SUCCESS;
}

