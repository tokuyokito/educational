/**
 * \file libtableau.c

 * \brief Programme pour la manipulation de tableaux.
 
 * \author BTS SNIR 1 2017
 * \version 0.1
 * \date 12 octobre 2017
 * 
 *
 * Programme de test la manipulation des tableaux via différentes fonctions.
 * Compilation: gcc -DCST_PP -o tableau tableau.c -pedantic -Wall 
 */

#include "lib_tableau.h"

/**
 * \fn void afficher_tableau(const int tab[], index_tab taille)
 */
void afficher_tableau(int tab[], index_tab taille)
{
    index_tab i;

    assert(tab != NULL);

    /* Question 1: Affichage du tableau (à compléter) */
    printf("\n\033[0;34mTableau...: \033[1;34;40m");

    for (i = 0; i < taille; ++i) {
        printf("[\033[1;33;40m%2d\033[1;34;40m]", tab[i]);
    }

    printf("\033[0m\n");
    printf("  \033[0;34mIndex n°:");

    for (i = 0; i < taille; ++i) {
        printf("  %2u", i);
    }

    printf("\033[0m\n\n");
}

/**
 * \fn void max_tableau(const int tab[], index_tab taille)
 */
int max_tableau(int tab[], index_tab taille)
{
    index_tab i;

    assert(tab != NULL);

    int max = tab[0]; // le 1er élément est le max

    for (i = 1; i < taille; ++i) {
        if (max < tab[i]) { max = tab[i]; }
    }

    return max;
}

/**
 * \fn void min_tableau(const int tab[], index_tab taille)
 */
int min_tableau(int tab[], index_tab taille)
{
    index_tab i;

    assert(tab != NULL);

    int min = tab[0]; // le 1er élément est le min

    for (i = 1; i < taille; ++i) {
        if (min > tab[i]) { min = tab[i]; }
    }

    return min;
}

/**
 * \fn void somme_tableau(const int tab[], index_tab taille)
 */
int somme_tableau(int tab[], index_tab taille)
{
    index_tab i;
    int somme = 0;

    assert(tab != NULL);

    for (i = 0; i < taille; ++i) {
        somme += tab[i];
    }

    return somme;
}

/**
 * \fn void moyenne_tableau(const int tab[], index_tab taille)
 */
double moyenne_tableau(int tab[], index_tab taille)
{
    index_tab i;
    int somme = 0;

    assert(tab != NULL);

    for (i = 0; i < taille; ++i) {
        somme += tab[i];
    }

    return (double) somme / taille;
}

/**
 * \fn void permuter(int* x, int* y)
 */
void permuter(int* x, int* y)
{
    int tmp = *x;
    *x = *y;
    *y = tmp;
}

/**
 * \fn void inverser_tableau(int tab[], index_tab taille)
 */
void inverser_tableau(int tab[], index_tab taille)
{
    index_tab i;

    assert(tab != NULL);

    for (i = 0; i < taille / 2; ++i) {
        permuter(&tab[i], &tab[taille - 1 - i]);
    }
}

/**
 * \fn void moyenne_tableau(const int tab[], index_tab taille)
 */
void trier_tableau(int tab[], index_tab taille)
{
    index_tab i, j;

    assert(tab != NULL);

    for (i = 0; i < taille - 1; ++i) {
#ifdef _DEBUG /* mise au point de l'algo de tri */
    	printf("\033[1;33mIndex Max: [%u] ", taille - 1 - i);
#endif /* _DEBUG */
        for (j = 0; j < taille - 1 - i; ++j) {
            if (tab[j] > tab[j + 1]) {
#ifdef _DEBUG /* mise au point de l'algo de tri */
            	printf(" (%u <-> %u) ", tab[j], tab[j + 1]);
#endif /* _DEBUG */
                permuter(&tab[j], &tab[j + 1]);
            }
        }
#ifdef _DEBUG /* mise au point de l'algo de tri */
	    printf("\n");
#endif /* _DEBUG */
    }
#ifdef _DEBUG /* mise au point de l'algo de tri */
    printf("\n\033[0m");
#endif /* _DEBUG */
}
