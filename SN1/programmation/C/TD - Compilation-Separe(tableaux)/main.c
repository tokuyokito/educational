/**
 * \file main.c

 * \brief Programme pour la manipulation de tableaux.
 
 * \author BTS SNIR 1 2017
 * \version 0.1
 * \date 12 octobre 2017
 * 
 *
 * Programme de test la manipulation des tableaux via différentes fonctions.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "lib_tableau.h"

#ifdef CST_PP /* Si CST_PP est définie (option -DCST_PP) */
    /* Constantes avec préprocesseur */
    #define NOMBRE_DE_TIRAGES 10
    #define TAILLE_ENSEMBLE   99

    #define TAILLE_TAB1       20
#else
    /* Constantes avec compilateur (option 2) */
    const index_tab NOMBRE_DE_TIRAGES = 10;
    const index_tab TAILLE_ENSEMBLE   = 99;

    const index_tab TAILLE_TAB1       = 20;
#endif


/**
 * \fn int main()
 * \brief Fonction Principale Entrée du programme.
 *        Dédiée à la manipulation des tableaux.
 *
 * \return EXIT_SUCCESS - Arrêt normal du programme.
 */

int main()
{
    /* Tableau de taille prédéfinie */
    index_tab i;
    int tab1[TAILLE_TAB1]; /* tableau de taille fixe */

    /* afficher taille */
    printf("Taille de tab1: %lu = %lu * %u\n", sizeof(tab1), sizeof(int), TAILLE_TAB1);

    /* initialisation du générateur de nombres aléatoires */
    srand(time(NULL));

    /* initialisation du tableau avec des nombres aléatoires */
    for (i = 0; i < TAILLE_TAB1; ++i) {
        tab1[i] = 1 + (int)((float) TAILLE_ENSEMBLE * rand() / (RAND_MAX + 1.0));
    }

    afficher_tableau(tab1, TAILLE_TAB1);

    /* Tableau de taille non définie */
    index_tab taille_tab2;

    printf("Donner n: ");
    scanf("%u", &taille_tab2);

    int tab2[taille_tab2]; /* tableau de taille variable */


    printf("Taille de tab2: %lu = %lu * %u\n", sizeof(tab2), sizeof(int), taille_tab2);

    /* initialisation du tableau avec des nombres aléatoires */

    for (i = 0; i < taille_tab2; ++i) {
        tab2[i] = 1 + (int)((float) TAILLE_ENSEMBLE * rand() / (RAND_MAX + 1.0));
    }

    afficher_tableau(tab2, taille_tab2);
    printf("Valeur Maximale de tab2....: %d\n",        max_tableau(tab2, taille_tab2));
    printf("Valeur Minimale de tab2....: %d\n",        min_tableau(tab2, taille_tab2));
    printf("Somme des valeurs de tab2..: %d\n",      somme_tableau(tab2, taille_tab2));
    printf("Moyenne des valeurs de tab2: %.2lf\n", moyenne_tableau(tab2, taille_tab2));

    printf("\nInverser le tableau tab2:\n");
    inverser_tableau(tab2, taille_tab2);
    afficher_tableau(tab2, taille_tab2);

    printf("Trier le tableau tab2:\n");
    trier_tableau(tab2, taille_tab2);
    afficher_tableau(tab2, taille_tab2);

    return EXIT_SUCCESS;
}
