/**
 * \file libtableau.h

 * \brief Programme pour la manipulation de tableaux.
 
 * \author BTS SNIR 1 2017
 * \version 0.1
 * \date 12 octobre 2017
 * 
 *
 * Programme de test la manipulation des tableaux via différentes fonctions.
 * Compilation: gcc -DCST_PP -o tableau tableau.c -pedantic -Wall 
 */

#ifndef LIB_TABLEAU_H_2017_10_18
#define LIB_TABLEAU_H_2017_10_18  1

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

/* *
 * \typedef unsigned int index_tab pour indexer les tableaux
 */
typedef unsigned int index_tab;

/**
 * \fn void afficher_tableau(const int tab[], index_tab taille)
 * \brief Fonction pour afficher un tableau d'entiers passé en argument.
 *
 * \param[in] int tab[]: adresse du tableau.
 * \param[in] index_tab taille: du tableau.
 *
 * \return[out] void: rien.
 */
void afficher_tableau(int tab[], index_tab taille);

/**
 * \fn void max_tableau(const int tab[], index_tab taille)
 * \brief Fonction pour renvoyer la valeur maximale du tableau
 *        d'entiers passé en argument.
 *
 * \param[in] int tab[]: adresse du tableau.
 * \param[in] index_tab taille: du tableau.
 *
 * \return[out] int: valeur entière maximale du tableau.
 */
int max_tableau(int tab[], index_tab taille);

/**
 * \fn void min_tableau(const int tab[], index_tab taille)
 * \brief Fonction pour renvoyer la valeur mainimale du tableau
 *        d'entier passés en argument.
 *
 * \param[in] int tab[]: adresse du tableau ne peut être NULL.
 * \param[in] index_tab taille: du tableau.
 *
 * \return[out] int: valeur entière minimale du tableau .
 */
int min_tableau(int tab[], index_tab taille);

/**
 * \fn void somme_tableau(const int tab[], index_tab taille)
 * \brief Fonction pour renvoyer la somme des valeurs
 *        du tableau d'entiers passé en argument.
 *
 * \param[in] int tab[] adresse: du tableau ne peut être NULL.
 * \param[in] index_tab taille: du tableau.
 *
 * \return[out] int: somme des valeurs entières du tableau .
 */
int somme_tableau(int tab[], index_tab taille);

/**
 * \fn void moyenne_tableau(const int tab[], index_tab taille)
 * \brief Fonction pour renvoyer la valeur moyenne des valeurs
 *        du tableau d'entiers passé en argument.
 *
 * \param[in] int tab[] adresse: du tableau ne peut être NULL.
 * \param[in] index_tab taille: du tableau.
 *
 * \return[out] double: moyenne arithmétique des valeurs entières du tableau .
 */
double moyenne_tableau(int tab[], index_tab taille);

/**
 * \fn void permuter(int* x, int* y)
 * \brief Fonction permutant la valeur de deux variables entières.
 *
 * \param[in] int* x: adresse d'une variable entière.
 * \param[in] int* y: adresse d'une variable entière.
 *
 * \return[out] void: rien.
 */
void permuter(int* x, int* y);

/**
 * \fn void inverser_tableau(int tab[], index_tab taille)
 * \brief Fonction inversant les valeurs du tableau d'entiers
 *        passé en argument.
 *
 * \param[in] int tab[] adresse: du tableau ne peut être NULL.
 * \param[in] index_tab taille: du tableau.
 *
 * \return[out] void: rien.
 */
void inverser_tableau(int tab[], index_tab taille);

/**
 * \fn void moyenne_tableau(const int tab[], index_tab taille)
 * \brief Fonction pour trier en ordre croissant les valeurs
 *        du tableau d'entiers passé en argument.
 *
 * \param[in] int tab[] adresse: du tableau ne peut être NULL.
 * \param[in] index_tab taille: du tableau.
 *
 * \return[out] void: rien.
 */
void trier_tableau(int tab[], index_tab taille);

#endif /* LIB_TABLEAU_H_2017_10_18 */
