#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAXI 1000

int main()
{
    printf("Affichage d'un nombre aléatoire \n");
    printf("Les nombres paire seront mis en vert et les impaires en rouge \n");    
    
    //initialisation variable aléatoire
    srand(time(NULL));
   
    int nombre;
    nombre = 0;
    nombre =  1 + (int) ((float) MAXI * rand() /(RAND_MAX + 1.0));
    
    // vérification si multiple de 2
    
    int reste;
    reste = nombre % 2;
   
   
    //Coloration du nombre 
        if (reste = 0)
        {
            printf("Le nombre ");
            printf("\e[31m%d\e[0m", nombre);
            printf(" est paire\n" );       
        }
        else (reste =! 0)
        {        
            printf("Le nombre ");
            printf("\e[30m%d\e[0m", nombre);
            printf(" est paire\n" );       
        }

    return EXIT_SUCCESS;
}

