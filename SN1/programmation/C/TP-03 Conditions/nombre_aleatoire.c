#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define NOMBRE_DE_TIRAGES     10
#define TAILLE_DE_L_ENSEMBLE 100

int main()
{
    int i;

    // initialisation du g�n�rateur de nombres all�atoires
    srand(time(NULL));

    for (i = 0; i < NOMBRE_DE_TIRAGES; i++)
    {
        printf("%d ", 1 + (int) ((float) TAILLE_DE_L_ENSEMBLE * rand() / (RAND_MAX + 1.0)));

        // version plus simple � v�rifier...        
        //printf("%d ", (rand() % (MAX - MIN + 1)) + MIN));
    }
    printf("\n");

    return EXIT_SUCCESS;
}
