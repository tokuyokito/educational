/**
\file compresseur.c
\authors Le Gall Cedric
\brief fichier principal de l'alghorythme de compression baser sur l'extention .re
\version 0.0.1
\date 25 novembre 2017
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <errno.h>

#define VRAI 0
#define FAUX !VRAI

#define DEBUG FAUX

#define EXTENTION .re

/**
\fn char* recup_nom(tab[])
\brief recuperation du nom dans une chaine de caractere de type /a/b/c ou \a\b\c
\param chaine de caractere
\return un pointeur sur la position du dernier / ou \ +1
*/
char* recup_nom(char tab[])
{
    char* pointeur_debut = NULL;
    char* pointeur_fin = NULL;

    pointeur_debut = pointeur_fin = tab;

    while (*pointeur_fin != '\0') {
        ++pointeur_fin;

        if (*pointeur_fin == '/' || *pointeur_fin == '\\') {
            pointeur_debut = pointeur_fin;
        }
    }

    pointeur_debut++;
    return pointeur_debut;
}

/**
\fn void formatage_nom(tab)
\brief formate un nom pour l'objet à compresser en cible avec l'extension .re
\param chaine à modifier | tableau de caratère vierge
\return tableau formate
*/
void formatage_nom(const char tab_modifier[], char chaine_nom[])
{
    int compteur = 0;
    char* p = recup_nom(tab_modifier);
    printf("%p\n", p);
    printf("%c\n", *p);
    while (*p != '\0') {

    	chaine_nom[compteur] = *p;
    	
    	p++;
    	compteur++;
    }
}

int main(int argc, char const* argv[])
{
    FILE* file_in = NULL; // poiteur de lecture de fichiers
    FILE* file_out = NULL; //poiteur d' écriture de fichiers

    char nom_fichier[255];

    int octet_in = 0, octet_out = 0; // lecture / écriture de fichiers
    errno = 0;
    setlocale(LC_ALL, ""); // utilise la langue local du l'ordinateur

    if (argc != 2) {
        printf("Usage ./compresseur <chemin fichier>\n");
        exit(1);
    }

    file_in = fopen(argv[1], "r");

    if (file_in == NULL) {
        printf("%s\n", strerror(errno));
        exit(1);
    }

    formatage_nom(argv, nom_fichier);

    fclose(file_in);


    return 0;
}
