/* programme : definie un tableau de taille et de contenue aléatoire, effectue la moyenne de ces valeurs
* gcc -o tableau tableau.c -Wall -lm
* erreur corriger : somme non initialiser prenait une valeur aléatoire + la somme
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAXI 100
#define MAXI_TABLEAU 10

// printf("%d ", 1 + (int) ((float) TAILLE_DE_L_ENSEMBLE * rand() / (RAND_MAX + 1.0)));

int main()
{
     // initilisation des valeurs "aléatoire" et du tableau
    printf("Définie un tableau de taille et de contenue aléatoire, effectue la moyenne des valeurs acquises.\n");
    
    srand(time(NULL));
   
    int tailleTableaux;
    tailleTableaux = 1 + (int) ((float) MAXI_TABLEAU * rand() / (RAND_MAX + 1.0));
    int tableau[tailleTableaux];
   
    //valeur des compteurs
    int i;
    int attendre = 5;

    for (int i = 0; i < tailleTableaux; ++i)
    {
        tableau[i] = 1 + (int) ((float) MAXI * rand() / (RAND_MAX + 1.0));       

        
        for (int attendre = 0; attendre < 5; ++ attendre)
        {
            // boucle crée pour ne pas obtenir un nombre aléatoire identique
        }   
    }

    // affiche le contenue du tableau et effectue la somme
    printf("le tableau a pour valeurs :\n");
    int ii, 
    somme = 0;
    
    for (int ii = 0; ii < tailleTableaux; ++ii)
    {
        printf("%d\n",tableau[ii]);
        somme = somme + tableau[ii];

    }
    
    int moyenne = somme / tailleTableaux;
    printf("La somme est de : %d\n",somme );
    printf("La taille du tableau est de %d et ça moyenne est de %d\n", tailleTableaux, moyenne);

    return EXIT_SUCCESS;

}
    
