//*
// \file mandelbrot.cpp
//
// \brief Rendu pour le fractal 'Ensemble de Mandelbrot'
// 
// \author BTS SNIR 1 2018
// \version 0.1
// \date 19 janvier 2018
// 
// À lire: https://fr.wikipedia.org/wiki/Ensemble_de_Mandelbrot
// Compilation: g++ -o Mandelbrot Mandelbrot.cpp -std=c++11 -Wall -lSDL2-2.0
//

#include <SDL2/SDL.h>
#include <iostream>
#include "complexe.h"

using namespace std;

int main()
{
    enum { Width = 1280, Height = 720 };

    // Starting SDL
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        cerr << "SDL_Init Error: " << SDL_GetError() << std::endl;

        return -1;
    }

    SDL_Window* window;
    SDL_Renderer* renderer;

    SDL_CreateWindowAndRenderer(Width, Height, SDL_WINDOW_OPENGL | SDL_WINDOW_BORDERLESS, &window, &renderer);
    SDL_SetWindowPosition(window, 65, 126);

    enum { PaletteSize = 32 };
    int palette[PaletteSize + 1][3];

    for (int i = 0; i <= PaletteSize; ++i) {
        palette[i][0] = i < 2 * PaletteSize / 3 ? i * 255 * 3 / (2 * PaletteSize) : 255;
        palette[i][1] = i < PaletteSize / 3 ? 0 : (i - PaletteSize / 3) * 255 * 3 / (2 * PaletteSize);
        palette[i][2] = 0;
    }

    double xScale = 1.0;
    double yScale = 1.0;

    for (int y = 0; y < Height; ++y) {
        for (int x = 0; x < Width; ++x) {
            Complexe c(xScale * (x - (Width - Height) / 2) / Height * 4.0 - 2.0,
            		   yScale * y / Height * 4.0 - 2.0);
            Complexe z(0.0, 0.0);
            int cnt = 0;

            while (cnt < PaletteSize) {
                z = z * z + c;

                if ((z.module()) > 2.0) {
                    break;
                }

                ++cnt;
            }

            SDL_SetRenderDrawColor(renderer, palette[cnt][0], palette[cnt][1], palette[cnt][2], 0xff);
            SDL_RenderDrawPoint(renderer, x, y);
        }
    }

    SDL_RenderPresent(renderer);
    cin.get();

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

    SDL_Quit();
}
