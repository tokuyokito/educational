///
// \file complexe.cpp
//
// * \brief Classe Complexe
//
// \author BTS SNIR 1 2018
// \version 0.1
// \date 18 janvier 2018
//

#include <cmath>
#include <cstdlib>

#include "complexe.h"

// initialisation de l'attribut statique
// pour le formatage _format;
Complexe::Format Complexe::_format = Complexe::alg;

// constructeur
Complexe::Complexe(double reel, double imag)
{
    _reel = reel;
    _imag = imag;
}

// renvoie le module d'un nombre complexe
double Complexe::module() const
{
    return sqrt(_reel * _reel + _imag * _imag);
}

double Complexe::argument() const
{
    // prendre en charge la division par z�ro
    // avec les 5 cas particuliers

    // complexe nul
    if (_reel == 0.0 && _imag == 0.0) {
        return 0.0;
    }

    // partie imaginaire nulle
    // partie r�elle positive
    if (_reel > 0.0 && _imag == 0.0) {
        return 0.0;
    }

    // partie r�elle n�gative
    if (_reel < 0.0 && _imag == 0.0) {
        return M_PI;
    }

    // partie r�elle nulle
    // partie imaginaire positive
    if (_reel == 0.0 && _imag > 0.0) {
        return M_PI_2;
    }

    // partie imaginaire n�gative
    if (_reel == 0.0 && _imag < 0.0) {
        return -M_PI_2;
    }

    // cas global
    // � faire traitement modulo pi
    if (_reel < 0.0) {
        return atan(_imag / _reel) + M_PI;
    }

    return atan(_imag / _reel);
}

Complexe Complexe::operator+ (const Complexe& z) const
{
    Complexe z_temporaire;

    z_temporaire._reel = _reel + z._reel;
    z_temporaire._imag = _imag + z._imag;

    return z_temporaire;
}

Complexe Complexe::operator+ (double reel) const
{
    Complexe z_temporaire;

    z_temporaire._reel = _reel + reel;
    z_temporaire._imag = _imag;

    return z_temporaire;
}

Complexe operator+ (double reel, const Complexe& z)
{
    Complexe z_temporaire;

    z_temporaire._reel = reel + z._reel;
    z_temporaire._imag = z._imag;

    return z_temporaire;
}

Complexe Complexe::operator* (const Complexe& z) const
{
    Complexe z_temporaire;

    z_temporaire._reel = _reel * z._reel - _imag * z._imag;
    z_temporaire._imag = _reel * z._imag + _imag * z._reel;

    return z_temporaire;
}

Complexe Complexe::operator* (double reel) const
{
    Complexe z_temporaire;

    z_temporaire._reel = reel * _reel;
    z_temporaire._imag = reel * _imag;
    return z_temporaire;
}

Complexe operator* (double reel, const Complexe& z)
{
    Complexe z_temporaire;

    z_temporaire._reel = z._reel * reel;

    return z_temporaire;
}

Complexe& Complexe::operator+= (const Complexe& z)
{
    _reel += z._reel;
    _imag += z._imag;

    return *this;
}

Complexe& Complexe::operator+= (double reel)
{
    _reel += reel;

    return *this;
}

Complexe& Complexe::operator*= (const Complexe& z)
{
    double  sauvegardeReel;
    
    sauvegardeReel *= _reel * z._reel - _imag * z._imag; 
    _imag *= _reel * z._imag + _imag * z._reel;

    _reel = sauvegardeReel;

    return *this;
}

Complexe& Complexe::operator*= (double reel)
{
    _reel *= reel;
    _imag *= 0;

    return *this;
}

Complexe& Complexe::operator= (double reel)
{

    _reel = reel;

    return *this;
}

std::ostream& operator<< (std::ostream& stream, Complexe::Format format)
{
    Complexe::_format = format;

    return stream;
}

std::ostream& operator<< (std::ostream& stream, const Complexe& z)
{
    if (Complexe::_format == Complexe::alg) {
        stream << z._reel;

        if (z._imag > 0.0) {
            stream << " + i" << z._imag;
        }
        else
            if (z._imag < 0.0) {
                stream << " - i" << fabs(z._imag);
            }
    }
    else
        if (Complexe::_format == Complexe::tri) {
            stream << z.module()    << " [cos("   << z.argument()
                   << ") +i sin(" << z.argument() << ")]";
        }
        else
            if (Complexe::_format == Complexe::exp) {
                stream << z.module() << " e i(" << z.argument() << ")";
            }

    return stream;
}
