//
// Auteur:
//
// Sujet: Introduction aux fonctions template
//
// Fait Le:
// Mise à Jour:
//
// Compilation: g++ -o tri triTableau.cpp -Wall
//


#include <iostream>
#include <cstdlib>
#include <ctime>
#include <SFML/Graphics.hpp>

using namespace std;

/* définition du type index_tab pour indexer les tableaux */
typedef unsigned int index_tab;

// Nombre de valeurs aléatoires
const index_tab TAILLE_ENSEMBLE  = 100;
// Taille du tableau
const index_tab TAILLE_TABLEAU   = 100;

template<typename T>
sf::Time quicksort(T* A, int len)
{
	sf::Time t1 = sf::microseconds(0);
	sf::Clock clock;

    if (len < 2) { return t1 = clock.getElapsedTime(); }

    T pivot = A[len / 2];

    int i, j;

    for (i = 0, j = len - 1; ; i++, j--) {
        while (A[i] < pivot) { i++; }

        while (A[j] > pivot) { j--; }

        if (i >= j) { break; }

        T temp = A[i];
        A[i]   = A[j];
        A[j]   = temp;
    }

    quicksort(A, i);
    quicksort(A + i, len - i);

    return t1 = clock.getElapsedTime();
}

template <class T>
 sf::Time triInsertion(const T tab[], const int taille)
{
	/*Déclaration des variables*/
	T tabTmp[taille];
	int i, j;
	T tmp;
	
	/*calcul du temp mis par la fonction*/
	sf::Time t1 = sf::microseconds(0);

	for (int i = 0; i < taille; ++i) tabTmp[i] = tab[i];
	
	sf::Clock clock;
	/*trie*/
	for ( i = 1; i < taille; ++i) {
		
		tmp = tabTmp[i];
		for (j = i; j > 1, tabTmp[j - 1] > tmp; --j) tabTmp[j] = tabTmp[j - 1];
		tabTmp[j] = tmp;

	}

	cout <<"TrieInsertion :\n";
	for ( i = 0; i < taille; ++i) cout << "["<< tabTmp[i] << "] "; 
	
		return t1 = clock.getElapsedTime();
}

template <class T>
void afficherTableau(T tab[], index_tab taille)
{
	index_tab i;

	cout << "\n\033[0;34mTableau...: \033[1;34;40m";

	for (i = 0; i < taille; ++i) {
		cout << "[\033[1;33;40m" << tab[i] << "\033[1;34;40m]";
	}

	cout << "\033[0m\n";
	cout << "  \033[0;34mIndex n°:";

	for (i = 0; i < taille; ++i) {
		cout << "  " << i;
	}
	cout << "\033[0m\n\n";
}

int main()
{
	index_tab i;
	double tableauInt[TAILLE_TABLEAU];

	// Initialisation du générateur de nombres aléatoires
	srand(time(NULL));
	//
	sf::Time t1 = sf::microseconds(0);
	// Initialisation du tableau avec des nombres aléatoires
	for (i = 0; i < TAILLE_TABLEAU; ++i) {
		tableauInt[i] = 1 + (double) ((float) TAILLE_ENSEMBLE * rand() / (RAND_MAX + 1.0));
	}

	// Affichage du tableau
	afficherTableau<double>(tableauInt, TAILLE_TABLEAU);

	// Mise en oeuvre des différentes fonctions de tri...
	t1 = triInsertion<double>(tableauInt, TAILLE_TABLEAU);
	cout << "\nLe temps mis par la fonction triInsertion est de " << t1.asMicroseconds() << " µs \n";

	t1 = quicksort(tableauInt, TAILLE_TABLEAU);
	cout << "\nLe temps mis par la fonction quicksort est de " << t1.asMicroseconds() << " µs \n";

	return EXIT_SUCCESS;
}
