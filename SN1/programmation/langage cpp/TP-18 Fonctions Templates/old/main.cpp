#include <cstdlib>
#include <iostream>

template<typename T>
void quicksort(T* A, int len);

int main (void)
{
    double a[] = {4, 65, 2, -31, 0, 99, 2, 83, 782, 1, 4.1};
    int n = sizeof a / sizeof a[0];
    int i;

    for (i = 0; i < n; i++) {
        std::cout << a[i] << " ";
    }

    std::cout << "\n";

    quicksort<double>(a, n);

    for (i = 0; i < n; i++) {
        std::cout << a[i] << " ";
    }

    std::cout << "\n";

    return 0;
}

template<typename T>
void quicksort(T* A, int len)
{

    if (len < 2) { return; }

    T pivot = A[len / 2];

    int i, j;

    for (i = 0, j = len - 1; ; i++, j--) {
        while (A[i] < pivot) { i++; }

        while (A[j] > pivot) { j--; }

        if (i >= j) { break; }

        T temp = A[i];
        A[i]   = A[j];
        A[j]   = temp;
    }

    quicksort(A, i);
    quicksort(A + i, len - i);
}
