///
// \file complexe.h
//
// * \brief Classe Complexe
// 
// \author BTS SNIR 1 2018
// \version 0.1
// \date 18 janvier 2018
// 

#ifndef COMPLEXE_H
#define COMPLEXE_H

#include <iostream>

class Complexe {
private:
    double _reel;
    double _imag;

public:
    // constructeurs et constructeur par d�faut
    // Complexe z(r, i);
    // Complexe z(r);
    // Complexe z;
    Complexe(double = 0.0, double = 0.0);

    // m�thodes relatives aux informations
    // des nombres complexes

    // module du nombre complexe
    double module() const;

    // argument du nombre complexe
    double argument() const;

    // accesseurs inlines

    // renvoie de la valeur r�elle
    double lireReel() const
    {
        return _reel;
    }

    // renvoie de la valeur imaginaire
    double lireImag() const
    {
        return _imag;
    }

    // surcharge d'op�rateur +
    // z1 + z2;
    Complexe operator+ (const Complexe& z) const;
    // z + r;
    Complexe operator+ (double reel) const;
    // r + z;
    friend Complexe operator+ (double, const Complexe& z);

    // surcharge d'op�rateur *
    // z1 * z2;
    Complexe operator* (const Complexe& z) const;
    // z + r;
    Complexe operator* (double reel) const;
    // r + z;
    friend Complexe operator* (double, const Complexe& z);

    // surcharge de l'op�rateur +=
    // z1 += z2;
    Complexe& operator+= (const Complexe& z);
    // z += r;
    Complexe& operator+= (double reel);

    // surcharge de l'op�rateur *=
    // z1 *= z2;
    Complexe& operator*= (const Complexe& z);
    // z *= r;
    Complexe& operator*= (double reel);

    // surcharge de l'op�rateur =
    // z = r;
    Complexe& operator=(double reel);

    // surcharge de l'op�rateur <<
    // �num�ration pour formatage
    // alg�brique, trigonm�trique, exponentiel
    enum Format {alg, tri, exp};
    friend std::ostream& operator<< (std::ostream& stream, Format format);
    // std::cout << z
    friend std::ostream& operator<< (std::ostream& stream, const Complexe& z);
private:
    // formatage pour ostream
    static Format _format;
};

#endif // COMPLEXE_H
