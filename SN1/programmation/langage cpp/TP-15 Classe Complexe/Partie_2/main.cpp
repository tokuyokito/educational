///
// \file main.cpp
//
// * \brief Classe Complexe
// 
// \author BTS SNIR 1 2018
// \version 0.1
// \date 18 janvier 2018
// 

#include <cstdlib>
#include <iostream>

using namespace std;

#include "complexe.h"

int main()
{
    Complexe z1;
    Complexe z2(2.3, -3.1);
    Complexe z3(3.2, 4.5);
    Complexe z4(-4.2, 1.3);
    
    // 2 chiffres de précision max
    cout.precision(2);

    cout << "Format algébrique:\n";
    cout << "z1 = " << z1 << "\n";
    cout << "z2 = " << z2 << "\n";
    cout << "z3 = " << z3 << "\n";
    cout << "z4 = " << z4 << "\n";
    cout << Complexe::tri << "\n";
    cout << "Format trigonométrique:\n";
    cout << "z1 = " << z1 << "\n";
    cout << "z2 = " << z2 << "\n";
    cout << "z3 = " << z3 << "\n";
    cout << "z4 = " << z4 << "\n";
    cout << Complexe::exp << "\n";
    cout << "Format exponentiel:\n";
    cout << "z1 = " << z1 << "\n";
    cout << "z2 = " << z2 << "\n";
    cout << "z3 = " << z3 << "\n";
    cout << "z4 = " << z4 << "\n";
    cout << Complexe::alg << "\n";
   
    z1 = z2 + z2;
    cout << "z2 + z2\n";
    cout << "z1 = " << z1 << "\n";
    z1 = z2 + 2.4;
    cout << "z1 = " << z1 << "\n";
    z1 = 1.3 + z2;
    cout << "z1 = " << z1 << "\n";
/*
    z1 = z2 * z2;
    cout << "z1 = " << z1 << "\n";
    z1 = z2 * 2.4;
    cout << "z1 = " << z1 << "\n";
    z1 = 1.3 * z2;
    cout << "z1 = " << z1 << "\n";

    z1 += z2;
    cout << "z1 = " << z1 << "\n";
    z1 += 2.4;
    cout << "z1 = " << z1 << "\n";
    z1 *= z2;
    cout << "z1 = " << z1 << "\n";
    z1 *= 1.3;
    cout << "z1 = " << z1 << "\n";
*/
    return EXIT_SUCCESS;
}


