#include <cstdlib>
#include <iostream>

using namespace std;

#include "complexe.h"


int main()
{
    int reel, imaginaire;
    
    int *p_reel = &reel;
    int *p_imag = &imaginaire;

    //Complexe z1;
	cout << "Affiche des nombres complexe dans l'intervalle [-pi, pi] \n";
	
	cout << "Saisir la partie reel : ";
	cin >> reel;
	
	cout << "Saisir la partie imaginaire : ";
	cin >> imaginaire;
	cout << "\n";

    // test du constructeur
    Complexe z1(reel, imaginaire);
   
    // affichage z = reel + i imag
    cout << "z1 = " << z1.reel() << "+i" << z1.imag() << '\n';

    // affichage [module,argument]
    cout << "z1 = [" << z1.module() << ',' << z1.argument() << "]\n"; 
    cout << "\n";

    //addition de nombre complexe
    cout << "Saisir la partie reel du deuxieme nombre : \n";
	cin >> reel;
	
	cout << "Saisir la partie imaginaire du deuxieme nombre : \n";
	cin >> imaginaire;
	cout << "\n";

	Complexe z2(reel, imaginaire);
	
	cout << "z2 = " << z2.reel() << "+i" << z2.imag() << '\n';
	cout << "z2 = [" << z2.module() << ',' << z2.argument() << "]\n";


    return EXIT_SUCCESS;
}
