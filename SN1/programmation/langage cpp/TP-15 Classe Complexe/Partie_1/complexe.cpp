#include <cmath>
#include "complexe.h"

// constructeur
Complexe::Complexe(double reel, double imag )
{
    _reel = reel;
    _imag = imag;
}

// initilisation des valeurs _reel et _imag
void Complexe::init(double reel, double imag)
{
    _reel = reel;
    _imag = imag;
}

// renvoie le module d'un nombre complexe
double Complexe::module()
{
  	return _module = sqrt( (pow( _reel, 2) + pow( _imag, 2)));
}

double Complexe::argument()
{	
    if ( _reel == 0 && _imag == 0)
    {
    	return _argument = 0;
    }
    else if ( _reel == 0 && _imag == 1) {
    	
    	return _argument = M_PI / 2;
    }
    else if ( _reel == 0 && _imag == -1) {
    	
    	return _argument = -M_PI / 2;
    }
    else if ( _reel == 1 && _imag == 0) {
    	
    	return _argument = -M_PI;
    }
    else if ( _reel == -1 && _imag == 0) {
    
    	return _argument = M_PI;
    }
    else {
    	
    	return atan( _imag / _reel);
    }

	return EXIT_SUCCESS;
}

Complexe Complexe::operator+(Complexe Z)
{
	Complexe tmp; 
	tmp._reel = _reel + Z._reel;
	tmp._imag = _imag + Z._imag;

	return tmp;
}