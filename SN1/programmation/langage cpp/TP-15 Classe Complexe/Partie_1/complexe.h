#ifndef COMPLEXE_H
#define COMPLEXE_H

#include <iostream>
#include <cstdlib>

class Complexe {
private:
    double _reel;
    double _imag;

    double _argument;
    double _module;
public:
    // constructeurs
    Complexe(double reel = 0.0, double imag = 0.0);
    
    void init(double reel, double imag);
    // m�thode autre
    
    Complexe Complexe::operator+(Complexe Z);

    // m�thodes relatives aux informations
    // des nombres complexes
    double module();
    double argument();

    // accesseurs inline
    double reel()
    {
        return _reel;
    }
    double imag()
    {
        return _imag;
    }
};

#endif // COMPLEXE_H

