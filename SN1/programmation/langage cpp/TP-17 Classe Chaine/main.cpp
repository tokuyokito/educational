#include "chaine.h"

#include <iostream>
#include <cstdlib>

// debugger fuites de mémoire
// valgrind --leak-check=full ./chaine

using namespace std;

int main()
{

	// Étape 0: opérateur <<
	// Étape 1: constructeur par défaut
    Chaine c1;
    cout << "c1: [" << c1 << "]\n";

    // Étape 2: constructeur char*
    Chaine c2("Bonjour");
    cout << "c2: [" << c2 << "]\n";

    Chaine c0 = NULL;
    cout << "c0: [" << c0 << "]\n";

    // Étape 3: constructeur c, n
    Chaine c3('z', 10);
    cout << "c3: [" << c3 << "]\n";

    // Étape 4: Inverser chaine
	c2.inverser();
    cout << "c2: [" << c2 << "]\n";

    // Étape 5: opérateurs +=
  //  c1 += "def";
    //cout << "c1: [" << c1 << "]\n";

    // Étape 6: opérateurs +=
    //c2 += "def";
    //cout << "c2: [" << c2 << "]\n";

    // Étape 7: opérateurs +=
    //c2 += c1;
    //cout << "c2: [" << c2 << "]\n";

    // Étape 8: opérateurs =
    //c1 = c2;
    //cout << "c1: [" << c1 << "]\n";

    // Étape 9: opérateurs =
    //c2 = c2;
    //cout << "c2: [" << c2 << "]\n";

    // Étape 10: Constructeur par recopie
    //Chaine c4(c2);
    // ou
    //Chaine c4 = c2;
    //cout << "c4: [" << c4 << "]\n";

    // Étape 11: opérateurs +
    //c4 = c1 + c2;
    //cout << "c4: [" << c4 << "]\n";

    // Étape 12: opérateurs +
    //c4 = c1 + "zzz";
    //cout << "c4: [" << c4 << "]\n";

    // Étape 13: opérateurs +
    //c4 = "aaa" + c1;
    //cout << "c4: [" << c4 << "]\n";

    // Étape 14: opérateurs []
    //c4[1] = 'z';
    //cout << "c4: [" << c4 << "]\n";

   return EXIT_SUCCESS;
}
