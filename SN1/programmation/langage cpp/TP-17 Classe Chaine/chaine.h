#ifndef CHAINE_H
#define CHAINE_H

#include <ostream>

class Chaine {
private:
    unsigned int     _tailleAllocation;
    char*            _ptChaine;
    char             _default;

public:
    // fonctions amies
    friend std::ostream& operator<< (std::ostream& s, const Chaine& c);
    // constructeurs et constructeur par défaut
    Chaine(const char* s = NULL);
    Chaine(const Chaine& c);
    Chaine(char c, unsigned int nb);
    ~Chaine();

    // inverser la chaine...
    void inverser();

    // opérateurs
//    Chaine& operator+= (const char* s);
    //Chaine& operator+= (const Chaine& c);

    //Chaine& operator=  (const char* s);
    //Chaine& operator=  (const Chaine& c);

    //Chaine  operator+  (const Chaine& c) const;
    //Chaine  operator+  (const char* s) const;
    //friend Chaine  operator+ (const char* s, const Chaine& c);

    //char& operator[] (const size_t c);
};

#endif // CHAINE_H






