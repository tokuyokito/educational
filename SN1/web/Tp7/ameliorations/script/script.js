/* auteurs      : Le Gall Cédric, Alan Le Garf
 * version      : 6
 * description  : Découverte et mise en place d'un lecteur audio avec playliste
 *
 */

//Variables globales
var selectMusique = document.getElementById("selection_musique");
var boxLiens = document.getElementById("liste");
var lecteur = document.getElementById("lecteur");
var posliste = 0;
var position = 0;

//Callback d'insertion de nouveaux titres
selectMusique.onchange = function(e) {

    var lienMusique = document.createElement("a");
    lienMusique.setAttribute("href", "#");
    lienMusique.setAttribute("title", "Ecoutez " + selectMusique.value);
    lienMusique.setAttribute("id", posliste); //id
    lienMusique.setAttribute("onclick", "lire(" + posliste + ")");

    files = selectMusique.files[0];
    var objectURL = window.URL.createObjectURL(files);
    var texteLien = document.createTextNode(files.name);
    lienMusique.appendChild(texteLien);

    boxLiens.appendChild(lienMusique);
    boxLiens.innerHTML += "<br>";
    posliste += 1;

    fichier.push(objectURL);
    nom_fichier.push(files.name);
    lecteur.load();

};


function lire(liste) {

    //lecteur audio
    var CreerMusique = document.getElementById("balise_source");

    document.getElementById("leg").innerHTML = "Lecture : " + nom_fichier[liste] + " en cours";

    CreerMusique.setAttribute("src", fichier[liste]);
    lecteur.load();
    position = liste;
}

var fichier = [];
var nom_fichier = [];

lecteur.onended = function(e) {

    var test = document.getElementById("lirePlaylist").checked;

    if (test == true && (nom_fichier.length - 1) != position) {

        position++;
        lecteur.src = fichier[position];
        lecteur.load();
        lecteur.play();
    }

};