"use strict";

/* auteur      : Le Gall Cédric
 * version     : 4
 * description : Découverte et mise en place d'un lecteur audio avec playliste
 *
 */


//ajout du chemin du fichier selectione par l'utilisateur dans le tableau lien_fichier
function ajout_morceau() {
	
	var lecteur   = document.getElementById('bouton_selection')
	var files     = lecteur.files[0];
	var objectURL = window.URL.createObjectURL(files);

	list.push(files.name);
	lien_fichier.push(objectURL);
	
	nouveau_morceau_playliste(files);
}

//ajout du nom du fichier formater dans la playlist
function nouveau_morceau_playliste(file) {
	
	i++;
	var nouveau_morceau  = document.createElement('a');
	nouveau_morceau.id   = 'morceau' + i;
	nouveau_morceau.href = "#";
	nouveau_morceau.addEventListener("click", lecture);
	
	var text = document.createTextNode(file.name);
	nouveau_morceau.appendChild(text);

	document.getElementById('emplacement_playliste').appendChild(nouveau_morceau);
}


function test_fin() {
	
	var fin    = document.getElementById("audio").ended; 
	var auto   = document.getElementById("auto").checked;
	
	if (fin == true && auto == true && index != (list.length - 1)) suivant();	
}

//fonction suivante 
function suivant() {	
	
	//variable
	index++;
	var audio            = document.getElementById('audio');
	var ancienne_musique = audio.firstChild;
	var nouvelle_musique = document.createElement('source');

	//Configuration
	nouvelle_musique.src = lien_fichier[index];
	nouvelle_musique.id  = 'morceau' + index;

    //Remplacement
    audio.replaceChild(nouvelle_musique, ancienne_musique);

    audio.load();
    afficher_nom_musique(index);
    audio.play();
}

function afficher_nom_musique(indexe) {
	
	var titre = document.getElementById('titre_playlist');

	//le texte devient l'enfant du nouveau text crée
	var titre_text = document.createTextNode((" " + list[indexe]));
	
	if (premier_passage == true) {
		
		titre.appendChild(titre_text);	
		
		premier_passage = false;	
	}
	else
	{
		var ancien_titre   = titre.lastChild;	
		titre.replaceChild(titre_text, ancien_titre);

	}	
}

function lecture() {

	var recuperation_index =  this.id.split('u');
	var audio              = document.getElementById('audio');
	var ancienne_musique   = audio.firstChild;
	var nouvelle_musique   = document.createElement('source');
	index                  = recuperation_index[1];
	
	//Configuration
	nouvelle_musique.src = lien_fichier[recuperation_index[1]];
	nouvelle_musique.id  = 'morceau' + recuperation_index[1];

    //Remplacement
    audio.replaceChild(nouvelle_musique, ancienne_musique);

    audio.load();
    afficher_nom_musique(index);
    audio.play();
}

//contiens les liens internet des fichiers
var lien_fichier = [];

//liste des noms des musiques
var list = [];

// indexe formatage pour les noms
var  i = -1;

//navigation dans la playlist
var index = 0;

var premier_passage = true;