"use strict";

/* auteur      : Le Gall Cédric
 * version     : 5
 * description : Manipulation de canvas
 *
 */

//chargement de la page
window.onload = function() {
	
	var canvasImage = document.getElementById("canvasImage");
    var ctx         = canvasImage.getContext("2d");
    
    var img = document.getElementById("image");
    ctx.drawImage(img, 0, 0);

    initialisationCanvasVideos();
};


function calculFiltre(value) {
	
	var canvas = document.getElementById("canvasImage");
    var ctx    = canvas.getContext("2d");
    
    var img = document.getElementById("image");
    ctx.drawImage(img, 0, 0);
    
   	var imageBaseCanvas = ctx.getImageData(0, 0, 500, 371);
   	
	if (value == "rouge"  ) ctx.putImageData(filtreRouge(imageBaseCanvas, ctx), 0,0);	
	
	if (value == "vert"   ) ctx.putImageData(filtreVert(imageBaseCanvas, ctx), 0,0);
	
	if (value == "bleu"   ) ctx.putImageData(filtreBleu(imageBaseCanvas, ctx), 0,0);
	
	if (value == "inverse") ctx.putImageData(filtreInverse(imageBaseCanvas, ctx), 0,0);
	
	if (value == "normale");
}

function filtreRouge(imageBaseCanvas, ctx) {
	
 	var nouvelleImageCanvas = ctx.getImageData(0, 0, imageBaseCanvas.width, imageBaseCanvas.height);

 	for (var i = 0; i < nouvelleImageCanvas.data.length - 1; i += 4) {
		
		nouvelleImageCanvas.data[i] = imageBaseCanvas.data[i]; // r
		
		if (i == (nouvelleImageCanvas.data.length - 1)) break;

		nouvelleImageCanvas.data[i + 1] = 0; // v
		
		nouvelleImageCanvas.data[i + 2] = 0; // b
		
		nouvelleImageCanvas.data[i + 3] = imageBaseCanvas.data[i + 3]; // alpha
	}

 	return nouvelleImageCanvas;
}


function filtreVert(imageBaseCanvas, ctx) {
	
	var nouvelleImageCanvas = ctx.getImageData(0, 0, imageBaseCanvas.width, imageBaseCanvas.height);

 	for (var i = 0; i < nouvelleImageCanvas.data.length - 1; i += 4) {
		
		nouvelleImageCanvas.data[i] = 0; // r
		
		if (i == (nouvelleImageCanvas.data.length - 1)) break;

		nouvelleImageCanvas.data[i + 1] = imageBaseCanvas.data[i + 1]; // v
		
		nouvelleImageCanvas.data[i + 2] = 0; // b
		
		nouvelleImageCanvas.data[i + 3] = imageBaseCanvas.data[i + 3]; // alpha
	}
	
 	return nouvelleImageCanvas;
}

function filtreBleu(imageBaseCanvas, ctx) {
	
	var nouvelleImageCanvas = ctx.getImageData(0, 0, imageBaseCanvas.width, imageBaseCanvas.height);

 	for (var i = 0; i < nouvelleImageCanvas.data.length - 1; i += 4) {
		
		nouvelleImageCanvas.data[i] = 0; // r
		
		if (i == (nouvelleImageCanvas.data.length - 1)) break;

		nouvelleImageCanvas.data[i + 1] = 0; // v
		
		nouvelleImageCanvas.data[i + 2] = imageBaseCanvas.data[i + 2]; // b
		
		nouvelleImageCanvas.data[i + 3] = imageBaseCanvas.data[i + 3]; // alpha
	}

	return nouvelleImageCanvas;
}

function filtreInverse(imageBaseCanvas, ctx) {
	
	var nouvelleImageCanvas = ctx.getImageData(0, 0, imageBaseCanvas.width, imageBaseCanvas.height);

 	for (var i = 0; i < nouvelleImageCanvas.data.length - 1; i += 4) {
		
		nouvelleImageCanvas.data[i] = 255 - imageBaseCanvas.data[i]; // r
		
		if (i == (nouvelleImageCanvas.data.length - 1)) break;

		nouvelleImageCanvas.data[i + 1] = 255 - imageBaseCanvas.data[i + 1]; // v
		
		nouvelleImageCanvas.data[i + 2] = 255 - imageBaseCanvas.data[i + 2]; // b
		
		nouvelleImageCanvas.data[i + 3] =  imageBaseCanvas.data[i + 3]; // alpha
	}

	return nouvelleImageCanvas;
}

function loupe(position) {
	
	
	var x = position.clientX;
	var y = position.clientY;

	var canvasFocus = document.getElementById("focus");
	var ctxFocus    = canvasFocus.getContext("2d");

	var canvas = document.getElementById("canvasImage");
    var ctx    = canvas.getContext("2d");

    var imageFocus = ctx.getImageData(x -40, y -40, 80, 80);
    ctxFocus.putImageData(imageFocus, 0, 0);

    //ctx.rect(x -40, y - 40, 80 ,80);
	//ctx.stroke();

	//ctx.clearRect(testX -40,testY -40,80,80);
	console.log("position x " + x + " y : " + y);

}

function reinitialisationLoupe(canvas) {

	var canvasFocus = document.getElementById("focus");
	var ctxFocus    = canvasFocus.getContext("2d");

	var canvas = document.getElementById("canvasImage");
    var ctx    = canvas.getContext("2d");

	var x = canvas.offsetLeft;
	var y = canvas.offsetTop;

    var imageFocus = ctx.getImageData(x, y, 80, 80);
    ctxFocus.putImageData(imageFocus, 0, 0);	

}