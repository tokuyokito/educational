// abcisse
t = 0:0.01:1;

//Fréquence
f = evstr(x_dialog("Valeur de la Frequence ?", ['']));

//Graphique
s = 0.5 * sin(2 * %pi * f * t);

// tracer du graphique
plot(s);

xtitle("fonction sinus", "n° d ''échantillons", "tension en V");
xgrid(1, 1, 10);
