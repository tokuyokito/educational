// <<Graphique>>
text = ["Nombre de points n"; "Durée en s"; "Amplitude en V"; "fréquence en Hz"];
valeur_graph = x_mdialog('Caratéristique du signal', text, ['100'; '1'; '5'; '2']);

n    = evstr(valeur_graph(1));
tmax = evstr(valeur_graph(2));
A    = evstr(valeur_graph(3));
f    = evstr(valeur_graph(4));

te = tmax / n
t  =  0 : te : tmax - te

s = A * sin(2 * %pi *  f * t);

// <<FIN>>

// Fonction seule
scf();plot(t,s);
xtitle("fonction sinus" , "temps en s", "Amplitude en V" )

// Fonction + échantillons bloquer
scf();

xsetech([0, 0, 1, 1 / 2])
plot(t, s);

xsetech([0, 1 / 2, 1, 1 / 2])
plot2d2(t, s);

