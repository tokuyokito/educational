// auteur : Le Gall Cédric
// fonction programme : mise en place d'une interface de manipulation du son 
// date : 10/04/2018
//

//Début fonctions
function []= ecoute(s, fe)
     playsnd(s, fe)//écoute avec comme frequence fe
     
endfunction

function []= chronnogramme(s)
     
     plot(s);//chronogramme du son 
     xtitle('chronogramme du son','n°d''échantillon','tension en V');
     
endfunction

function []= spectre(s, fe)
    analyze(s,fmin=100,fmax=8000,rate=fe,points=fe); // spectre du son
    
endfunction
//Fin Fonctions

//Début main
//chargement du son
[s,fe,b]=wavread("gtr-nylon22");

choix = messagebox("Que voulez-vous faire ?", "modal", "info", ["Ecouter le son", "Visualiser le chronnogramme", "Visualiser le spectre", "Quitter"]);

select choix
case 1 then
    ecoute(s, fe);
case 2 then
    chronnogramme(s);
case 3 then
    spectre(s, fe);
else
    //quitter
end
        
//Fin main
