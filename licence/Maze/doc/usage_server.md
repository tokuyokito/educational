# Sommaire

* Documentation partie serveur
    * Fichiers concernés
    * Fonctions
    * Exemple

## Documentation partie serveur

### Fichiers concernés
Les fichiers concernés pour la partie serveur sont player.py et server.py. 

Le fichier server.py sert **d'api** il permet d'initialiser un serveur. 

Le fichier player.py définit un objet représentant les joueurs.
### Fonctions

Class serveur : server.py Constructeur
```python
   server.Server(server_port          : int, 
                 client_port          : int, 
                 secret_key           : bytes, 
                 max_number_of_client : int,
                 maze_size            : int,
                 maze_seed            : str) -> None
```

| Paramètre| Description|
|:---------|:-----------:
|serveur_port                           |Port d'écoute du serveur
|client_port                            |Port de destination pour l'envoi de messages aux clients
|secret_key                             |Clé utilisée pour le chiffrement doit être commune aux clients comme au serveur  
|max_number_of_client                   |Nombre maximal de joueurs pouvant rejoidre la partie 
|maze_size                              | Taille du Labyrinthe à générer
|maze_seed                              | Graine du Labyrinthe à générer

Class serveur : server.py methode start 
```python
start() -> None
```
Cette fonction permet de démarrer le serveur intialise un thread.

Class serveur : server.py methode start_game 
```python
start_game() -> str
```
Cette fonction tente de démarrer le jeu si le jeu démarre elle renvoie **start** sinon elle peut renvoyer les messages suivant : **Everyone is not ready** ou **Server is running**.

### Exemple
```python
import server    

"""
Démarre le serveur en écoute sur le port 6666 prêt à renvoyer des messages sur le port 6667.

La clé secrète est toto.

Le nombre maximal de joueurs est de 2

la taille du labyrinthe est de 10 et la graine est boujour 
"""
server = server.Server(6666, 6667, b'toto', 2, 10, "boujour")

# Démarre un nouveau thread
server.start()

# Tente de démarrer le jeu
message = server.start_game()
print(message)

```
