# Sommaire

* Documentation partie client
    * Fichier concerné
    * Fonctions
    * Exemple

## Documentation partie client
### Fichier concerné
Le fichier concerné est le fichier maze_request.py. Cette api permet de communiquer avec le serveur.
Elle prend en charge toutes les requêtes acceptées par le serveur.

| Requête  | Description|
|:---------|:-----------:
|join            |Permet de joindre un serveur
|leave           |Permet de quitter un serveur
|ready           |Permet de se mettre en état prêt
|not_ready       |Permet de se mettre en état non prêt
|send_coordinate |Permet d'envoyer ses coordonnées au serveur

### Fonctions
Class Request : maze_request.py Constructeur
```python

maze_request.Request(server_address : str,
                     server_port    : int,
                     listen_port    : int,
                     secret_key     : bytes) -> None
```
| Paramètre  | Description|
|:---------|:-----------:
|server_address   |Addresse du serveur à joindre
|server_port      |Port d'écoute du serveur à joindre
|listen_port      |Port d'écoute pour recevoir les messages
|secret_key       |Clé utilisée pour le chiffrement doit être commune aux clients comme au serveur


Class Request : server.py methode join
```python
join() -> None
```
Permet de joindre le serveur. Une **réponse** est attendue avec la fonction **get_result()**.

Class Request : server.py methode leave
```python
leave() -> None
```
Permet de quitter le serveur cette fonction est appelée dans le destructeur de l'objet request.


Class Request : server.py methode ready
```python
ready() -> None
```
Permet de se mettre en état prêt.

Class Request : server.py methode not_ready
```python
not_ready() -> None
```
Permet de se mettre en état non prêt

Class Request : server.py methode send_coordinate
```python
send_coordinate(x : int, y : int) -> None
```

Permet d'envoyer les coordonées au serveur y et x sont des integer.

Class Request : server.py methode rcv_and_dechiffre_message
```python
rcv_and_dechiffre_message() -> bytes
```

Permet de recevoir des données. ***Attention !*** la fonction est bloquante seule les fonctions avec la mention **Une réponse est attendue** débloque le thread en cours.

### Exemple
```python
import maze_request

request = maze_request.Request("127.0.0.1", 6666, 6667, b"toto")
request.join()
message = request.rcv_and_dechiffre_message()

if message['type'] == "sucess_answer":
    secret_server_id = message['unique_id']
else:
    print(message['error'])
    exit()

request.ready() # Then we will wait for game param or change status

message = request.rcv_and_dechiffre_message() # waiting for game param
if message['type'] == "maze_param" and secret_server_id == message['unique_id']:
    
    maze_size        = message['maze_size']
    maze_seed        = message['maze_seed']

else:
    print(message['error'])

while "GAME":
    # must include x, y and addresse of client who send the coordinate
    message  = request.rcv_and_dechiffre_message()
     
```
