# Générateur de labyrinthe "maze_generator.py"
"maze_generator.py" est un module permettant de générer des labyrinthes carrés de taille variable. Pour être utilisé dans d'autres fichiers python, il doit être importé comme suit:
```python
from maze_generator import *
```
Un labyrinthe est composé de cases. Quatre types sont possibles : mur, chemin, entrée et sortie.
La configuration est présente dans le fichier "maze_config.py" qui est requis pour fonctionner correctement.

## L'algorithme :
Pour concevoir l'algorithme, je me suis inspiré de cette [vidéo](https://youtu.be/p3mymCWzhV8?t=86) (timestamp dans le lien).
Mon but est de générer un tableau en deux dimensions où chaque case représente un mur, un passage, l'entrée du labyrinthe ou la sortie du labyrinthe.

Le labyrinthe est séparé en salles de 3x3 où chaque milieu est un passage, chaque coin est un pilier (mur) et chaque côté est un mur pouvant être ouvert pour communiquer avec une salle adjacente.

Je débute alors en générant les piliers de chaque salle. Ensuite je peux commencer la génération par un système de chemins. 

Tant que le labyrinthe n'est pas rempli, je cherche la prochaine salle libre pour débuter un chemin. Je génère le sol et les murs de cette salle, puis ouvre un des murs. La salle adjacente à ce mur est notre prochaine salle. Je répète alors ce processus, jusqu'à rencontrer une salle déjà générée. Un chemin a alors été créé.

Lorsque toutes les salles ont été utilisées, j'ajoute alors l'entrée du labyrinthe dans la salle en haut à gauche puis sa sortie dans la salle en bas à droite.
## Fonctions disponnibles :
- [generateMaze](#generateMaze) : génère un labyrinthe
- [printMaze](#printMaze) : afficher un labyrinthe en ligne de commande
---
### generateMaze
Fonction principale du module. Elle permet, comme son nom l'indique de générer des labyrinthe en fonction d'une taille et d'une seed.
```python
generateMaze(size:int, seed:str="") -> list
```
### Paramètres :
- *size* : nombre entier qui correspond à la taille d'un des côtés du labyrinthe. Ce nombre doit être impaire, si ce n'est pas le cas, il sera incrémenté de un.
- *seed* : chaine de caractères qui définit la génération du labyrinthe. Deux labyrinthe générés de même taille et de même seed seront identique. **Cet argument est optionnel**
### Retour :
- un tableau en deux dimensions de caractères (variant selon la configuration).
### Exemple :
Générer un labyrinthe 25x25 avec la seed "test_purpose_seed"
**source :**
```python
maze = generateMaze(25,"test_purpose_seed")
```
---
### printMaze
Fonction principale du module. Elle permet, comme son nom l'indique de générer des labyrinthes en fonction d'une taille et d'une seed.
```python
printMaze(maze:list) -> None
```
### Paramètres :
- *maze* : un labyrinthe généré au préalable avec la fonction "generateMaze()"
### Retour :
*rien*
### Exemple :
Générer un labyrinthe 11x11 avec la seed "best_seed_ever", puis l'afficher.

**source :**
```python
maze = generateMaze(11,"best_seed_ever")
printMaze(maze)
```

**sortie console :**
```
#S#########
#         #
# ### #####
# #       #
# ### ### #
#   # #   #
### ### # #
# #   # # #
# # ### ###
#   #     #
#########E#
```

---
