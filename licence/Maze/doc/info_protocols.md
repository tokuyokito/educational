# Sommaire

* Documentation du protocole
    * Fonctionnement du protocole
    * Liste des requêtes
        * size
        * join
        * sucess_answer
        * leave
        * ready
        * not_ready
        * send_coordinate
        * relay_coordinate
        * victory
        * maze_param
        * error

## Fonctionnement du protocole
    Le protocole utilisé pour le projet maze est l'UDP pour sa rapidité, le format utilisé est le json. 
    Les données sont chiffrées grâce au chiffrement AES en mode EAX, puis encapsulées dans le champ cyphertext.

    ```json
    {
        "cyphertext": "données utiles chiffrées au format json",
        "tags"      : "tags renvoyé lors du chiffrement",
        "nonce"     : "nonce renvoyé lors du chiffrement"
    }
    ```

Le champ nonce sert pour le déchiffrage du message, le champ tags sert pour vérifier l'intégrité du message.

## Liste des requêtes
Les requêtes sont au nombre de 11. Le champ type définit le type de requête reçue.

### size
```json
{
    "type" : "size",
    "size" : "taille du message à recevoir"
}
```
La requête size est envoyée avant chaque message. Cela permet de recevoir correctement la requête suivante.

### join
```json
{
    "type" : "join"
}
```
La requête join permet de joindre un serveur.

### sucess_answer
```json
{
    "type" : "sucess_answer"
}
```
La requête sucess_answer est renvoyée en réponse à la requête join si celle-ci est fructueuse.

### leave
```json
{
    "type" : "leave"
}
```
La requête leave permet de quitter un serveur.

### ready
```json
{
    "type" : "ready"
}
```
La requête ready permet de se mettre en état prêt.

### not_ready
```json
{
    "type" : "not_ready"
}
```
La requête not_ready permet de se mettre en état non prêt.

### send_coordinate
```json
{
    "type" : "send_coordinate",
    "x"    : "coordonée x",
    "y"    : "coordonée y"
}
```
La requête send_coordinate permet d'envoyer les coordonnées au serveur.

### relay_coordinate
```json
{
    "type"   : "relay_coordinate",
    "x"      : "coordonée x",
    "y"      : "coordonée y",
    "player" : "adresse du propriétaire des coordonées"
}
```
La requête relay_coordinate est renvoyée en réponse à la requête send_coordinate à tous les clients sauf 
au propriétaire des coordonnées.

### victory
```json
{
    "type"  : "victory",
    "winer" : "adresse du gagnant"
}
```
La requête victory est envoyée à tous les clients si un joueur est sur la case sortie.

### maze_param
```json
{
    "type"      : "maze_param",
    "maze_size" : "taille du labyrinthe",
    "maze_seed" : "graine unique du labyrinthe"
}
```

La requête maze_param est envoyée à tous les joueurs quand la partie démarre. 
Elle contient la taille du labyrinthe et la graine unique.
### error
```json
{
    "type"  : "error",
    "error" : "message d'erreur"
}
```

La requête erreur est envoyée si le serveur n'arrive pas à effectuer une action.

