"""
Module server of maze project
"""
import threading
import socket
import json

from hashlib       import sha256
from sys           import path, stderr
from Crypto.Cipher import AES

path.insert(0, '../client')
import maze_generator
import maze_config
import player

# pip3 install pycryptodome
class Server(threading.Thread):
    """
    Server object run in new thread
    """
    ACCEPT   = "CONST_STATE_ACCEPT_CONECTION",
    FULL     = "CONST_STATE_FULL",
    RUNNING  = "CONST_STATE_SERVER_RUNNING",
    DEBUG    = False

    def __init__(self, server_port          : int,
                       secret_key           : bytes,
                       max_number_of_client : int,
                       maze_size            : int,
                       maze_seed            : str,
                       pseudo               : str="host"):

        threading.Thread.__init__(self)
        assert max_number_of_client > 0, "Zero or negative numbers are not valide"

        self.secret_key = sha256(secret_key).digest()
        self.server_port = server_port
        self.udp_socket_server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.udp_socket_server.bind(("0.0.0.0", server_port))
        self.state = Server.ACCEPT

        self.udp_socket_client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        self.client_number     =  1
        self.max_client_number = max_number_of_client

        self.players = []
        self.players.append(player.Player("127.0.0.1", None, None, pseudo))
        
        self.players[-1].x = maze_config.START_X
        self.players[-1].y = maze_config.START_Y
        
        self.players[0].state = player.Player.READY # Server's owner is always ready because he start the game

        self.maze_size = maze_size
        self.maze_seed = maze_seed
        self.maze = ""

        self.__keep_thread_alive = False

        self.game_is_win = (False, None)

    # Return -1 when the unique_id is not found
    def __search_client_by_id(self, id_to_found : str) -> int:
       
        for index, element in enumerate(self.players):
            
            if element.unique_id == id_to_found:
                return index

        return -1


    # Return -1 when the address and the port are not found
    def __search_client_by_address_and_port(self, address : str, port : str) -> int:

        for index, element in enumerate(self.players):

            if element.address == address and element.listen_port == port:
                return index

        return -1


    def __search_pseudo(self, pseudo_to_found:str) -> bool:
        for element in self.players:
            if pseudo_to_found == element.pseudo:
                return True
            
        return False


    def __increment_client_number(self) -> None:
        if not self.state in (Server.FULL, Server.RUNNING):
            self.client_number += 1

        if self.client_number == self.max_client_number:
            self.state = Server.FULL


    def __append_player(self, address:str, client_unique_id:str, port:str, pseudo:str) -> bool:

        address_is_found = self.__search_client_by_address_and_port(address, port)

        if address_is_found == -1 and self.__search_pseudo(pseudo) == False:

            self.players.append(player.Player(address, client_unique_id, port, pseudo))
            self.players[-1].x = maze_config.START_X
            self.players[-1].y = maze_config.START_Y

            return True

        return False


    def __decrement_client_number(self) -> None:
        self.client_number -= 1

        if self.state is not Server.RUNNING:
            self.state = Server.ACCEPT


    def __pop_player(self, client_unique_id : str) -> bool:

        unique_id_is_found = self.__search_client_by_id(client_unique_id)
        
        if unique_id_is_found > -1:
            del self.players[unique_id_is_found]
            return True

        return False


    def __handle_join(self, address:str, client_unique_id:str, port:str, pseudo:str) -> None:
        if self.state == Server.ACCEPT:

            if self.__append_player(address, client_unique_id, port, pseudo):
                self.__increment_client_number()
                self.__secure_send("sucess_answer",
                                (address, port),
                                unique_id=self.players[-1].unique_id_share_with_server)
                return
            
            self.__secure_send("error",
                            (address, port),
                            error=f"Pseudo or address is probably used")
            return

        self.__secure_send("error",
                        (address, port),
                        error=self.state)


    def __handle_leave(self, client:player.Player) -> None:

        if self.__pop_player(client.unique_id):
            
            self.__decrement_client_number()


    def __handle_ready(self, client:player.Player) -> None:
        client.state = player.Player.READY


    def __handle_not_ready(self, client:player.Player) -> None:
            client.state = player.Player.NOT_READY


    def __everyone_is_ready(self) -> bool:

        for client in self.players:
            if player.Player.NOT_READY == client.state:
                return False

        return True

    def __can_start(self) -> bool or str:

        if not self.__everyone_is_ready():
            message = "Everyone is not ready"

        elif self.state == Server.RUNNING:
            message = "Server is running"

        else:
            return True

        return message


    def start_game(self) -> str:
        can_it_start = self.__can_start()
        if  can_it_start == True:

            self.state = Server.RUNNING

            self.maze = maze_generator.generateMaze(self.maze_size, self.maze_seed)

            pseudo = []
            for element in self.players:
                pseudo.append([element.pseudo, element.x, element.y])

            for player in self.players:
                

                self.__secure_send("maze_param",
                                (player.address,
                                player.listen_port),
                                maze_size=self.maze_size,
                                maze_seed=self.maze_seed,
                                unique_id=player.unique_id_share_with_server,
                                players=pseudo)            
            
            return "start"
            
        return can_it_start


    def server_information(self) -> None:
        debbug_string = ""
        for client in self.players:
            debbug_string += f"{client.pseudo}:{client.address}:{client.listen_port} {client.state[0]} {client.x} {client.y}\n"

        with open("debugg.out", "a") as file:
            file.write(debbug_string)


    def __handle_request(self, data, address) -> None:
        player_index = self.__search_client_by_id(data['unique_id'])

        if data['type'] == 'coordinate': return
        
        if data['type'] == "join" and player_index == -1:
            self.__handle_join(address, data['unique_id'], data['listen_port'], data['pseudo'])

        elif data['type'] == "ready":
            self.__handle_ready(self.players[player_index])

        elif data['type'] == "not_ready":
            self.__handle_not_ready(self.players[player_index])
    
        elif data['type'] == "leave" and player_index > -1:
            self.__handle_leave(self.players[player_index])

        elif data['type'] == "ping":
            self.__secure_send("ack_ping", (address, int(data['port'])))
        
        elif data['type'] == "status":
            self.__secure_send("ack_status", (address, int(data['port'])), status=self.state[0])
        
        else:
            
            player = self.players[player_index]
            self.__secure_send("error", (player.address, player.listen_port), 
                                error="unknown request",)


    def __send_coordinates(self, x_coord : int, y_coord : int, client:player.Player) -> None:

        for player in self.players:
            if player.listen_port != None:
                self.__secure_send("relay_coordinate",
                                (player.address,
                                player.listen_port),
                                x=x_coord,
                                y=y_coord,
                                pseudo=client.pseudo,
                                unique_id=player.unique_id_share_with_server)


    def __chek_coordinate(self, client:player.Player, x:int, y:int) -> bool:
        
        if self.maze[x][y] == maze_config.WALL: return False

        x_delta = abs(abs(client.x) - abs(x))
        y_delta = abs(abs(client.y) - abs(y)) 

        if (x_delta == 1 and y_delta == 0) or (x_delta == 0 and y_delta == 1) or (x_delta == 0 and y_delta == 0): 
            
            return True
        
        return False
    
    
    def __recv_coordinate(self, data : dict, client:player.Player) -> bool:
        out_of_range = False
    
        try:
           self.maze[int(data['x'])][int(data['y'])]
           
        except IndexError:
            out_of_range = True
        
        if out_of_range or not self.__chek_coordinate(client, int(data['x']), int(data['y'])) or self.maze[int(data['x'])][int(data['y'])] == maze_config.WALL: 
            if client.listen_port != None:
                self.__secure_send("ack_coordinate", (client.address, int(data['port'])), 
                                                    accept="False", 
                                                    unique_id=client.unique_id_share_with_server)
            return False
        
        
        if self.maze[int(data['x'])][int(data['y'])] == maze_config.END:
            
            for player in self.players:
                if player.listen_port != None:

                    self.__secure_send("victory",
                                    (player.address,
                                    player.listen_port),
                                    winer=f"{client.pseudo}",
                                    unique_id=player.unique_id_share_with_server)
            
            self.game_is_win = (True, client.pseudo)
            
        else:
            client.x, client.y = int(data['x']), int(data['y'])
            self.__send_coordinates(data['x'], data['y'], client)
        
        if client.listen_port != None:
            self.__secure_send("ack_coordinate", (client.address, int(data['port'])), 
                                                accept="True", 
                                                unique_id=client.unique_id_share_with_server)
        
        return True

    def __rcv_and_dechiffre_message(self, msg_size : int) -> (dict, str):

        data, address = self.udp_socket_server.recvfrom(msg_size)
        raw_json = data.decode("utf-8")

        try:
            dic_message = json.loads(raw_json)

        except Exception as exception:
            return {"type" : "error", "error" : exception}, address

        cipher = AES.new(self.secret_key, AES.MODE_EAX, nonce=bytes(dic_message['nonce']))
        plaintext = cipher.decrypt(bytes(dic_message['ciphertext']))

        # return the message encapsulate in ciphertext field
        return json.loads(plaintext.decode("utf-8")), address


    def __secure_send(self, protocol_type : str, target : (str, int), **field) -> None:

        if target[1] == None:
            return None
        
        field['type'] = protocol_type
        message = json.dumps(field)

        cipher = AES.new(self.secret_key, AES.MODE_EAX)
        ciphertext, tags = cipher.encrypt_and_digest(bytes(message, encoding="utf-8"))

        message_send = {}
        message_send['ciphertext'] = list(ciphertext)
        message_send['tags']       = list(tags)
        message_send['nonce']      = list(cipher.nonce)

        json_message = json.dumps(message_send)
        self.udp_socket_client.sendto(bytes(json_message, encoding="utf-8"), target)


    def __play(self, data : dict, sender : str) -> None:
        client_index = self.__search_client_by_id(data['unique_id'])
        if data['type'] == "coordinate" and client_index > -1:
            self.__recv_coordinate(data, self.players[client_index])


    def run(self) -> None:
        
        self.__keep_thread_alive = True
        self.game_is_win = (False, None)
        
        while True:
            if not self.__keep_thread_alive: break

            data, address = self.__rcv_and_dechiffre_message(4096)
            
            self.__handle_request(data, address[0])
            
            if self.state == Server.RUNNING:
                self.__play(data, address[0])

            if Server.DEBUG:
                self.server_information()

        socket.socket.close(self.udp_socket_server)
        


    def stop(self) -> None:
        self.state = Server.ACCEPT
        self.__keep_thread_alive = False
        self.__secure_send("error", ("127.0.0.1", self.server_port), error="end_of_thread", unique_id="bad_id")


    def get_status(self) -> str:
        return self.state[0]

    
    def push_coordinates(self, x_coord : int, y_coord) -> bool:
        """
        Sends coordinates directly to the players' table without going through the network
        """
        if self.__recv_coordinate({'x': x_coord, 'y': y_coord}, self.players[0]):
            
            return True
        
        return False


    def get_players(self) -> dict:
        result = {}

        for element in self.players:
            result[element.pseudo] = {'x':element.x, 'y':element.y}

        return result
