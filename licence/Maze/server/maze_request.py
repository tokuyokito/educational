"""
Module use by client to communicate with server.
"""
import threading
import socket
import json

from string          import ascii_letters, digits, punctuation
from time            import sleep
from random          import choice
from hashlib         import sha256
from Crypto.Cipher   import AES
from multiprocessing import Process, Value

class Request(threading.Thread):
    """
    Class request is the object use to make easy the communication with the maze server server.
    """
    def __init__(self, server_address : str,
                       server_port    : int,
                       secret_key     : bytes):
        
        threading.Thread.__init__(self)
        
        self.server_port_and_address = (server_address, server_port)

        self.udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        self.udp_socket_server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.udp_socket_server.bind(("0.0.0.0", 0))

        self.secret_key = sha256(secret_key).digest()
        
        # Token send to server
        self.unique_id = "".join(choice(ascii_letters + digits + punctuation) for _ in range(16))

        # Token from the server
        self.unique_id_share_with_server = ""

        self.__keep_thread_alive = False
        self.__players = {}

        self.__join_state = False
        
        self.__white_list = ["sucess_answer", "error", "ack_ping", "ack_status"]

        self.game_is_win = (False, None)


    def join_game(self, nickname:str) -> str:
        """
        Allow to join a server. The server will reply so
        you must use __rcv_and_dechiffre_message after call this methode.
        A unique key will be send by server to Prevent identity substitution,
        you must save this key and compare the key send with future each message
        """
        if self.__join_state: return "Already join"

        self.udp_socket_server.setblocking(True)
        self.__secure_send("join", self.server_port_and_address, unique_id=self.unique_id,
                                                                 listen_port=self.udp_socket_server.getsockname()[1],
                                                                 pseudo=nickname)
        data = self.__rcv_and_dechiffre_message(4096, self.udp_socket_server)
        if data['type'] == "sucess_answer":
            self.unique_id_share_with_server = data['unique_id']
            self.__join_state = True
            return "join"
        
        return data['error']


    def leave(self) -> None:
        """
        Allow to leave a server you will be erase from server registre.
        Automatically use in destructor
        """
        self.__secure_send("leave", self.server_port_and_address, unique_id=self.unique_id)
        self.__keep_thread_alive = False
        self.__join_state        = False

        self.__secure_send("error", ("127.0.0.1", self.udp_socket_server.getsockname()[1]))


    def ready(self) -> dict:
        """
        Allow you to be ready in server registre
        """
        self.__secure_send("ready", self.server_port_and_address, unique_id=self.unique_id)
        
        data = {'type' : 'none'}
        while data['type'] != "maze_param":
            data = self.__rcv_and_dechiffre_message(4096, self.udp_socket_server)
        
        for info_player in data['players']:

            self.__players[info_player[0]] = {'x':info_player[1], 'y':info_player[2]}
        
        return data


    def not_ready(self) -> None:
        """
        Allow you to be not_ready in server registre
        """
        self.__secure_send("not_ready", self.server_port_and_address, unique_id=self.unique_id)


    def __rcv_and_dechiffre_message(self, msg_size:int, soc:socket.socket) -> dict:
        """
        Rcv udp message and dechiffre with self.secret_key
        """
        message = soc.recvfrom(msg_size)[0]
        raw_json = message.decode("utf-8")

        try:
            dic_message = json.loads(raw_json)

        except Exception as exception:
            return {"type" : "error", "error" : exception}

        cipher = AES.new(self.secret_key, AES.MODE_EAX, nonce=bytes(dic_message['nonce']))
        plaintext = cipher.decrypt(bytes(dic_message['ciphertext']))

        # message encapsulate in ciphertext field
        data = json.loads(plaintext.decode("utf-8"))

        
        #Bypass verification with token because we can't verify the server authenticity
        #When player is not register
        if data['type'] in self.__white_list:
            pass
             
        elif data['unique_id'] != self.unique_id_share_with_server:
            return {"type" : "error", "error" : "bad_sender"}   
        
        return data


    def __secure_send(self, protocol_type:str, target:(str, int), **field) -> None:
        """
        Chiffre and send across a udp socket
        """
    
        field['type'] = protocol_type
        message = json.dumps(field)

        cipher = AES.new(self.secret_key, AES.MODE_EAX)  

        ciphertext, tags = cipher.encrypt_and_digest(bytes(message, encoding="utf-8"))

        message_send = {}
        message_send['ciphertext'] = list(ciphertext)
        message_send['tags']       = list(tags)
        message_send['nonce']      = list(cipher.nonce)

        json_message = json.dumps(message_send)

        # Then we send the message
        self.udp_socket.sendto(bytes(json_message, encoding="utf-8"), target)


    def __do_push(self, coord_x : int, coord_y : int, ret_value:int) -> None:
        
        tmp_udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        tmp_udp_socket.bind(("0.0.0.0", 0))
        self.__secure_send("coordinate",
                         self.server_port_and_address,
                         unique_id=self.unique_id,
                         x=coord_x,
                         y=coord_y,
                         port=tmp_udp_socket.getsockname()[1])
        
        data = self.__rcv_and_dechiffre_message(4096, tmp_udp_socket)
        tmp_udp_socket.close()

        if data['accept'] == "True":
            ret_value.value = 1
            

    def push_coordinate(self, coord_x : int, coord_y : int) -> bool:
        """
        Send coordinate to server x and y must be integer
        """
        ret_value = Value('i', 0)
        
        action_push = Process(target=self.__do_push, args=[coord_x, coord_y, ret_value])
        
        action_push.start()
        action_push.join(timeout=2)
        action_push.terminate()

        return True if ret_value.value == 1 else False


    def run(self):
        
        self.__keep_thread_alive = True
        self.game_is_win = (False, None)

        while self.__keep_thread_alive:
            data = self.__rcv_and_dechiffre_message(4096, self.udp_socket_server)
            
            if data['type'] == "victory":
                self.game_is_win = (True, data['winer'])

            elif data['type'] == "relay_coordinate": 
                self.__players[data['pseudo']]['x'] = int(data['x'])
                self.__players[data['pseudo']]['y'] = int(data['y'])


    def __do_ping(self, return_value) -> int:
        tmp_udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        tmp_udp_socket.bind(("0.0.0.0", 0))

        self.__secure_send("ping", self.server_port_and_address, 
                            port=tmp_udp_socket.getsockname()[1],
                            unique_id=self.unique_id)
        
        data = self.__rcv_and_dechiffre_message(4096, tmp_udp_socket)
        
        tmp_udp_socket.close()

        if data['type'] == "ack_ping":
           return_value.value = 1


    def ping_server(self) -> bool:
        return_value = Value('i', 0)
        
        action_ping = Process(target=self.__do_ping, args=(return_value,))
        
        action_ping.start()
        action_ping.join(timeout=1)
        action_ping.terminate()
        
        return True if return_value.value == 1 else False
    
    
    def get_status(self) -> str:
        if not self.ping_server():
            return "SERVER_STOP"

        tmp_udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        tmp_udp_socket.bind(("0.0.0.0", 0))

        self.__secure_send("status", self.server_port_and_address, 
                                    port=tmp_udp_socket.getsockname()[1],
                                    unique_id=self.unique_id)
        
        data = self.__rcv_and_dechiffre_message(4096, tmp_udp_socket)
        
        tmp_udp_socket.close()

        if data['type'] == "ack_status":
            return data['status']

        return "unknow"


    def get_players(self) -> dict:
        return self.__players

"""
    def __del__(self):
        self.leave()
"""        
