from random import choice
from string import ascii_letters, digits, punctuation

class Player:
    
    READY     = "CONST_STATE_READY",
    NOT_READY = "CONST_STATE_NOT_READY",
    
    def __init__(self, address:str, unique_id:str, listen_port:str, pseudo:str):
        
        self.state       = Player.NOT_READY
        self.address     = address
        # Token send by client to prove identity
        self.unique_id   = unique_id
        self.listen_port = listen_port
        self.pseudo      = pseudo
        self.x           = -1
        self.y           = -1
        
        # Token send to client to prove identity
        self.unique_id_share_with_server = "".join(choice(ascii_letters + digits + punctuation) for _ in range(16))
        