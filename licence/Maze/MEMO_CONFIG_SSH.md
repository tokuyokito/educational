# Aide mise en place ssh

## Sous Linux
La mise en place d'un jeu de clés ssh permet d'éviter d'avoir à écrire son identifiant et son mot de passe à chaque push request.
Cela est optionnel mais apporte du confort pour le travail.

### Création d'une clé ssh
- ssh-keygen -t rsa -b 4096 -C 'user@test.fr'

Avec cette commande on génère une clé publique en .pub et une clé privée sans extension.
t : type de clé
b : taille de la clé
C : commentaire

### Ajout de la clé
- Test si l'agent ssh est démarré : eval "$(ssh-agent -s)"

Doit retourner quelque chose comme ça 
```bash
Agent pid 11620
```

- Ajout de la clé : ssh-add private-key
- Aller sur le site de github -> setting -> SSG and GPG keys -> New ssh key -> rentrer la clé dans le champ key et donner un nom a cette clé (donner un nom sert pour l'organisation).
- Tester la connection ssh entre vous et github : ssh -T git@github.com

Si toutes ces étapes sont bonnes alors il faut configurer notre projet.

### Configuration du projet
Le protocole utilisé pour les requêtes est le https. Pour basculer en ssh il faut utilser cette commande : git remote set-url origin git@github.com:<user>/<repo>.git

Après tester avec une requête pull. Normalement aucun mot de passe n'est demandé.
