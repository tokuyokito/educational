#!/usr/bin/env python3

import curses,time,math
import maze_config
import maze_game
from sys import path

path.insert(0,"../server")
import maze_request

stdscr = curses.initscr()
curses.start_color()
curses.use_default_colors()
curses.curs_set(0)
curses.noecho()
curses.cbreak()
stdscr.keypad(True)#disable enter
stdscr.nodelay(True)#getch->non bloquant


def screen_menu(choices:list, screen, title:str="", center:bool=True, padding_x:int=0, padding_y:int=0):
        screen.erase()
        screen.refresh()
        if center:
            sx, sy = screen.getmaxyx()
            maxsize = len(title)+2
            for i in choices:
                a = len(i)
                if a>maxsize: maxsize = a
            
            pady = round((sy-maxsize)/2)
            padx = 10
        else:
            padx = padding_x
            pady = padding_y
        
        title_padd = 4
        choice = 0
        cap = 0
        selector = maze_config.WIN_SELECTOR
        if title:
            screen.addstr(padx,pady,"o%so" % ('-'*(maxsize)))
            screen.addstr(padx+1,pady," %s " % title.center(maxsize))
            screen.addstr(padx+2,pady,"o%so" % ('-'*(maxsize)))
        
        for i in range(len(choices)):
            screen.addstr(padx+i+title_padd,pady+2, choices[i])
        screen.addch(padx+choice+title_padd,pady, selector)
        
        while cap!=10 and cap!=32:#enter or space
            cap = screen.getch()
            if cap==122:
                screen.addch(padx+choice+title_padd,pady, " ")
                choice = (choice - 1) % len(choices)
                screen.addch(padx+choice+title_padd,pady, selector)

            if cap==115:
                screen.addch(padx+choice+title_padd,pady, " ")
                choice = (choice + 1) % len(choices)
                screen.addch(padx+choice+title_padd,pady, selector)
        return choice

def screen_input(screen, title,center=True, x=0,y=0):
    #curses.echo()
    curses.curs_set(1)
    screen.erase()
    screen.nodelay(False)#getch->bloquant
    c = ""
    res = []
    maxsize = len(title)+2
    max = maxsize-2

    if center:
        sx, sy = screen.getmaxyx()
        y = round((sy-len(title)-2)/2)
        x = 10

    screen.addstr(x, y, "o%so" % ('-'*(maxsize)))
    screen.addstr(x+1, y, " %s " % title.center(maxsize))
    screen.addstr(x+2, y, "o%so" % ('-'*(maxsize)))
    screen.addstr(x+4,y,"> %s" % ('_'*(maxsize-2)))
    screen.move(x+4,y+2)
    while c!= "\n":
        c = screen.getkey()

        if c=="KEY_BACKSPACE" and not res==[]: res.pop(-1)
        if not "KEY_" in c and len(res)<max: res.append(c)
        screen.addstr(x+4,y,"> %s" % ('_'*(maxsize-2)))
        screen.move(x+4,y+len(res)+2)
        for i in range(len(res)):
            screen.addch(x+4,y+2+i,res[i])
    res.pop(-1)
    curses.curs_set(0)
    #curses.noecho()
    screen.nodelay(True)#getch->non bloquant
    return "".join(res)


def screen_info(screen, info:str, title:str="", center=True):
    screen.erase()
    screen.nodelay(False)#getch->bloquant
    c = ""
    maxsize = len(title)+2
    max = maxsize-2

    if center:
        sx, sy = screen.getmaxyx()
        y = round((sy-len(title)-2)/2)
        x = 10

    screen.addstr(x, y, "o%so" % ('-'*(maxsize)))
    screen.addstr(x+1, y, " %s " % title.center(maxsize))
    screen.addstr(x+2, y, "o%so" % ('-'*(maxsize)))

    oldx=4
    infoSize = sy-2*y
    for i in range(0,len(info),infoSize):
        screen.addstr(x+oldx,y+1,info[i:i+infoSize])
        oldx+=1

    screen.addstr(x+oldx+1,y+1,"[Espace] pour continuer".center(infoSize))

    while c!=" ":
        c = screen.getkey()
    screen.nodelay(True)#getch->non bloquant


def screen_instant(screen, text:str, center=True, x=10, y=0, erase=True):
    if erase: screen.erase()
    c = ""
    maxsize = len(text)+2
    max = maxsize-2

    if center:
        sx, sy = screen.getmaxyx()
        y = round((sy-len(text)-2)/2)

    infoSize = sy-2*y
    oldx=4
    for i in range(0,len(text),infoSize):
        screen.addstr(x+oldx,y+1,text[i:i+infoSize])
        oldx+=1
    screen.refresh()


choices = ["[Solo                ]", "[Rejoindre une partie]","[Héberger une partie ]", "[Quitter             ]"]
try:
    while True:
        stdscr.erase()
        choice  = screen_menu(choices,stdscr,title="MENU")

#-----------------------------------------

        if choice == 0:#solo
            choice = -1
            size = "30"
            seed = ""

            #setup loop
            while choice not in [2,3]:
                choice =  screen_menu(["Seed : %s" % seed, "Taille : %s" % size, "[lancer        ]", "[retour au menu]"], stdscr, "Créer une partie solo")
                
                if choice == 0: seed = screen_input(stdscr,"Choisissez une seed :")

                if choice == 1:
                    size = screen_input(stdscr,"Taille du labyrinthe :")
                    while not size.isdigit():
                        screen_info(stdscr, "La taille doit être un nombre.", title="Valeur no valide !".center(44))
                        size = screen_input(stdscr,"Taille du labyrinthe :")

            if choice == 2:
                game = maze_game.Game(stdscr, size=int(size), seed=seed)
                stdscr.nodelay(True)#getch->non bloquant
                res = game.start()
                if res: screen_info(stdscr, "Vous parvenez à sortir du labyrinthe", title="          Gagné !          ")
                else: screen_info(stdscr, "Retour au menu...", title="Vous venez de quitter la partie")

            choice = -1#choice reset

#-----------------------------------------

        if choice == 1:#join a game
            choice = -1
            serverAddr = "127.0.0.1"
            serverPort = "6666"
            serverPass = ""
            pseudo = "player"

            #setup loop
            while choice not in [4,5]:
                choice = screen_menu(["Adresse : %s" % serverAddr, "Port : %s" % serverPort, "Pass : %s" % serverPass, "Pseudo : %s" % pseudo, "[connexion     ]", "[retour au menu]"], stdscr, "Rejoindre une partie")

                if choice == 0: serverAddr = screen_input(stdscr,"Adresse du serveur :")

                if choice == 1:
                    serverPort = screen_input(stdscr,"Port du serveur :")
                    while not serverPort.isdigit():
                        screen_info(stdscr, "Le numéro du port doit être un nombre.", title="Valeur no valide !".center(44))
                        serverPort = screen_input(stdscr,"Port du serveur :")

                if choice == 2: serverPass = screen_input(stdscr,"Mot de passe de la partie :")

                if choice == 3: pseudo = screen_input(stdscr,"Choisissez votre pseudo :")

            #connection to the server
            if choice == 4:
                if not serverPass: serverPass = b"toto"
                else: serverPass = serverPass.encode("utf-8")
                 
                onlineGame = maze_game.OnlineGame(stdscr, serverAddr, int(serverPort), serverPass, pseudo)
                
                screen_instant(stdscr,"Connecting to the server...")
                res = onlineGame.connect()
                if res:
                    screen_instant(stdscr, "En attente du serveur, merci de patienter.")
                    onlineGame.ready()#wait for the server to start the game
                    
                    stdscr.nodelay(True)#getch->non bloquant
                    youWin = onlineGame.start()#proceed the game
                    
                    if youWin == pseudo: screen_info(stdscr, "Féliciation !", title=("%s remporte la partie !" % youWin))
                    elif youWin == 0: screen_info(stdscr, "Retour au menu...", title="Vous venez de quitter la partie")
                    else: screen_info(stdscr, "Dommage, vous ne finissez pas en première position.", title=("%s remporte la partie !" % youWin))
                    onlineGame.disconnect()
                else: screen_info(stdscr, "Le serveur n'accepte pas les connexions ou le mot de passe est mauvais.", title="Connexion au serveur impossible")

            choice = -1#choice reset

#-----------------------------------------

        if choice == 2:#host a game (currently test section)
            choice = -1
            serverPort = "6666"
            serverPass = ""
            serverSeed = ""
            mazeSize = "16"
            pseudo = "host"

            #setup loop
            while choice not in [5,6]:
                choice = screen_menu(["Port : %s" % serverPort, "Pass : %s" % serverPass, "Seed : %s" % serverSeed, "Taille : %s" % mazeSize, "Pseudo : %s" % pseudo, "[valider       ]", "[retour au menu]"], stdscr, "Héberger une partie")

                if choice == 0: 
                    serverPort = screen_input(stdscr,"Port du serveur :")
                    while not serverPort.isdigit():
                        screen_info(stdscr, "Le numéro du port doit être un nombre.", title="Valeur no valide !".center(44))
                        serverPort = screen_input(stdscr,"Port du serveur :")

                if choice == 1: serverPass = screen_input(stdscr,"Mot de passe de la partie :")

                if choice == 2: serverSeed = screen_input(stdscr,"Choisissez une seed :")

                if choice == 3:
                    mazeSize = screen_input(stdscr,"Taille du labyrinthe :")
                    while not mazeSize.isdigit():
                        screen_info(stdscr, "La taille du labyrinthe doit être un nombre.", title="Valeur non valide !".center(44))
                        mazeSize = screen_input(stdscr,"Taille du labyrinthe :")

                if choice == 4: pseudo = screen_input(stdscr,"Choisissez votre pseudo :")
            
            if choice == 5: #launching the server...
                screen_instant(stdscr, "Lancement du serveur...")
                if not serverPass: serverPass = b"toto"
                else: serverPass = serverPass.encode("utf-8")

                onlineHostedGame = maze_game.OnlineHostedGame(stdscr, int(serverPort), serverPass, int(mazeSize), serverSeed, pseudo)
                onlineHostedGame.listen()

                stdscr.nodelay(True)#getch->non bloquant
                choice = -1
                c = -1

                while c!= 32: #space
                    c = stdscr.getch()
                    screen_instant(stdscr, "En attente des joueurs...")
                    players = list(onlineHostedGame.getPlayers())
                    xstart = 12
                    xdelta = 0
                    for player in players:
                        screen_instant(stdscr, "J%s : %s" % (str(xdelta+1), player), x=xstart+xdelta, erase=False)
                        xdelta +=1
                    screen_instant(stdscr, "Appuyez sur [espace] pour débuter la partie", x=xstart+xdelta+1, erase=False)
                    time.sleep(0.2)
                
                youWin = onlineHostedGame.start()
                
                if youWin == pseudo: screen_info(stdscr, "Féliciation !", title=("%s remporte la partie !" % youWin))
                elif youWin == 0: screen_info(stdscr, "Retour au menu...", title="Vous venez de quitter la partie")
                else: screen_info(stdscr, "Dommage, vous ne finissez pas en première position.", title=("%s remporte la partie !" % youWin))

                onlineHostedGame.stop()

            choice = -1

#-----------------------------------------

        if choice == 3:#exit
            break

except Exception as e:#change error
    curses.curs_set(1)
    curses.echo()
    curses.nocbreak()
    stdscr.keypad(True)
    curses.endwin()
    print("Le programme a été intérompu")

curses.curs_set(1)
curses.echo()
curses.nocbreak()
stdscr.keypad(True)
curses.endwin()
