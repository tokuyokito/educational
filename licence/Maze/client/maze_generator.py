#!/usr/bin/env python3
import random, time
from maze_config import *

GRAPHICAL = False	#graphic maze generation
DIR = [[1,0],[0,-1],[-1,0],[0,1]] #possible directions (do not touch)

def printMaze(m:list):
	for i in m:
		print(''.join(i))
	print()


def generateMaze(size:int, seed:str="") -> list:
    if seed: random.seed(seed) #init
    size+= 1 if (not size%2) else 0
    way=[0,0] #[x,y] delta
    maze = [[EMPTY for j in range(size)] for i in range(size)] #empty maze
    for i in range(size): #add pillars and walls
    	for j in range(size):
    		if i==0 or i==size-1 or j==0 or j==size-1 or (i%2==0 and j%2==0): maze[i][j]=WALL
    
    #generation
    while not filled(maze):
    	#start a new path
    	x,y = findNext(maze) #next free case to start
    	populate(maze,x,y) #add a tile
    	openWall(maze,x,y) #open a wall with a connected 
    	xs,ys,way = nextTile(x,y) #next adjacent in maze (random direction)
    	#generate the path
    	while free(maze,xs,ys): #while next adj is free and in maze
    		populate(maze,xs,ys,way)
    		maze[xs+way[0]*-1+way[0]][ys+way[1]*-1+way[1]]=TRAIL
    		xs,ys,way = nextTile(xs,ys) #new adj case
    
    maze[START_X][START_Y] = START
    maze[-1][-2] = END
    
    return maze


def filled(maze:list) -> bool:
	ret = True
	size=len(maze)
	for i in range(size):
		for j in range(size):
			if maze[i][j]==EMPTY:
				ret = False
				break
		else: continue #break 1st loop if the second has been broken (-> free tile has been found)
		break
	return ret	


def findNext(maze:list) -> (int, int):
	size=len(maze)
	x = y = -1
	for i in range(0,size):
		for j in range(0,size):
			if maze[i][j]==EMPTY: #next empty tile
				x=i
				y=j
				break
		else: continue #break 1st loop if the second has been broken (-> free tile has been found)
		break
	return x,y


def populate(maze:list, x:int, y:int, w=[0,0]):
	maze[x+1][y]=maze[x-1][y]=maze[x][y+1]=maze[x][y-1]=WALL #four walls
	maze[x][y]=TRAIL #center
	maze[x+w[0]*-1][y+w[1]*-1]=TRAIL #open a path with previous tile
	if GRAPHICAL:
		printMaze(maze)
		time.sleep(0.1)


def free(maze:list, x:int, y:int) -> bool:
	ret = False
	size = len(maze)
	if x not in range(size-1) or y not in range(size-1): ret = False #in Maze ?
	else: ret = maze[x][y]==EMPTY #is tile populated ?
	return ret


def openWall(maze:list, x:int, y:int):
	size = len(maze)
	connected=[]
	for i in DIR: #find existants tiles around
		if x+i[0]*2 in range(size-1) and y+i[1]*2 in range(size-1):
			if maze[x+i[0]*2][y+i[1]*2]==TRAIL and not i in connected: connected.append(i)
	if connected==[]: connected=[[0,0]] #first tile
	res=random.choice(connected)
	maze[x+res[0]][y+res[1]]=TRAIL #open a wall with an existant tile


def nextTile(x:int, y:int) -> (int, int, list):
	xr = yr = -1
	way = random.choice(DIR)
	xr = x+way[0]*2
	yr = y+way[1]*2
	return xr,yr,way


if __name__ == "__main__": printMaze(generateMaze(int(input("size: "))))
