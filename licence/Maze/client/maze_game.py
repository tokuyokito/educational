import maze_generator
import maze_config
import curses, time
from sys import path

path.insert(0,"../server")
import maze_request
import server

#revoir les pairs de couleur !

class Game:
    def __init__(self, screen, size=30, seed=""):
        curses.init_pair(1, 20, 1)
        curses.init_pair(2, 40, 1)
        curses.init_pair(3, 100, 1)
        self.map = maze_generator.generateMaze(size, seed)
        self.mapSize = len(self.map)
        self.local_y = 0
        self.local_x = 1
        self.players = [] #used for multiplayer purpose
        self.screen = screen
        self.screen.erase()

    def __can_move(self, x=0, y=0):
        return True if self.map[self.local_y+y][self .local_x+x]!=maze_config.WALL else False

    def __render(self):
        for x in range(self.mapSize):
            for y in range(self.mapSize):
                self.screen.addch(x,y,self.map[x][y], curses.color_pair(3))
          
        #local player
        self.screen.addch(self.local_y, self.local_x, maze_config.PLAYER, curses.color_pair(1)) #change color

    def __action(self, c):
        if c==122 and self.local_y-1>=0 and self.__can_move(y=-1): self.local_y-=1   #z
        if c==113 and self.local_x-1>=0 and self.__can_move(x=-1): self.local_x-=1   #q
        if c==115 and self.local_y+1<=self.mapSize and self.__can_move(y=1): self.local_y+=1   #s
        if c==100 and self.local_x+1<=self.mapSize and self.__can_move(x=1): self.local_x+=1   #d

    def start(self):
        c = 0
        ret = True
        while c!=27:#echap
            c = self.screen.getch()
            self.__action(c)
            self.__render()
            self.screen.refresh()
            time.sleep(1/maze_config.FPS)
            if self.map[self.local_y][self.local_x]==maze_config.END: break
        
        if c==27: ret = False
        
        return ret


class OnlineGame(Game):
    def __init__(self, screen, server_ip:str, server_port:int, password:bytes, pseudo:str):
        self.screen = screen
        self.pseudo = pseudo
        self.connected = False
        self.requests = maze_request.Request(server_ip, server_port, password)

    def connect(self):
        if self.requests.ping_server() and self.requests.get_status()=="CONST_STATE_ACCEPT_CONECTION":#can connect
            if self.requests.join_game(self.pseudo): self.connected = True
        return self.connected
    
    def disconnect(self):
        self.requests.leave()
        self.connected = False

    def ready(self):
        if self.connected:
            parameters = self.requests.ready() #bloquant
            Game.__init__(self, self.screen, size=parameters["maze_size"], seed=parameters["maze_seed"])
    
    def __render(self):
        for x in range(self.mapSize):
            for y in range(self.mapSize):
                self.screen.addch(x,y,self.map[x][y], curses.color_pair(3))
          
        #other players
        players = self.requests.get_players()
        for player in players:
            self.screen.addch(players[player]['x'], players[player]['y'], maze_config.PLAYER, curses.color_pair(2))

        #local player
        self.screen.addch(self.local_y, self.local_x, maze_config.PLAYER, curses.color_pair(1)) #change color

    def __action(self, c):
        modif = [0,0]
        if c==122: modif[1] = -1    #z
        if c==113: modif[0] = -1    #q
        if c==115: modif[1] = 1     #s
        if c==100: modif[0] = 1     #d
        if modif!=[0,0] and self.requests.push_coordinate(self.local_y+modif[1],self.local_x+modif[0]): #only push if a key has been entered
            self.local_x += modif[0]
            self.local_y += modif[1]

    def start(self):#return 0 if [escape] was hit, else return the winner's name
        ret = 0
        c = 0
        self.requests.start()
        while c != 27 and not self.requests.game_is_win[0]: #not [escape] or game is not finished
            c = self.screen.getch()
            self.__action(c)
            self.__render()
            self.screen.refresh()
            time.sleep(1/maze_config.FPS)
        
        if self.requests.game_is_win[0]: ret = self.requests.game_is_win[1]
        
        return ret


class OnlineHostedGame(Game):
    def __init__(self, screen, serverPort:int, password:bytes, mazeSize:int, mazeSeed, pseudo:str, maxPlayer=4):
        self.screen = screen
        self.pseudo = pseudo
        self.connected = False
        self.server = server.Server(serverPort, password, maxPlayer, mazeSize, mazeSeed if mazeSeed else maze_config.DEFAULT_SEED, pseudo=pseudo)
        Game.__init__(self, self.screen, size=mazeSize, seed=mazeSeed if mazeSeed else maze_config.DEFAULT_SEED)
    
    def listen(self):
        self.server.start()

    def getPlayers(self):
        return self.server.get_players()
    
    def stop(self):
        self.server.stop()

    def __render(self):
        for x in range(self.mapSize):
            for y in range(self.mapSize):
                self.screen.addch(x,y,self.map[x][y], curses.color_pair(3))
          
        #other players
        players = self.getPlayers()
        for player in players:
            self.screen.addch(players[player]['x'], players[player]['y'], maze_config.PLAYER, curses.color_pair(2))

        #local player
        self.screen.addch(self.local_y, self.local_x, maze_config.PLAYER, curses.color_pair(1)) #change color

    def __action(self, c):
        modif = [0,0]
        if c==122: modif[1] = -1    #z
        if c==113: modif[0] = -1    #q
        if c==115: modif[1] = 1     #s
        if c==100: modif[0] = 1     #d
        if modif!=[0,0] and self.server.push_coordinates(self.local_y+modif[1],self.local_x+modif[0]): #only push if a key has been entered
            self.local_x += modif[0]
            self.local_y += modif[1]

    def start(self):#return 0 if [escape] was hit, else return the winner's name
        ret = 0
        c = 0
        self.server.start_game()
        self.screen.erase()
        while c != 27 and not self.server.game_is_win[0]: #not [escape] or game is not finished
            c = self.screen.getch()
            self.__action(c)
            self.__render()
            self.screen.refresh()
            time.sleep(1/maze_config.FPS)
        
        if self.server.game_is_win[0]: ret = self.server.game_is_win[1]
        
        return ret