Bienvenu dans Maze, cette application python fonctionne dans la console grâce au module "curses".

Votre but sera de vous échaper du labyrinthe seul ou en équipe.

Liste des librairies à installer avant lancement :
- curses
- vpython
- keyboard
- screeninfo
- pycryptodome
- hashlib
