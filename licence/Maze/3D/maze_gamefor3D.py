import maze_generator
import maze_config



class Game:
    def __init__(self, size=8, seed=-1):
        #curses.init_pair(1, 20, 1)
        #curses.init_pair(2, 100, 1)
        self.map = maze_generator.generateMaze(size)
        self.mapSize = len(self.map)
        self.y = 0
        self.x = 1
        #self.screen = screen
        #self.screen.erase()
        return #size , seed
    
    def get_map(self):
        return self.map
        print(self.map)

    def can_move(self, x=0, y=0):
        return True if self.map[self.y+y][self.x+x]!=maze_config.WALL else False

    def render(self):
        for x in range(self.mapSize):
            for y in range(self.mapSize):
                print("test")
                #self.screen.addch(x,y,self.map[x][y], curses.color_pair(2))
        #self.screen.addch(self.y, self.x, maze_config.PLAYER, curses.color_pair(1))

    def action(self, c):
        if c==122 and self.y-1>=0 and self.can_move(y=-1): self.y-=1   #z
        if c==113 and self.x-1>=0 and self.can_move(x=-1): self.x-=1   #q
        if c==115 and self.y+1<=self.mapSize and self.can_move(y=1): self.y+=1   #s
        if c==100 and self.x+1<=self.mapSize and self.can_move(x=1): self.x+=1   #d
        return self.y , self.x

    def start(self):
        c = 0
        while c!=27:#echap
            c = self.screen.getch()
            self.action(c)
            self.render()
            self.screen.refresh()
            #time.sleep(1/maze_config.FPS)
            if self.map[self.y][self.x]==maze_config.END:
                break

class OnlineGame(Game):
    def __init__(self, screen, saddr, sport, lport, size=30, seed=-1):
        Game.__init__(screen, size, seed)

