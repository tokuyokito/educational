# -*- coding: utf-8 -*-
"""
Created on Thu Jan 21 14:33:09 2021
@author: abiga
"""

import time
import sys
import vpython
from screeninfo import get_monitors
#from maze_config import *
from maze_generator import WALL
from maze_gamefor3D import *
from thekeyboard import *

def init_scene():
    scene1 = vpython.canvas()
    scene1.background = vpython.color.gray(0.2)
    scene1.autoscale = False
    scene1.resizable = False
    scene1.userzoom = False
    scene1.userspin = False
    scene1.userpan = False
    scene1.visible = True
    scene1.lights = []




    for monitor in get_monitors():
        print(str(monitor))

    scene1.width = (monitor.width-50)
    scene1.height = (monitor.height-170)


    vpython.rate(60)
    return scene1

def light(scene1):
    """def for lightning of the scene"""
    #light=vpython.local_light(pos=player.pos , color=vpython.color.yellow)
    #éclairage lointain
    distant_light1 = vpython.distant_light(canvas=scene1, direction=vpython.vector(0.22, 0.44, 0.88), color=vpython.color.black)
    #éclairage lointain
    distant_light2 = vpython.distant_light(canvas=scene1, direction=vpython.vector(-0.88, -0.22, -0.44), color=vpython.color.black)
    distant_light = vpython.distant_light(canvas=scene1, direction=vpython.vector(0, 1, 0), color=vpython.color.yellow)


def draw3d(maze: list, scene1):
    """def for constructing the laby"""
    size = len(maze)
    for i in range(size):
        for j in range(size):
            if maze[i][j] == WALL:
                #generate walls
                wall = vpython.box(canvas=scene1, pos=vpython.vector(i, 3, j), length=1, height=3, width=1, color=vpython.color.black, texture=vpython.textures.rough)
            if (i == 0 and j == 1):
                #generate start
                start = vpython.box(canvas=scene1, pos=vpython.vector(i, 3, j), length=1, height=3, width=1, color=vpython.color.red, texture=vpython.textures.rough)
            if (i == size-1 and j == size-2):
                #generate end
                end = vpython.box(canvas=scene1, pos=vpython.vector(i, 3, j), length=1, height=3, width=1, color=vpython.color.green, texture=vpython.textures.rough)
            if (i == 0 and j == 0):
                #generate the floor
                floor = vpython.box(canvas=scene1, pos=vpython.vector(i+size//2, 1.5, j+size//2), length=size, height=1, width=size, color=vpython.color.black, texture=vpython.textures.rough)


def initplayer(scene1):
    """initialize the player"""
    player = vpython.sphere(canvas=scene1, pos=vpython.vector(0, 2.5, 1), length=1, height=1, width=1, color=vpython.color.orange, opacity=0.5, texture=vpython.textures.metal, emissive=True)
    return player

def drawplayer(x: int, y: int, player):
    """def for draw the player"""
    
    player.pos = vpython.vector(x, 2.5, y)
    #light.pos=player.pos
    """return vpython.cyvector.vector"""
    return player.pos


def camera(x: int, y: int, scene1):
    """def for the position of the camera"""
    scene1.camera.pos = vpython.vector(0, 16, 1)
    scene1.camera.axis = vpython.vector(0, -16, -1)
    scene1.up = vpython.vector(0, 1, 0)
    scene1.forward = vpython.vector(0.0135145, -16.971, -1.07619e-05)
    scene1.center = vpython.vector(x, 5, y)
