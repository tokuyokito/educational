"""
Web interface for display capteur's data
"""
import sqlite3

from flask import Flask, request, render_template
from socket import gethostname
import matplotlib.pyplot as plt

app = Flask(__name__)

def get_data_from_bdd(nb_element, connection, cursor):
    querry = f""" SELECT date_saved, humidity, temperature FROM data_capteur ORDER BY _id DESC LIMIT {nb_element}"""

    cursor.execute(querry)
    answer = cursor.fetchall()
    answer.reverse()
    return answer

def create_graph(nb_element, connection, cursor) -> None:
    required_data_for_graph = get_data_from_bdd(nb_element, connection, cursor)

    tab_date        = []
    tab_humidity    = [] 
    tab_temperature = []
    
    for data_tuple in required_data_for_graph:
        tab_date.append(data_tuple[0])
        tab_humidity.append(data_tuple[1])
        tab_temperature.append(data_tuple[2])

    plt.figure(figsize=(15,8))
    plt.scatter(tab_date, tab_humidity, label="Humidity")
    plt.scatter(tab_date, tab_temperature, label="Temperature")
    plt.xticks(rotation=90)
    plt.legend()
    plt.savefig("static/fig.png")


@app.route('/', methods=["GET"])
def send_index():
    nb_element = request.args.get('nb_element')
    nb_element = int(nb_element) if nb_element else 0
    
    if nb_element > 0:
        connection = sqlite3.connect("/tmp/capteur_data.bdd")
        cursor = connection.cursor()

        create_graph(nb_element, connection, cursor)

        return render_template("index.html", computer_hostname=gethostname())

    return render_template("error.html")


if __name__ == "__main__":

    app.run(host='0.0.0.0', debug=True, threaded=True, port=80)