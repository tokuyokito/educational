FROM debian:10-slim

ENV HTTP_PROXY=http://10.0.0.1:3128
ENV HTTPS_PROXY=http://10.0.0.1:3128

RUN echo "Acquire::http::Proxy \"http://10.0.0.1:3128/\";" > /etc/apt/apt.conf.d/proxy.conf
RUN echo "Acquire::https::Proxy \"http://10.0.0.1:3128/\";" >> /etc/apt/apt.conf.d/proxy.conf

RUN apt update -y && apt install -y python3 python3-pip
RUN python3 -m pip install ansible

           
ENTRYPOINT [ "tail","-f", "/dev/null" ]
