"""
Simple REST API accross web
"""
from flask import Flask, request
from time import sleep
import json 

PATH_JSON_SENSOR_VALUE = "/tmp/sensor_values.json"

def get_json_value(path : str)-> str:
    with open(path, "r") as file:
        return file.read()


def get_humidity(json_str : str ):
    return json_str.split(",")[0] + "}"


def get_temp(json_str : str ):
    return "{" + json_str.split(",")[1] 


app = Flask(__name__)

@app.route('/api', methods=["GET"])
def hanlde_request():
    
    temp       = request.args.get('temp')
    humidity   = request.args.get('humidity')
    json_value = get_json_value(PATH_JSON_SENSOR_VALUE) 
    
    #sleep(0.5)

    if temp and humidity:
         
        return json_value
    
    elif temp:
        return get_temp(json_value)
    
    elif humidity:
        return get_humidity(json_value)    
    
    return "Wrong request"
    


if __name__ == "__main__": 
    
    app.run(host='0.0.0.0', debug=True, threaded=True)