from flask import Flask, request
import Adafruit_DHT
import json 
import time


def get_value(sensor : Adafruit_DHT, pin : int) -> tuple:
     humidity, temperature = Adafruit_DHT.read(sensor, pin)
     return humidity, temperature


def all_value_is_correct(value : tuple) -> bool:
    if None in value:
        return False
    
    return True


def print_value(value : tuple) -> None:
    print(f"Temp={value[1]:0.1f}C Humidity{value[0]:0.1f}%")


app = Flask(__name__)
DHT_SENSOR, DHT_PIN = Adafruit_DHT.DHT11, 4


@app.route('/api', methods=["GET"])
def hanlde_request():
    temp     = request.args.get('temp')
    humidity = request.args.get('humidity')

    if temp and humidity:
        humidity_value, temp_value = get_value(DHT_SENSOR, DHT_PIN)
         
        return json.dumps({"temp" : temp_value, "humidity" : humidity_value})
    
    elif temp:
        return json.dumps({"temp" : get_value(DHT_SENSOR, DHT_PIN)[1]})
    
    elif humidity:
        return json.dumps({"temp" : get_value(DHT_SENSOR, DHT_PIN)[0]})    
    
    return "Wrong request"


if __name__ == "__main__":
        
    app.run(host='0.0.0.0', debug=True)