"""
Get DHT value every 30 s then write in /tmp/sensor_values.json
"""
from time import sleep
import json
import Adafruit_DHT


def get_value(sensor : Adafruit_DHT, pin : int) -> tuple:
     humidity, temperature = Adafruit_DHT.read(sensor, pin)
     return humidity, temperature


def all_value_is_correct(value : tuple) -> bool:
    if None in value:
        return False
    
    return True


def print_value(value : tuple) -> None:
    print(f"Temp={value[1]}C Humidity{value[0]}%")

if __name__ == "__main__":
    
    DHT_SENSOR, DHT_PIN = Adafruit_DHT.DHT11, 4
    
    while True:    
        humidity_value, temp_value = get_value(DHT_SENSOR, DHT_PIN)
        json_value = json.dumps({"humidity" : humidity_value, "temp" : temp_value})

        with open("/tmp/sensor_values.json", "w") as file:
            file.write(json_value)
        
        sleep(30)
     