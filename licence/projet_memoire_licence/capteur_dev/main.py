#pip install dht11 RPi.GPIO
import Adafruit_DHT
import time
 
DHT_SENSOR = Adafruit_DHT.DHT11
# PIN 7
DHT_PIN = 4


def get_value(sensor : Adafruit_DHT, pin : int) -> tuple:
     humidity, temperature = Adafruit_DHT.read(sensor, pin)
     return humidity, temperature


def all_value_is_correct(value : tuple) -> bool:
    if None in value:
        return False
    
    return True


def print_value(value : tuple) -> None:
    print(f"Temp={value[1]:0.1f}C Humidity{value[0]:0.1f}%")

def main() -> None:

    while True:
        value = get_value(DHT_SENSOR, DHT_PIN)
        
        if all_value_is_correct(value):
            print_value(value)    
        
        else:
            print("Error")
        time.sleep(1)

if __name__ == "__main__":
    main()