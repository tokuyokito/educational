import sqlite3
from random import choice
from time import sleep

querry = """CREATE TABLE IF NOT EXISTS data_capteur(
            _id integer PRIMARY KEY,
            date_saved DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
            humidity FLOAT,
            temperature FLOAT)
            """

connection = sqlite3.connect("/tmp/capteur_data.bdd")
cursor = connection.cursor()

cursor.execute(querry)
connection.commit()

value = [i for i in range(31)]
for _ in range(301):
   
    temp     = choice(value)
    humidity = choice(value)
    
    querry = f"""INSERT INTO data_capteur (humidity, temperature) VALUES ({humidity}, {temp})"""
    cursor.execute(querry)
    sleep(1)

connection.commit()
    
    