#!/usr/bin/env python3
"""
Module use to generate host file from docker-compose.yaml
Every service will generate a pull name
To generate more than one machine you must precise with the --scale argument e.g:
python3 generate_conf_hosts.py --scale web=10 bdd=5
"""
import argparse
import os
import yaml

def generate_config(pull_name:str, hosts_names_prefix:str , server_range:list) -> str:
    """
    insert [pull_name] then hosts_names_prefix_i where i until server_range
    """
    data = f"[{pull_name}]\n"
    for i in range(1,server_range +1):
        data += f"{hosts_names_prefix}_{i}\n"

    return data + "\n"


def must_be_scale(service:str) -> int:
    """
    Return the number of hostname to generate if found in args.scale
    else None
    """

    if not args.scale:
        return None

    for argument_value in args.scale:
        if argument_value.split("=")[0] == service:
            return int(argument_value.split("=")[1])

    return None

parser = argparse.ArgumentParser(description="Generate host from docker-compose.yml")
parser.add_argument("--scale", nargs="+")
args = parser.parse_args()

parent_path = os.path.dirname(os.path.realpath(__file__)).split("/")[-1]

with open("docker-compose.yml", "r") as yamlfile:
    service_list = yaml.safe_load(yamlfile)

future_configuration = ""
for service in service_list['services']:

    server_range = must_be_scale(service) if must_be_scale(service) else 1
    future_configuration += generate_config(service, f"{parent_path}_{service}", server_range)

with open("hosts", "w") as file:
    file.write(future_configuration)
