/**
 * \file   comunication-basic.c
 * \brief  Communication entre deux processus père et fils.
 * \author Le Gall Cédric
 * \version 0.1
 */

#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#define PATH "/tmp/swap-valeur"
#define MAXI 1

int main(int argc, char const *argv[])
{

    // Création du fichier
    FILE* fichier = fopen(PATH, "w");
        
    fputs("1\n", fichier);
    
    fclose(fichier);

    
    char chaine[MAXI] = "";  
    int number = 0, i = 0 ;
    char curseur = 0;
    
    
    // Boucle infinie pour comencer la communication syncrone entre les deux processus
    while(1)
    {
   		
    	fichier = fopen(PATH, "r");
    	// Gestion des erreurs
    	if ( fichier == NULL) 
    	{
        	printf("Erreur ouverture fichier\n");
        	exit(0);
    	}       
   		
   		// Récupération valeur
   		i = 0;
   		while(curseur != '\n')
   		{
   			
   			curseur = fgetc(fichier);
   			chaine[i] = curseur;
   			i++;
   		}

   		fclose(fichier);
   		
   		// Sauvegarde donnée
   		fichier = fopen(PATH, "w");
   		
   		if ( fichier == NULL) 
    	{
        	printf("Erreur ouverture fichier\n");
        	exit(0);
    	}
   	 	
   	 	// Convertion valeur
   	 	number = atoi(chaine);
   	 	   	 	
   	 	number++;
   	 	
   	 	// Stockage valeur
   	 	sprintf(chaine, "%d\n", number);
		fputs(chaine, fichier);
		fclose(fichier);
   	 	
   	 	sleep(1);	        
    }   
	

    return EXIT_SUCCESS;
}