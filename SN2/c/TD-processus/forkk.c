#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

int main()
{
    pid_t   pid, pidCourant;

    pid = fork();
    pidCourant = getpid();

    if (pid > 0) { /* Processus père */
        printf("processus père, pid: %i\n", pidCourant);
        
        while (1) /* boucle infinie */
        {
            fprintf(stderr, "p");
            sleep(1);
        }

    } else if (pid == 0) { /* Processus fils */
        printf("processus fils, pid: %i\n", pidCourant);
        while (1) /* boucle infinie */
        {
            fprintf(stderr,"f");
            sleep(1);
        }
    }

    return 0;
}