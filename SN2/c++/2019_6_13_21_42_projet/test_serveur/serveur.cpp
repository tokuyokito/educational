#include <iostream>
#include <unistd.h>
#include <SFML/Network.hpp>

//g++ serveur.cpp -o serveur -Wall -lsfml-network

int main()
{
	//*
	sf::UdpSocket socketServer;
	// bind the socketServer to a port

	//*
	unsigned short port = 1700;
	if (socketServer.bind(port) != sf::UdpSocket::Done);

	char data[200];
	std::size_t received;	
	
	// UDP socketServer:
	//*
	sf::IpAddress sender;
	socketServer.setBlocking(false);

	std::cout << "Listen at " << port<< "\n";
	std::string dtaString;
	while(1)
	{

		if (socketServer.receive((void*) data, 200, received, sender, port) != sf::UdpSocket::Done);
		else
		{
			dtaString = data;
			std::cout << "Received " << received << " bytes from " << sender << " on port " << port << std::endl;
		}
			
		if (dtaString == "start")
		{
			std::cout << "Start\n";
		}
		else if (dtaString == "restart")
		{
			std::cout << "Restart\n";
		}
		else if (dtaString == "conection")
		{
			if (socketServer.send("Connection success", 18, sender, port) != sf::UdpSocket::Done);
		}
		else
		{
			std::cout << "Process data\n";
		}
  		
  	int i = 0;
    while (data[i] != '\0')
    {
        data[i] = '\0'; 
        i++; 
    }
	    sleep(5);
	}

  return 0;
    
}

