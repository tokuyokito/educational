#include "sqlite3c++.h"
extern "C" {
#include <sqlite3.h>
}
Sqlite3::Sqlite3(char *file)
    : _db(NULL),
      _zErrMsg(NULL),
      _rc(0),
      _sql(NULL),
      _data(NULL)
{
    _rc = sqlite3_open(file, &_db);
}
    
