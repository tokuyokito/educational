#ifndef SQLITE3CPP_H 
#define SQLITE3CPP_H 

#include <cstdlib>

extern "C" {
#include <sqlite3.h>
}

class Sqlite3 {
private:
    sqlite3    *_db;
    char       *_zErrMsg;
    int        _rc;
    char       *_sql;
    const char *_data;

public:
    Sqlite3(char *);

};

#endif // SQLITE3CPP_H 
