/* 
 * Connecting To Database
 * Following C code segment shows how to connect to an existing database.
 * If database does not exist, then it will be created and finally
 * a database object will be returned
 * 
 * Compilation: gcc -o prog01 prog01.c -l sqlite3 -Wall
 */
 
#include <stdio.h>
#include <sqlite3.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    sqlite3 *db;
    char   *zErrMsg = 0;
    int     rc;

    rc = sqlite3_open("test.db", &db);

    if (rc)
    {
        fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
        exit(0);
    }
    else
    {
        fprintf(stderr, "Opened database successfully\n");
    }
    sqlite3_close(db);

    return EXIT_SUCCESS;
}
