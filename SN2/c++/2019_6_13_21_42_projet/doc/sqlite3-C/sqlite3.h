#ifndef SQLITE3_H 
#define SQLITE3_H 

#include <cstdlib>
#include <sqlite3.h>


class Sqlite3 {
private:
    sqlite3    *db;
    char       *zErrMsg = 0;
    int        rc;
    char       *sql;
    const char *data = "Callback function called";

public:
    Sqlite3();

};

#endif // SQLITE3_H 
