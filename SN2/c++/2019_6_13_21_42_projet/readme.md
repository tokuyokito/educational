# What is this document

This document is the version management. When a build had the **depreciate** state. It must be not use anymore. 
You can read this document with a markdown viewer like [stackedit](https://stackedit.io/).

# Versions of the gateways's sofware

| Build number |         Build name         |        description          			| state        |
|:------------:|:--------------------------:|:-------------------------------------:|:------------:|
|       0      | single_chan_pkt_fwd-master |  C version    		      			| Deppreciate  |
|       1      | dragino_objet              |  C++ version of build 0    			| Depreciate   |
|       2      | hat_parametrable_reseau    |  Version with a configuration file    | Depreciate   |
|       3      | ServerDragino              |  Bug correction               		| Depreciate   |
|       4	   | ServerDraginolog           |  Improve error gestion                | Current 	   |	

# Other

| Build number |   Build name   |                              description                              |
|:------------:|:--------------:|:---------------------------------------------------------------------:|
|      O.1     |  test_serveur  | A test client,server used to test the server configuration depreciate |
|      O.2     | gestion_config |                 A prototype of the configuration file                 |
|      O.3     |  ClientDragino |   The current client used to change the configuration of the gateway  |

# Communication protocol

The communication protocol used is a personal. But it can be exploit because it is free and open-source.

## How it's work ? 

### Connection

When a technicien want to change the gateway's configuration, he need to used the softaware : **ClientDragino**.
At first a connection weft will be send to the server configuration if the logs are correct a weft connectionR will be send by the server to the client.
The ClientDragino will be fill the configuration fields automatically.

### Configuration

A reload weft will be send if the reconfiguration is a success a connectionR will be sand else the socket will return a string error.

## The protocol

### The format

|  Weft name  |                                                              format                                                             |
|:-----------:|:-------------------------------------------------------------------------------------------------------------------------------:|
|  connection |                                                     connection:user:password                                                    |
| connectionR |   connectionRreload:serverTosendData:portTosendData:portServerConfiguration:frenquency:spiBus:spreadFactor:newUser:newPassword  |
|    reload   | reload:user:password:serverTosendData:portTosendData:portServerConfiguration:frenquency:spiBus:spreadFactor:newUser:newPassword |

### The regex

|  Weft name  |                                                          Regex                                                          |
|:-----------:|:-----------------------------------------------------------------------------------------------------------------------:|
|  connection |                                                connection:(\w+):(\w+)\n*                                                |
| connectionR |           connectionR:(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}):(\d{1,4}):(\d{1,4}):(\d{1,10}):(\d):(\d{1,2}):(\w+)\n*          |
|    reload   | reload:(\w+):(\w{64}):(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}):(\d{1,4}):(\d{1,4}):(\d{1,10}):(\d):(\d{1,2}):(\w+):(\w{64})\n* |

# Default configuration

[serverTosendData] = 13.76.168.68
[portTosendData] = 1700
[portServerConfiguration] = 1701
[frenquency] = 868100000
[spiBus] = 0
[spreadFactor] = 7
[user] = technicien
[passwordMD-SHA-256] = 80de5ff8f3ec3bd11346725056dcae8f147d03c8c6b34b4c7a7fdb7edc86455e
