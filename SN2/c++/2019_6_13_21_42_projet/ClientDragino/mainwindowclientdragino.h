#ifndef MAINWINDOWCLIENTDRAGINO_H
#define MAINWINDOWCLIENTDRAGINO_H

#include <QMainWindow>
#include <QtNetwork>
#include <SFML/Network.hpp>

namespace Ui {
class MainWindowClientDragino;
}

class MainWindowClientDragino : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindowClientDragino(QWidget *parent = nullptr);
    ~MainWindowClientDragino();
public slots:

    /**
    @brief Check if the connection fields are correct.
    @return true if all fields are fill
    */
    bool verifFieldConnection();

    /**
    @brief Fill statues fields with correct values. Lock fields connection and push button connection. Unlock send button.
    */
    void setFieldStatusConnect();

    /**
    @brief Try to send a connection trame to server to test if the server is up
    */
    void connectToUdpServer();

    /**
    @brief unlock fields connection and push button connection. Lock send button.
    */
    bool verifFieldConfiguration();

    /**
    @brief Check if the configuration fields are correct.
    @return true if all fields are fill
    */
    void logOut();

    /**
    @brief Send an reload trame.
    */
    void ChangeConfiguration();

private:

    Ui::MainWindowClientDragino *ui;

    sf::UdpSocket socket;
    //sf::IpAddress* receiver;

    unsigned short port;
    unsigned short portSender;

    std::string addresse;
    sf::IpAddress sender;
    std::size_t received;

    QRegExp rx;
};

#endif // MAINWINDOWCLIENTDRAGINO_H
