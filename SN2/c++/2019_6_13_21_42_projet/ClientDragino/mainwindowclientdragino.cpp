#include "mainwindowclientdragino.h"
#include "ui_mainwindowclientdragino.h"

MainWindowClientDragino::MainWindowClientDragino(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindowClientDragino)
{
    ui->setupUi(this);
    socket.setBlocking(false);

    rx.setPattern(QString("connectionR:(\\d{1,3}.\\d{1,3}.\\d{1,3}.\\d{1,3}):(\\d{1,4}):(\\d{1,4}):(\\d{1,10}):(\\d):(\\d{1,2}):(\\w+)\n*"));

    connect(ui->pushButtonConnection,SIGNAL(clicked()),this, SLOT(connectToUdpServer()));
    connect(ui->pushButtonLogout, SIGNAL(clicked() ), this, SLOT(logOut() ));
    connect(ui->pushButtonSendConfig, SIGNAL(clicked()),this, SLOT(ChangeConfiguration()));

    ui->tabWidget->setTabText(0,"Logs");
    ui->tabWidget->setTabText(1,"Configuration");
    setWindowTitle("Client gateway Dragino");

    ui->textEditLog->setReadOnly(true);
    ui->pushButtonLogout->setEnabled(false);
    ui->pushButtonSendConfig->setEnabled(false);

    ui->lineEditPassword->setEchoMode(QLineEdit::Password);
    ui->lineEditNewPassword->setEchoMode(QLineEdit::Password);
}

/**
@brief Check if the connection fields are correct.
@return true if all fields are fill
*/
bool MainWindowClientDragino::verifFieldConnection()
{
    bool statutField = true;

    if(!ui->lineEditUsername->text().size())
    {
        ui->textEditLog->append("Please fill the username field.");
        statutField = false;
    }

    if(!ui->lineEditPassword->text().size())
    {
        ui->textEditLog->append("Please fill the password field.");
        statutField = false;
    }

    if(!ui->lineEditAddresseServer->text().size())
    {
        ui->textEditLog->append("Please fill the adresse field.");
        statutField = false;
    }

    if(!ui->lineEditPort->text().size())
    {
        ui->textEditLog->append("Please fill the port field.");
        statutField = false;
    }

    return statutField;
}

/**
@brief Fill statues fields with correct values.Lock fields connection and push button connection. Unlock and Logout button.
*/
void MainWindowClientDragino::setFieldStatusConnect()
{
    /*Fill fields*/
    ui->lineEditServerToSendData->setText(rx.cap(1).toUtf8());
    ui->lineEditPortTosendData->setText(rx.cap(2).toUtf8());
    ui->lineEditPortServerConfiguration->setText(rx.cap(3).toUtf8());
    ui->lineEditFrenquency->setText(rx.cap(4).toUtf8());
    ui->lineEditSpiBus->setText(rx.cap(5).toUtf8());
    ui->lineEditSpreadFactor->setText(rx.cap(6).toUtf8());
    ui->lineEditNewUser->setText(rx.cap(7).toUtf8());

    /*Lock fields*/
    ui->lineEditUsername->setEnabled(false);
    ui->lineEditPassword->setEnabled(false);
    ui->lineEditPort->setEnabled(false);
    ui->lineEditAddresseServer->setEnabled(false);
    ui->pushButtonConnection->setEnabled(false);

    /*Unlock fields*/
    ui->pushButtonLogout->setEnabled(true);
    ui->pushButtonSendConfig->setEnabled(true);
}

/**
@brief Try to send a connection trame to server to test if the server is up
*/
void MainWindowClientDragino::connectToUdpServer()
{
    ui->textEditLog->append("Try to connect please wait.");

    if(verifFieldConnection())
    {
        /*Trame connection*/
        QString data("connection:");
        data += ui->lineEditUsername->text();
        data += ":";

        QCryptographicHash passwordsha256(QCryptographicHash::Sha256);

        passwordsha256.addData(ui->lineEditPassword->text().toUtf8());
        passwordsha256.result();

        data += passwordsha256.result().toHex();
        data += '\0';
        /*end trame*/

        /*Send a connection trame*/
        socket.send(data.toStdString().c_str(), data.length(),
                    sf::IpAddress(ui->lineEditAddresseServer->text().toStdString()),
                    ui->lineEditPort->text().toUShort());

        char buffer[200] = {'\0'};
        QThread::msleep(500);

        /*If password, username and the connection trame are goods the server will send a connction respond trame*/
        if((socket.receive(buffer, 200 ,received, sender, portSender) == sf::Socket::Done) &&
                rx.indexIn(QString(buffer)) != -1)
        {
            ui->textEditLog->append("Connexion success !");
            setFieldStatusConnect();
        }
        else if (QString(buffer).size())
        {
            /*Error dectected by the server*/
            ui->textEditLog->append(QString(buffer));
        }
        else
        {
            /*Connection no etablished*/
            ui->textEditLog->append("Connexion error !");
        }

    }
}

/**
@brief unlock fields connection and push button connection. Lock send and Logout button.
*/
void MainWindowClientDragino::logOut()
{

    ui->textEditLog->append("Log out");

    /*Unlock fields connection*/
    ui->lineEditUsername->setEnabled(true);
    ui->lineEditPassword->setEnabled(true);
    ui->lineEditPort->setEnabled(true);
    ui->lineEditAddresseServer->setEnabled(true);
    ui->pushButtonConnection->setEnabled(true);

    /*Lock send button and Logout button*/
    ui->pushButtonLogout->setEnabled(false);
    ui->pushButtonSendConfig->setEnabled(false);
}

/**
@brief Check if the configuration fields are correct.
@return true if all fields are fill
*/
bool MainWindowClientDragino::verifFieldConfiguration()
{
    bool statutField = true;

    if(!ui->lineEditServerToSendData->text().size())
    {
        ui->textEditLog->append("Please fill the serverToSendData field.");
        statutField = false;
    }

    if(!ui->lineEditPortTosendData->text().size())
    {
        ui->textEditLog->append("Please fill the PortTosendData field.");
        statutField = false;
    }

    if(!ui->lineEditPortServerConfiguration->text().size())
    {
        ui->textEditLog->append("Please fill the PortServerConfiguration field.");
        statutField = false;
    }

    if(!ui->lineEditFrenquency->text().size())
    {
        ui->textEditLog->append("Please fill the Frenquency field.");
        statutField = false;
    }

    if(!ui->lineEditSpiBus->text().size())
    {
        ui->textEditLog->append("Please fill the SpiBus field.");
        statutField = false;
    }

    if(!ui->lineEditSpreadFactor->text().size())
    {
        ui->textEditLog->append("Please fill the SpreadFactor field.");
        statutField = false;
    }

    if(!ui->lineEditNewUser->text().size())
    {
        ui->textEditLog->append("Please fill the NewUser field.");
        statutField = false;
    }

    if(!ui->lineEditNewPassword->text().size())
    {
        ui->textEditLog->append("Please fill the NewPassword field.");
        statutField = false;
    }

    return  statutField;
}

/**
@brief Send an reload trame.
*/
void MainWindowClientDragino::ChangeConfiguration()
{

    if(verifFieldConfiguration())
    {
        /*Trame reload*/
        QString data("reload:");
        data += ui->lineEditUsername->text().toUtf8();
        data += ":";

        QCryptographicHash passwordsha256(QCryptographicHash::Sha256);
        passwordsha256.addData(ui->lineEditPassword->text().toUtf8());
        passwordsha256.result();

        data += passwordsha256.result().toHex();
        ui->textEditLog->append(passwordsha256.result().toHex());
        data += ":" + ui->lineEditServerToSendData->text().toUtf8()+ ":" + ui->lineEditPortTosendData->text().toUtf8() + ":";
        data += ui->lineEditPortServerConfiguration->text().toUtf8()+":" + ui->lineEditFrenquency->text().toUtf8() + ":" +  ui->lineEditSpiBus->text().toUtf8() + ":";
        data += ui->lineEditSpreadFactor->text().toUtf8() + ":" + ui->lineEditNewUser->text() +":";

        QCryptographicHash newpasswordsha256(QCryptographicHash::Sha256);
        passwordsha256.addData(ui->lineEditNewPassword->text().toUtf8());
        passwordsha256.result();

        data += newpasswordsha256.result().toHex();
        data += '\0';
        /*end trame*/

        /*Send the reload trame*/
        socket.send(data.toStdString().c_str(), data.length(),
                    sf::IpAddress(ui->lineEditAddresseServer->text().toStdString()),
                    ui->lineEditPort->text().toUShort());

        char buffer[200];
        QThread::msleep(1000);

        if((socket.receive(buffer, 200 ,received, sender, portSender) == sf::Socket::Done) &&
                rx.indexIn(QString(buffer)) != -1)
        {

            ui->textEditLog->append("Changement success !");
            setFieldStatusConnect();
        }
        else
        {
            ui->textEditLog->append(QString(buffer));
        }

    }
}

    MainWindowClientDragino::~MainWindowClientDragino()
    {
        delete ui;
    }
