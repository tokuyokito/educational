#ifndef _DRAGINO_HAT_CONFIG_21_MAY_2019_BTS_SN2_LANNION
#define _DRAGINO_HAT_CONFIG_21_MAY_2019_BTS_SN2_LANNION

#include <iostream>
#include <fstream>
#include <regex>
#include <cstdlib> 

enum sf_t { SF_ERROR = -1, SF7=7, SF8, SF9, SF10, SF11, SF12 };

enum DraginoConfigError { PATH_EMPTY = -2, CANT_OPEN_CONFIG, NO_ERROR };

class DraginoConfig
{
private:
	
	std::ifstream _myConfig;
	std::ofstream _writeInMyConfig;
	
	std::string _dataFile;
	std::string _pathConfig;
    std::regex _validationConfig;

    std::string _serverTosendData;
    int _portTosendData;
    int _portServerConfiguration;
    uint32_t _frenquency;
    int _spiBus;
    sf_t _spreadFactor;
    
    std::string _userName;
    std::string _password;

    DraginoConfigError _statue;

	bool analyseData();
	void extractData();

public:

	DraginoConfig(const char* pathToConfig);

	DraginoConfigError load();
	
	bool save(const char* serverTosendData, const char* portTosendData, const char* portServerConfiguration, const char* frenquency, const char* spiBus, const char* spreadFactor, const char* user,const char* password);

	char* getServerTosendData();
	
	int   getPortTosendData();
	
	int	getPortServerConfiguration();
	
	uint32_t getFrenquency();
	
	int getSpiBus();
	
	sf_t getSpreadFactor();
	
	char* getUser();
	
	char* getPassword();
	
	DraginoConfigError getCurrentStatue(){ return _statue;}

	~DraginoConfig();
	
};

#endif