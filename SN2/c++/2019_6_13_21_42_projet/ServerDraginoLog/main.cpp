#include "draginoHat.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/file.h>
#include <sys/stat.h>

void demon(const char *fTmpLock)
{
    int fd;
    char str[2];

    /* masque pour création de fichier */
    umask(022);

    /* ouverture syslog */
    openlog(NOM_DEMON, LOG_PID, LOG_DAEMON);

    /* se positionne dans /tmp */
    chdir("/tmp");

    /* une seule exécution par verrouillage sur fichier */
    fd = open(fTmpLock, O_RDWR | O_CREAT, 0640);
    
    if (fd < 0) {
        syslog(LOG_ERR, "Can't open the lock file: %s", fTmpLock);
        exit(EXIT_FAILURE);
    }

    if (lockf(fd, F_TLOCK, 0) < 0) 
    {
        
        syslog(LOG_WARNING, "The service is already start");
        exit(EXIT_SUCCESS);
    }
    
    snprintf(str, 12, "%d\n", getpid());
    write(fd, str, strlen(str));

    /* informer du démarrage */
    syslog(LOG_INFO, "Service run");

    closelog();
}

int main(int argc, char const *argv[])
{
	demon("dragino.lock");
	DraginoHat myHat("/etc/dragino.conf");
	myHat.info();
	myHat.start();

	return 0;
}
