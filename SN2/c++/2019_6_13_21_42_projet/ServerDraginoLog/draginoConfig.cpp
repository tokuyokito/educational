#include "draginoConfig.h"

DraginoConfig::DraginoConfig(const char* pathToConfig)
{
	_pathConfig = pathToConfig;
	_validationConfig = R"(\[serverTosendData\]\s*=\s*(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})\n\[portTosendData\]\s*=\s*(\d{1,4})\n\[portServerConfiguration\]\s*=\s*(\d{1,4})\n\[frenquency\]\s*=\s*(\d{1,10})\n\[spiBus\]\s*=\s*(\d)\n\[spreadFactor\]\s*=\s*(\d{1,2})\n\[user\]\s*=\s*(\w+)\n\[passwordMD-SHA-256\]\s*=\s*(\w{64})\n*)";
	_statue = load();

}

bool DraginoConfig::analyseData()
{
    return std::regex_match(_dataFile, _validationConfig); 
}

void DraginoConfig::extractData()
{
    std::smatch dataMatch;

        if(std::regex_search(_dataFile, dataMatch, _validationConfig)) 
        {            
            _serverTosendData = dataMatch.str(1).c_str();
            _portTosendData = atoi(dataMatch.str(2).c_str()); 
            _portServerConfiguration = atoi(dataMatch.str(3).c_str());
            _frenquency = (uint32_t) atoi(dataMatch.str(4).c_str());
            _spiBus = atoi( dataMatch.str(5).c_str());
            
           int spreadStringTosf_t = atoi(dataMatch.str(6).c_str());

            switch(spreadStringTosf_t)
            {
                case 7:
                    _spreadFactor = SF7;
                    break;

                case 8:
                    _spreadFactor = SF8;
                    break;

                case 9:
                    _spreadFactor = SF9;
                    break;

                case 10:
                    _spreadFactor = SF10;
                    break;

                case 11:
                    _spreadFactor = SF11;
                    break;
            
                case 12:
                    _spreadFactor = SF12;
                    break;

                default:
                _spreadFactor = SF_ERROR;
            }
            
            _userName = dataMatch.str(7).c_str();
            _password = dataMatch.str(8).c_str();
        }
        	
}


DraginoConfigError DraginoConfig::load()
{
	if (_pathConfig.empty())
	{
		std::cout << "Error path empty";
        return PATH_EMPTY;
       
	}
	
    _myConfig.open(_pathConfig.c_str());
    
    if (!_myConfig)
    {
        std::cout << "Error open file\n";
        return CANT_OPEN_CONFIG;
        
    }

    /*Read file*/
    std::string ligne;
    _dataFile.clear();
    
    while(getline(_myConfig, ligne))
    {
        _dataFile += ligne +="\n";
    }
    
    _myConfig.close();
    
    analyseData();

    extractData();

    return NO_ERROR;
}

bool DraginoConfig::save(const char* serverTosendData, const char* portTosendData, const char* portServerConfiguration, const char* frenquency, const char* spiBus, const char* spreadFactor, const char* user,const char* password)
{
	_writeInMyConfig.open(_pathConfig.c_str());

    if (_writeInMyConfig)
    {
        _writeInMyConfig << "[serverTosendData] = " << serverTosendData << "\n[portTosendData] = " << portTosendData <<"\n[portServerConfiguration] = " << portServerConfiguration << "\n[frenquency] = " << frenquency << "\n[spiBus] = " << spiBus << "\n[spreadFactor] = " << spreadFactor << "\n[user] = " << user << "\n[passwordMD-SHA-256] = " << password;
        _writeInMyConfig.close();
        
        load();

        return true;
    }

    _writeInMyConfig.close();

    return false;
}

char* DraginoConfig::getServerTosendData()
{
	return (char*)_serverTosendData.c_str();
}

int DraginoConfig::getPortTosendData()
{
	return _portTosendData;
}

int DraginoConfig::getPortServerConfiguration()
{
	return _portServerConfiguration;
}

uint32_t DraginoConfig::getFrenquency()
{
	return _frenquency;
}

int DraginoConfig::getSpiBus()
{
	return _spiBus;
}

sf_t DraginoConfig::getSpreadFactor()
{
	return _spreadFactor;
}

char* DraginoConfig::getUser()
{
    return (char*)_userName.c_str();
}
char* DraginoConfig::getPassword()
{
    return (char*)_password.c_str();
}
DraginoConfig::~DraginoConfig()
{

}