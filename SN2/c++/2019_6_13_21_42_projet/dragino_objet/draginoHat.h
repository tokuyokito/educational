#ifndef _DRAGINO_HAT_LORA_13_MARTCH_2019_BTS_SN2_LANNION
#define _DRAGINO_HAT_LORA_13_MARTCH_2019_BTS_SN2_LANNION

#include "base64.h"
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <ctime>
#include <cmath>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <wiringPi.h>
#include <wiringPiSPI.h>
#include <iostream>
#include <string>


/*<--------------------------Public Constant-------------------------->*/
#define REG_FIFO                    0x00
#define REG_FIFO_ADDR_PTR           0x0D
#define REG_FIFO_TX_BASE_AD         0x0E
#define REG_FIFO_RX_BASE_AD         0x0F
#define REG_RX_NB_BYTES             0x13
#define REG_OPMODE                  0x01
#define REG_FIFO_RX_CURRENT_ADDR    0x10
#define REG_IRQ_FLAGS               0x12
#define REG_DIO_MAPPING_1           0x40
#define REG_DIO_MAPPING_2           0x41
#define REG_MODEM_CONFIG            0x1D
#define REG_MODEM_CONFIG2           0x1E
#define REG_MODEM_CONFIG3           0x26
#define REG_SYMB_TIMEOUT_LSB  		0x1F
#define REG_PKT_SNR_VALUE			0x19
#define REG_PAYLOAD_LENGTH          0x22
#define REG_IRQ_FLAGS_MASK          0x11
#define REG_MAX_PAYLOAD_LENGTH 		0x23
#define REG_HOP_PERIOD              0x24
#define REG_SYNC_WORD				0x39
#define REG_VERSION	  				0x42

#define SX72_MODE_RX_CONTINUOS      0x85
#define SX72_MODE_TX                0x83
#define SX72_MODE_SLEEP             0x80
#define SX72_MODE_STANDBY           0x81

#define PAYLOAD_LENGTH              0x40

// LOW NOISE AMPLIFIER
#define REG_LNA                     0x0C
#define LNA_MAX_GAIN                0x23
#define LNA_OFF_GAIN                0x00
#define LNA_LOW_GAIN		    	0x20

// CONF REG
#define REG1                        0x0A
#define REG2                        0x84

#define SX72_MC2_FSK                0x00
#define SX72_MC2_SF7                0x70
#define SX72_MC2_SF8                0x80
#define SX72_MC2_SF9                0x90
#define SX72_MC2_SF10               0xA0
#define SX72_MC2_SF11               0xB0
#define SX72_MC2_SF12               0xC0

#define SX72_MC1_LOW_DATA_RATE_OPTIMIZE  0x01 // mandated for SF11 and SF12

// FRF
#define        REG_FRF_MSB              0x06
#define        REG_FRF_MID              0x07
#define        REG_FRF_LSB              0x08

#define        FRF_MSB                  0xD9 // 868.1 Mhz
#define        FRF_MID                  0x06
#define        FRF_LSB                  0x66

#define BUFLEN 2048  //Max length of buffer

#define PROTOCOL_VERSION  1
#define PKT_PUSH_DATA 0
#define PKT_PUSH_ACK  1
#define PKT_PULL_DATA 2
#define PKT_PULL_RESP 3
#define PKT_PULL_ACK  4

#define TX_BUFF_SIZE  2048
#define STATUS_SIZE	  1024

//#define SERVER1 "54.72.145.119"
//#define PORT 1700

typedef unsigned char byte;
enum sf_t { SF7=7, SF8, SF9, SF10, SF11, SF12 };


class DraginoHat
{

private:
	
	/*<--------------------------Private attribu-------------------------->*/

	int CHANNEL;

	byte currentMode;

	char message[256];
	char b64[256];

	bool sx1272;

	byte receivedbytes;

	struct sockaddr_in si_other;
	int s, slen;
	struct ifreq ifr;

	uint32_t cp_nb_rx_rcv;
	uint32_t cp_nb_rx_ok;
	uint32_t cp_nb_rx_bad;
	uint32_t cp_nb_rx_nocrc;
	uint32_t cp_up_pkt_fwd;

	// SX1272 - Raspberry connections
	int ssPin;
	int dio0;
	int RST;

	// Set spreading factor (SF7 - SF12)
	sf_t sf;

	// Set center frequency
	uint32_t  freq; // in Mhz! (868.1)

	// Set location
	float lat;
	float lon;
	int   alt;

	/* Informal status fields */
	char platform[24];  /* platform definition */
	char email[40];           /* used for contact email */
	char description[64];                   /* used for free form description */
	char* SERVER1; 	
	int PORT;

public:

	/*<--------------------------Public constructor-------------------------->*/
	
	DraginoHat(uint32_t frenquency = 868100000, const char* addresseServer = "127.0.0.1", int portServer = 80000, sf_t spreadFactor = SF7, int SPIChannel = 0);
	
	/*<--------------------------Public methodes-------------------------->*/
	
	void die(const char *s);
	
	void selectreceiver();
	
	void unselectreceiver();
	
	byte readRegister(byte addr);
	
	void writeRegister(byte addr, byte value);
	
	bool receivePkt(char *payload);
	
	void SetupLoRa();
	
	void sendudp(char *msg, int length);
	
	void sendstat();
	
	void receivepacket();
	
	void info();

	void start();
	/*<--------------------------Methodes for configuration-------------------------->*/
	
	/*<--------------------------Accesseurs-------------------------->*/

	/**
	@brief Destructor
	*/
	~DraginoHat();

};

//_DRAGINO_HAT_LORA_13_MARTCH_2019_BTS_SN2_LANNION
#endif

