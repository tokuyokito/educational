// source : https://en.cppreference.com/w/cpp/regex/regex_search

#include <iostream>     // std::cout
#include <fstream>      // std::ifstream
#include <regex>
#include <cstdlib> 
enum sf_t { SF7=7, SF8, SF9, SF10, SF11, SF12 };

int main(int argc, char const *argv[])
{
	std::ifstream _myConfig("config");
	std::string _dataFile;
    std::regex _validationConfig(R"(\[serverTosendData\]\s*=\s*(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})\n\[portTosendData\]\s*=\s*(\d{1,4})\n\[portServerConfiguration\]\s*=\s*(\d{1,4})\n\[frenquency\]\s*=\s*(\d{1,10})\n\[spiBus\]\s*=\s*(\d)\n\[spreadFactor\]\s*=\s*(\d{1,2})\n*)");

    std::string _serverTosendData;
    int _portTosendData;
    int _portServerConfiguration;
    uint32_t _frenquency;
    int _spiBus;
    sf_t _spreadFactor;

    /*Open file*/
    if (!_myConfig)
    {
        std::cout << "Error open file\n";
    }

    /*Read file*/
    std::string ligne;
    while(std::getline(_myConfig, ligne)) //Tant qu'on n'est pas à la fin, on lit
    {
        _dataFile += ligne +="\n";
    }
    
    _myConfig.close();

    /*Analyse data*/
    if (std::regex_match(_dataFile, _validationConfig) == true)
    {
    	std::cout << "It's match \n";
    }
    else
    {
    	std::cout <<"don't match\n";
    }
    
    /*extract data*/
    std::smatch dataMatch;

        if(std::regex_search(_dataFile, dataMatch, _validationConfig)) 
        {
            
        }

        _serverTosendData = dataMatch.str(1).c_str();
        _portTosendData = atoi(dataMatch.str(2).c_str()); 
        _portServerConfiguration = atoi(dataMatch.str(3).c_str());
        _frenquency = (uint32_t) atoi(dataMatch.str(4).c_str());
        _spiBus = atoi( dataMatch.str(5).c_str());
        
       int spreadStringTosf_t = atoi(dataMatch.str(6).c_str());

        switch(spreadStringTosf_t)
        {
            case 7:
                _spreadFactor = SF7;
                break;

            case 8:
                _spreadFactor = SF8;
                break;

            case 9:
                _spreadFactor = SF9;
                break;

            case 10:
                _spreadFactor = SF10;
                break;

            case 11:
                _spreadFactor = SF11;
                break;
        
            case 12:
                _spreadFactor = SF_ERROR;
                break;
        }
	
        std::cout << "_serverTosendData  " << _serverTosendData << " _portTosendData " << _portTosendData << "_portServerConfiguration "<< _portServerConfiguration<<" _frenquency : " << _frenquency << " _spiBus : " << _spiBus << " _spreadFactor " << _spreadFactor <<"\n"; 

    return 0;

}