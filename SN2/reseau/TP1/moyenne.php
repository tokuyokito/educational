<!DOCTYPE html>
<html>
<head>
	 <meta charset="UTF-8"> 
	<title>Tp1 BTS SN2</title>

	<?php
		include 'fonction.php';

		$notes  = array(5, 12,8, 20, 10);

		// Ajout d'une note
		$notes[]=13.5;

		// modification de l'index 2
		$notes[2] = 9;
		
		print_moyenne($notes);
		
		print_onteutlamoyenne($notes);
		
		print_notemax($notes);

		// tableau associatif
		$etudiants = array('toto'    =>  '15', 
							'tata'   =>  '12', 
							'titi'   =>  '17', 
							'tonton' =>  '9', 
							'foo'    =>  '20');

		echo "<hr>";
		
		echo "<h1> Tableau associatif   </h1>";
		
		echo "<h2> Résultats évaluation </h2>";

		print_associatif($etudiants);

		echo "<hr>";

		//tableau discontiguës
		$tabVile[0]  =  "Lannion";
		$tabVile[1]  =  "Guingamp";
		$tabVile[3]  =  "Morlaix";
		$tabVile[4]  =  "Brest";
		$tabVile[10] =  "Rennes";

		echo "<h1> Utilisation d'une boucle while </h1>";
		print_tabVille_while($tabVile);
		echo "<hr>";
		
		echo "<h1> Utilisation d'une boucle for </h1>";
		print_tabVille_for($tabVile);
		
		echo "<h1> Utilisation d'une boucle foreach </h1>";
		echo "<hr>";
		print_associatif($tabVile);
		echo "<hr>";

		$joueurFoot = array('joueur1' => array('nom'    => 'MBAPPE', 
											   'prenom' => 'Kylian',
										       'age'    => '19',
										       'club'   => 'France',	
											  ), 

					 	    'joueur2' => array('nom'    => 'POGBA', 
										       'prenom' => 'Paul',
										       'age'    => '34',
										       'club'   => 'France',	
											  ), 

					 		'joueur3' => array('nom'    => 'LIVAKOVIC', 
											   'prenom' => 'Dominic',
										       'age'    => '24',
										       'club'   => 'Croatie',	
										      ), 

					 		'joueur4' => array('nom'    => 'PJACA', 
											   'prenom' => 'Marko',
										       'age'    => '29',
										       'club'   => 'Croatie',	
											  ),
				     
				     		'joueur5' => array('nom'    => 'LUKAKU', 
											   'prenom' => 'Romelu',
											   'age'    => '34',
											   'club'   => 'Belgique',	
											  ),
					 
							'joueur6' => array('nom'    => 'POPE', 
											   'prenom' => 'Nick',
										       'age'    => '34',
										       'club'   => 'Angleterre',	
											  ),
					 
					 		'joueur7' =>  array('nom'    => 'KANE', 
										 		'prenom' => 'Harry',
										 		'age'    => '34',
										 		'club'   => 'Angleterre',	
										       ),
					 
					  		'joueur8' => array('nom'    => 'DE BRUYNE', 
										       'prenom' => 'Kévin',
										       'age'    => '26',
										       'club'   => 'Belgique',	
											  )    
	);
		foreach ($joueurFoot as $key => $value) {
			
			foreach ($key as $keyy => $valuee) {
				# code...
			}
			echo "$keyy : $valuee <br>";
		}
	?>

</head>
<body>

</body>
</html>