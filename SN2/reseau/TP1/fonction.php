<?php
// Calcule de la moyenne
function moyenne($tab_valeur)
{
	$somme = 0;
			
	for ($i=0; $i < sizeof($tab_valeur); $i++) { 
					
		$somme  += $tab_valeur[$i] ;
					
	}	
			
	if ($somme == 0) return 0;
			
	return $somme / sizeof($tab_valeur); 	
}
		
// cherche le nombre de perssone qui on eu la moyenne
function onteulamoyenne($tab_valeur) {
		
	$moyenne = moyenne($tab_valeur);
	$nb_perssonne = 0;

	for ($i=0; $i < sizeof($tab_valeur); $i++) { 
				
		if ($tab_valeur[$i] > $moyenne) $nb_perssonne++;
						
	}
			
	return $nb_perssonne;						
}

// Cherche la note la plus haute
function notemax($tab_valeur)
{
	$note = 0;
			
	for ($i=0; $i < sizeof($tab_valeur); $i++) { 
				
		if ($tab_valeur[$i] > $note) $note = $tab_valeur[$i];
						
	}

	return $note;
}

// Calcule de la valeur moyenne
function print_moyenne($notes)
{
	echo "<p>La moyenne est de ".moyenne($notes).".</p>\n";
}

// Calcule du nombre de persone qui on eu la moyenne
function print_onteutlamoyenne($notes)
{
	echo "<p>".onteulamoyenne($notes)." personnes on eu la moyenne.</p>\n";
}	

// Calcule de la note max
function print_notemax($notes)
{
	echo "La note max est de ".notemax($notes).".</p>\n";
}

// Affichage d'un tableau associatif
function print_associatif($tab)
{
	// Affichage des clées et des valeurs
	foreach ($tab as $key => $value) echo "$key a obtenue $value <br>\n";
}

// Utilisation d'une boucle while pour afficher un tableau discontiguës
function print_tabVille_while($tab)
{
	$i = 0;
	while ( $i < sizeof($tab)) {
		
		if (array_key_exists($i, $tab)) echo "Ville : $tab[$i]\n <br>\n ";

		$i++;
	}
}

// Utilisation d'une boucle for pour afficher un tableau discontiguës
function print_tabVille_for($tab)
{
	for ($i=0; $i < sizeof($tab); $i++) if (array_key_exists($i, $tab)) echo "Ville : $tab[$i]\n <br>\n ";

}

?>

