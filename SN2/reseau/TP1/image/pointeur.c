#include <stdlib.h>
#include <stdio.h>

void inverse(int m, int n)
{

	int inverse;

	inverse = m;
	m = n;
	n = inverse;

}


int main()
{
	int a = 2;
	int b = 6;

	printf("a = %d b = %d\n", a, b);

	//permutation

	inverse(a, b);

	/*
		int inverse = a;
		a = b;
		b = inverse;
	*/
	printf("a = %d b = %d\n", a, b);

	return EXIT_SUCCESS;
}