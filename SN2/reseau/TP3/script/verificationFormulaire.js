"use scrict";

function verifLogin()
{
	
	var pse = document.getElementById('pseudo');
	var err = document.getElementById('erreur');
	
	err.innerHTML="";
	var retour = true;
	
	if(pse.value.length < 4){
		
		err.innerHTML = "Votre pseudo doit contenir au moins 4 caractères.<br/>";
		retour = false;
	}
	else{
		
		err.innerHTML = "";
		err.innerHTML = "Pseudo OK.<br/>";

	}

	return retour;
}

function contientMajuscule(t)
{
	return /[A-Z]/.test(t);
}

function contientPonctuation(t)
{
	return /[-_;,:.]/.test(t);
}

function verifMdP()
{
	var pswUser = document.getElementById('pwdUser');
	var errMdp = document.getElementById('errMdp');
	
	errMdp.innerHTML="";
	var retour = true;
	
	if(pswUser.value.length < 8){
		
		errMdp.innerHTML = "Mot de passe faible. Taille : "+pswUser.value.length+"<br/>";
		errMdp.style.color = "red";
		retour = false;
	}
	else if(pswUser.value.length < 15 && pswUser.value.length >= 8) {
		
		errMdp.innerHTML = "";
		errMdp.style.color = "orange";
		errMdp.innerHTML = "Mot de passe moyen. Taille : " + pswUser.value.length +"<br/>";		

	}
	else if (pswUser.value.length > 15 && contientPonctuation(pswUser.value) == true && contientMajuscule(pswUser.value) === true) 
	{
		errMdp.innerHTML = "";
		errMdp.style.color = "green";
		errMdp.innerHTML = "Mot de passe OK. Taille : "+pswUser.value.length+"<br/>";

	}
	else{}

	return retour;
}

function verifConfirmMdP()
{
	var pswUser     = document.getElementById('pwdUser');
	var confPwdUser = document.getElementById('confPwdUser');
	var errMdp      = document.getElementById('errMdpConf');
	
	errMdp.innerHTML="";
	var retour = true;
	
	if(pswUser.value === confPwdUser.value){
		
		errMdp.innerHTML = "Mot de passe identique OK.<br/>";
		errMdp.style.color = "green";
		
	}
	else{
		
		errMdp.innerHTML = "";
		errMdp.innerHTML = "Mot de passe différent.<br/>";
		errMdp.style.color = "red";
		retour = false;
	}

	return retour;
}
