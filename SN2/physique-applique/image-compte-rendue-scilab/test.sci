// Auteurs  : Le Gall cédric
// Objectif : Négatif d'une image
// date     : 18/09/2018

// récupération de l'image
pathImage = uigetfile("*.jpg");

// récupération des données de l'image
image = rgb2gray(imread(pathImage));

image(:,:, 1) = 255 - image(:,:, 1);

imshow(image);
