#!/usr/bin/python3.5

# auteur : Le Gall Cedric
# fonction programme : Fonction qui une chaine de caracteres de date formate selon l'argument envoyée 
# date : 24/05/2018

#modules
from datetime import datetime
import os
import os.path


import warnings
warnings.filterwarnings('default', category=DeprecationWarning)

#Demande en argument un nombre compris entre [0; 1]
#Renvoie une chaine de caracteres de date formate selon l'argument envoyée
def formatageDate(formatage):
        
        date = datetime.now()
	
        if formatage == 0:
		
                formatage = str(date.year) +"_" + str(date.day) + "_" + str(date.month) \
                + "_" + str(date.hour) +  ":" + str(date.minute) +  ":" +str(date.second) 
                
                return formatage
	
        elif formatage == 1:
		
                formatage = str(date.year) + "_" + str(date.day) + "_" + str(date.month)
                return formatage

        else:
		
                print("Non definie")


def creationDossier():

	#ouverture fichier de configuration
	if not os.path.exists("variables"):

		print("Erreur fichier <variable> manquant")
		print("Reconfiguration du fichier <variables>")

		fichier_config = open("variables", "a")
		fichier_config.write("SeuilDeclanchement:0:\ntempoAvantAction:0:\ntempoApreAction:0:")
		print("Reconfiguration termine : suite du programe")
		fichier_config.close()

	date = formatageDate(1)
    	
	#Creation du fichier du jour si inexistant
	if not os.path.exists(date):
        
		os.mkdir(date)
		print("Le chemin cree est : " + date)
         
	return date         
    


def configCapteur():
	
	fichier =  open("variables", "r")
	donnes = fichier.read()
	fichier.close()
	tableau_recup_nombre = donnes.split(':', 7 )
	tableau_envoye = [tableau_recup_nombre[1], tableau_recup_nombre[3], tableau_recup_nombre[5]]
	
	return tableau_envoye