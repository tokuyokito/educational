import http.server
from threading import Thread, RLock

verrou = RLock();

class Serveur_web(Thread):

    def __init__(self, port=8888):

        Thread.__init__(self);
		
        self.PORT = port;
        self.server_address = ("", self.PORT);

        self.server = http.server.HTTPServer;
        self.handler = http.server.CGIHTTPRequestHandler;
        self.handler.cgi_directories = [""];
        print("Serveur actif sur le port :", self.PORT);

        self.httpd = self.server(self.server_address, self.handler);
		        
        

    def run(self):
        with verrou:
	           
            print("Thread serveur")
            self.httpd.serve_forever();