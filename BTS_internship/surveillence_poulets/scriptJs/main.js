"use strict";

var compteur = 0;
var tempo = 0;

window.onload = function() {
    
};




function envoieValeurCompteur(value) {

	var afficheurCompteur     = document.getElementById("EmplacementAfficheurCompteur");		
	var acienneValeurAfficher = afficheurCompteur.firstChild;
	
	if (value === '+') {
		compteur++
	}
	else {
		
		if (compteur) compteur--;
			
	}
	
	
	
	var valeurCompteurAfficher =  document.createTextNode(compteur);
	
	afficheurCompteur.replaceChild(valeurCompteurAfficher, acienneValeurAfficher);
	console.log("Valeur compteur : " + compteur);
}

function envoieValeurTempo(value, name) {

	var afficheurTempo     = document.getElementById("EmplacementAfficheurTempo");		
	var acienneValeurAfficher = afficheurTempo.firstChild;

	if (value === '+') {

		if (name === "tempo+1") {

			tempo++;
		}
		else if (name === "tempo+0.1") {
			tempo += 0.1;
		}
		else if (name === "tempo+0.01"){
			tempo += 0.01; 
		}
		else {
			tempo += 0.001;
		}

	}
	else {

		if (name === "tempo-1" && tempo != 0 && arrondiTroncature(tempo) > 0.0001) {
			tempo -= 1;
		}
		else if (name === "tempo-0.1" && tempo != 0 && arrondiTroncature(tempo) > 0.0001) {
			tempo -= 0.1;
		}
		else if (name === "tempo-0.01" && tempo != 0 && arrondiTroncature(tempo) > 0.0001){
			tempo -= 0.01;
		}
		else if (name === "tempo-0.001" && tempo != 0) {
			tempo -= 0.001;
		}
		else {

		}
	}
	tempo = Math.abs(arrondiTroncature(tempo));

	var valeurTempoAfficher =  document.createTextNode(tempo + " s");
	
	afficheurTempo.replaceChild(valeurTempoAfficher, acienneValeurAfficher);
	console.log("Valeur tempo : " + tempo);
}

function arrondiTroncature(nombre) {
	
	console.log("nombre : " + nombre)
	var arrondi = nombre*1000;
	arrondi = Math.round(arrondi);
	console.log(arrondi)        
	return arrondi/1000; 
}