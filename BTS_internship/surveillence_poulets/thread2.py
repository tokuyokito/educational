import capteur
import photo
import fonction
from threading import Thread, RLock

verrou = RLock();

class Thread2(Thread):

    def __init__(self):

        Thread.__init__(self);
	
        self.pathDossier = fonction.creationDossier()
        self.configurationCapteur = fonction.configCapteur()

        self.capteur = capteur.Capteur(self.configurationCapteur[0], self.configurationCapteur[1], self.configurationCapteur[2])
        self.capteur.info()

        self.image = photo.Photo(self.pathDossier)		        
        

    def run(self):
        with verrou:
	           
            while True : 
    
                if str(fonction.creationDossier()) != str(self.pathDossier) :
	    
                    self.pathDossier = fonction.creationDossier()
                    print("nouveau chemin : " + self.pathDossier)
                    self.image.setRepertoireSauvegarde(pathDossier)
		
                if self.capteur.testPassageObjet() == True:
		
                    self.capteur.incrementationCompteur()
                    print("incrementation du compteur : " + str(capteur.getCompteurObjet()))
	
                if self.capteur.testEgaliteSeuilCompteur() == True:
		
                    nomFichier = fonction.formatageDate(0)
		
                    self.capteur.setCompteurObjet(0)
                    print(nomFichier)
		
                    self.image.setNomPhoto(nomFichier)
                    self.image.setTextOnImage(nomFichier)
            
                    self.capteur.attendreAvantAction()
                    self.image.prendrePhoto()
                    self.capteur.attendreApreAction() 



