#!/usr/bin/python3.5

# auteur : Le Gall Cedric
# fonction programme : Utilisation de l'api picamera pour la prise de photo
# date : 24/05/2018

#modules
import picamera
import time

import warnings
warnings.filterwarnings('default', category=DeprecationWarning);

class Photo():

	# Arguments 
	# Repertoire sauvegarde photo
	# nom photo
	# format photo 

	#Méthodes
	# __init__

	# setRepertoireSauvegarde
 	# setNomPhoto
 	# setFormatPhoto	
	 
	# getRepertoireSauvegarde
	# getNomPhoto
	# getFormatPhoto

	#prendrePhoto
	# info

	def __init__(self, repertoireSauvegarde = "", formatPhoto = "png", nomPhoto = "basicName"):

		self.camera               = picamera.PiCamera();
		self.repertoireSauvegarde = repertoireSauvegarde
		self.nomPhoto             = nomPhoto
		self.formatPhoto          = formatPhoto

	
	def setRepertoireSauvegarde(self, text):
		print("variable recus : " + text)
		self.setRepertoireSauvegarde = str(text)
	
	
	def setNomPhoto(self, text):
		self.nomPhoto = text

	
	def setFormatPhoto(self, text):
		self.formatPhoto = text

	
	def getRepertoireSauvegarde(self):
		return str(self.setRepertoireSauvegarde)
	
	
	def getNomPhoto(self):
		return self.nomPhoto

	
	def getFormatPhoto(self):
		return self.formatPhoto

	
	def prendrePhoto(self):

		self.camera.capture(self.repertoireSauvegarde + "/" + self.nomPhoto + "." + self.formatPhoto)
	
	def setTextOnImage(self, text):
		
		self.camera.annotate_text = text
		
	def info(self):
		
		print("repertoireSauvegarde" + self.repertoireSauvegarde)
		print("nomPhoto" + self.nomPhoto)
		print("formatPhoto" + self.formatPhoto)
