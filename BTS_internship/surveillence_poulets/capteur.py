#!/usr/bin/python3.5
from threading import Thread, RLock
import time

class Capteur():

	# Arguments 
	# compteur objets
	# Seuil declanchement d'une action x 
	# temporisation avant l'action x
	# temporisation après l'action x

	#Méthodes
	# __init__

	# setCompteurObjet
 	# setSeuilDeclanchement	
	# setTempoAvantAction
	# setTempoApreAction
	# incrementationCompteur
	 
	# getCompteurObjet
	# getSeuilDeclanchement
	# getTempoAvantAction
	# getTempoApreAction

	# testPassageObjet
	# attendreAvantAction
	# attendreApreAction
	# testEgaliteSeuilCompteur
	# info

	def __init__(self, SeuilDeclanchement = 0, tempoAvantAction = 0, tempoApreAction = 0):

		self.compteurObjet        = 0
		self.seuilDeclanchement   = SeuilDeclanchement
		self.tempoAvantAction     = tempoAvantAction
		self.tempoApreAction      = tempoApreAction
	
	def setCompteurObjet(self, nombre):
		self.compteurObjet = nombre


	def setSeuilDeclanchement(self, nombre):
		self.seuilDeclanchement = nombre


	def setTempoAvantAction(self, nombre):
		self.tempoAvantAction = nombre


	def setTempoApreAction(self, nombre):
		self.tempoApreAction = nombre				
	

	def incrementationCompteur(self):
		self.compteurObjet = self.compteurObjet + 1 

	def getCompteurObjet(self):
		return self.compteurObjet


	def getSeuilDeclanchement(self):
		return self.seuilDeclanchement


	def getTempoAvantAction(self):
		return self.tempoAvantAction


	def getTempoApreAction(self):
		return self.tempoApreAction

	def testPassageObjet(self):
		
		passageObjet = False
		#TO DO

		return passageObjet

	def attendreAvantAction(self):
		time.sleep(float(self.tempoAvantAction))

	def attendreApreAction(self):
		time.sleep(float(self.tempoApreAction))

	def testEgaliteSeuilCompteur(self):
            essais = True
            
            if int(self.compteurObjet) == int(self.seuilDeclanchement) :
                essais = True
                return bool(essais)

            return bool(essais)
			
	
	def info(self):
		
		print("------------")
		print("compteurObjet : "        + str(self.compteurObjet))
		print("------------")
		print("seuilDeclanchement : "   + str(self.seuilDeclanchement))
		print("------------")
		print("tempoAvantAction : "     + str(self.tempoAvantAction))
		print("------------")
		print("tempoApreAction : "      + str(self.tempoApreAction) + "\n")
